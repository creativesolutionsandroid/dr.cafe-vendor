package com.cs.dr_cafe_vendor.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.OrderDetailsList;
import com.cs.dr_cafe_vendor.R;
import com.xujiaji.happybubble.BubbleDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CustomDialog extends BubbleDialog implements View.OnClickListener {
    private ViewHolder mViewHolder;
    private OnClickCustomButtonListener mListener;
    String search;
    OrderDetailsList.Data orderdetailsList;
    PopupWindow popupWindow;

    public CustomDialog(final Context context, final OrderDetailsList.Data orderdetailsList) {
        super(context);
        setTransParentBackground();
        setPosition(Position.TOP);
        View rootView = LayoutInflater.from(context).inflate(R.layout.address_popup, null);
        mViewHolder = new ViewHolder(rootView);
        addContentView(rootView);

        mViewHolder.address_txt.setText("" + orderdetailsList.getCAddress());


    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onClick(((Button) v).getText().toString());
        }
    }

    private static class ViewHolder {
        //        EditText search_text;
        TextView address_txt;


        public ViewHolder(View rootView) {
            address_txt = (TextView) rootView.findViewById(R.id.address_txt);

        }
    }

    public void setClickListener(OnClickCustomButtonListener l) {
        this.mListener = l;
    }

    public interface OnClickCustomButtonListener {
        void onClick(String str);
    }
}
