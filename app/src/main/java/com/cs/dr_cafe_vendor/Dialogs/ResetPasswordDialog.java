package com.cs.dr_cafe_vendor.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.cs.dr_cafe_vendor.Activities.ForgotPasswordActivity;
import com.cs.dr_cafe_vendor.Activities.SignInActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.ResetPasswordlist;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//**************Puli*************

public class ResetPasswordDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    EditText inputPassword, inputConfirmPassword;
    String strPassword, strConfirmPassword;
    Button buttonSubmit, buttonVerify;
    String strMobile,strotp,userId;
    View rootView;
    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences LanguagePrefs;
    String language;

    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.dialog_reset_password, container, false);
        } else {
            rootView = inflater.inflate(R.layout.dialog_reset_password_arabic, container, false);
        }
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getArguments().getString("mobile");
        strotp = getArguments().getString("otp");
        userId = getArguments().getString("userid");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword = (EditText) rootView.findViewById(R.id.change_password_input_new_password);
        inputConfirmPassword = (EditText) rootView.findViewById(R.id.change_password_input_confirm_password);

        buttonSubmit = (Button) rootView.findViewById(R.id.button_submit);

//        setTypeface();

        buttonSubmit.setOnClickListener(this);

        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

//    private void setTypeface(){
//        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
//                "helvetica.ttf");
//
//        inputPassword.setTypeface(typeface);
//        inputConfirmPassword.setTypeface(typeface);
//        buttonSubmit.setTypeface(typeface);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_submit:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            } else {
                inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, getActivity());
            return false;
        }
        return true;
    }


    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reset_input_password:

                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.reset_input_retype_password:

                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")) {
                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();


        try {
            parentObj.put("VendorId",userId);
            parentObj.put("OTP",strotp);
            parentObj.put("NewPassword",strPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }



    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {

//        ACProgressFlower dialog;
        String inputStr;
        AlertDialog loaderDialog = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();


            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ResetPasswordlist> call = apiService.getresetpass(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ResetPasswordlist>() {
                @Override
                public void onResponse(Call<ResetPasswordlist> call, Response<ResetPasswordlist> response) {
                    if(response.isSuccessful()){
                        ResetPasswordlist resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){

                                Log.i(TAG, "onResponse: ");
//                                status true case
//                                String userId = String.valueOf(resetPasswordResponse.getData().getVendorId());
//                                userPrefsEditor.putString("userId", userId);
//                                userPrefsEditor.putString("name", resetPasswordResponse.getData().getUserName());
//                                userPrefsEditor.putString("email", resetPasswordResponse.getData().getEmail());
//                                userPrefsEditor.putString("mobile", resetPasswordResponse.getData().getMobile());
//                                userPrefsEditor.commit();
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getActivity(), R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.reset_success_msg_ar, Toast.LENGTH_SHORT).show();
                                }
//                                ForgotPasswordActivity.isResetSuccessful = true;

                                getDialog().dismiss();
                                Intent intent = new Intent(getActivity(), SignInActivity.class);
                                startActivity(intent);
                            }
                            else {
//                                status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = resetPasswordResponse.getMessageEn();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = resetPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ResetPasswordlist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
