package com.cs.dr_cafe_vendor.Dialogs;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cs.dr_cafe_vendor.Activities.ForgotPasswordActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.ForgetPassword;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//**************Puli*************

public class VerifyOtpDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    TextView textMobileNumber, textOtpTitle, textBody1, textBody2, textBody3;
    String otpEntered = "", strName, strEmail, strMobile, strPassword, serverOtp;
    Button buttonResend, buttonVerify;
    //    ImageView imgEditMobile;
    String language;

    OtpView otpView;

    View rootView;
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;

    public static VerifyOtpDialog newInstance() {
        return new VerifyOtpDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        rootView = inflater.inflate(R.layout.dialog_verify_otp, container, false);

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        textMobileNumber = (TextView) rootView.findViewById(R.id.otp_mobile_number);
        textOtpTitle = (TextView) rootView.findViewById(R.id.otp_title);
        textBody1 = (TextView) rootView.findViewById(R.id.otp_body1);
        textBody2 = (TextView) rootView.findViewById(R.id.otp_body2);
        textBody3 = (TextView) rootView.findViewById(R.id.otp_body3);
        otpView = (OtpView) rootView.findViewById(R.id.otp_view);

        buttonVerify = (Button) rootView.findViewById(R.id.button_verify_otp);
        buttonResend = (Button) rootView.findViewById(R.id.button_resend_otp);

        strName = getArguments().getString("name");
        strEmail = getArguments().getString("email");
        strMobile = getArguments().getString("mobile");
        strPassword = getArguments().getString("password");
        serverOtp = getArguments().getString("otp");
        textMobileNumber.setText(strMobile);

        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);
        setTypeface();
        setTimerForResend();

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                otpEntered = otp;
            }
        });

        buttonVerify.setOnClickListener(this);
        buttonResend.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setTypeface() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "helvetica.ttf");

        textBody1.setTypeface(typeface);
        textBody2.setTypeface(typeface);
        textBody3.setTypeface(typeface);
        textOtpTitle.setTypeface(typeface);
        buttonResend.setTypeface(typeface);
        buttonVerify.setTypeface(typeface);
        textMobileNumber.setTypeface(typeface);
    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
//                Log.i(TAG, "onTick: "+timeRemaining);
                if (getDialog() != null) {
                        buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);

                }
            }

            public void onFinish() {
                if (getDialog() != null) {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                    buttonResend.setEnabled(true);
                    buttonResend.setAlpha(1.0f);
                }
            }

        }.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_verify_otp:
                if (otpEntered.length() != 4) {
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                            getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), getActivity());

                } else if (!serverOtp.equals(otpEntered)) {

                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                            getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), getActivity());

                } else {
                    Fragment forgot = getFragmentManager().findFragmentByTag("forgot");
                    if (forgot != null && forgot.isVisible()) {
                        ForgotPasswordActivity.isOTPSuccessful = true;
                        getDialog().cancel();
                    }

                    Fragment register = getFragmentManager().findFragmentByTag("register");
                    if (register != null && register.isVisible()) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                            new userRegistrationApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                            }
                        }

                    }
                    break;
                }


                    case R.id.button_resend_otp:
                        String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new ForgotPasswordApi().execute();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                        }
                        break;
                }
        }

        private String prepareForgotPasswordJson () {
            JSONObject parentObj = new JSONObject();

            try {
                parentObj.put("mobileNo", "" + strMobile);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "prepareResetPasswordJson: " + parentObj.toString());
            return parentObj.toString();
        }

        private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {

            //        ACProgressFlower dialog;
            String inputStr;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                inputStr = prepareForgotPasswordJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
                Constants.showLoadingDialog(getActivity());
            }

            @Override
            protected String doInBackground(String... strings) {
                final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

                Call<ForgetPassword> call = apiService.forgotPassword(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
                call.enqueue(new Callback<ForgetPassword>() {
                    @Override
                    public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                        if (response.isSuccessful()) {
                            ForgetPassword forgotPasswordResponse = response.body();
                            try {
                                if (forgotPasswordResponse.getStatus()) {
                                    serverOtp = String.valueOf(forgotPasswordResponse.getData().getOTPCode());
                                    Log.i(TAG, "onResponse: " + serverOtp);
//                                displayVerifyOTPDialog();
                                } else {
                                    //                          status false case
                                    String failureResponse = forgotPasswordResponse.getMessageEn();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                            }
                        } else {

                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }

                        Constants.closeLoadingDialog();
                    }

                    @Override
                    public void onFailure(Call<ForgetPassword> call, Throwable t) {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }


                        Constants.closeLoadingDialog();
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Constants.closeLoadingDialog();
            }
        }

        @Override
        public void onDestroy () {
            super.onDestroy();
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
        }
    }

