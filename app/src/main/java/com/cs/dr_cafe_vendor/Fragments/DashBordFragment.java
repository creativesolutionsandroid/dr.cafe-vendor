package com.cs.dr_cafe_vendor.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.MainActivity;
import com.cs.dr_cafe_vendor.Adapters.OrderAccepteAdapter;
import com.cs.dr_cafe_vendor.Adapters.OrderReadyAdapter;
import com.cs.dr_cafe_vendor.Adapters.OrderNewAdapter;
import com.cs.dr_cafe_vendor.Adapters.SwipeDeckAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.Models.DashbordRenuveResponce;
import com.cs.dr_cafe_vendor.Models.SwipeDeckData;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.daprlabs.cardstack.SwipeDeck;
import com.daprlabs.cardstack.SwipeFrameLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.closeLoadingDialog;
import static com.cs.dr_cafe_vendor.Constants.customDialog;
import static com.cs.dr_cafe_vendor.Constants.customDialog1;

public class DashBordFragment extends Fragment {

    View rootView;
    RelativeLayout new_layout, accept_layout, ready_layout;
    TextView new_cout, accepted_cout, ready_cout;
    ImageView menu_btn, home, profile;
    RelativeLayout new_viewlayout,accept_viewlayout,ready_viewlayout;
    LinearLayout deleylayout;
    boolean viewall = true;
    public static int cardViewPosition = 0;
    public static int PrevOrdersSize = 0;

    View new_view, accept_view, ready_view;
    View new_unview, acc_unview, ready_unview;
    //    CustomListView list_item;
    RecyclerView list_item;
    TextView pending_txt, scheduled_txt, order_txt, total_revenue, today_orders;
    View pending_view, scheduled_view, order_view;
    String language, userId = "0", username = "0";
    SharedPreferences userPrefs;
    private static boolean pending_boolean = false, scheduled_boolean = false, order_boolean = false;
    RelativeLayout pending_orders, scheduled_orders, orders;
    TextView new_order_count, accepted_order_count, ready_order_count;
    TextView cancel_order_count, closed_order_count;
    TextView scheduled_order_count;

    RelativeLayout new_order, accepted_order, ready_order;
    RelativeLayout cancel_order, closed_order;
    RelativeLayout scheduled_order;

    RelativeLayout header_layout;
    boolean loder = true;
    boolean adapterLoading = true;
    boolean refreshData = false;
    boolean firstdata = true;

    ArrayList<DashBordResponce.NewOrders> delayedNewOrders = new ArrayList<>();
    ArrayList<DashBordResponce.AcceptedOrders> delayedacceptOrders = new ArrayList<>();
    ArrayList<DashBordResponce.ReadyOrders> delayedreadtOrders = new ArrayList<>();
//
//    ArrayList<DashBordResponce.NewOrders> viewallNewOrders = new ArrayList<>();
//    ArrayList<DashBordResponce.AcceptedOrders> viewallacceptOrders = new ArrayList<>();
//    ArrayList<DashBordResponce.ReadyOrders> viewallreadyOrders = new ArrayList<>();

    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    BroadcastReceiver mRegistrationBroadcastReceiver;
    private Timer timer = new Timer();
    private Timer timer1 = new Timer();

    DashBordResponce.Data dashbordarrylist;
    ArrayList<DashBordResponce.NewOrders> newarrylist = new ArrayList<>();
    ArrayList<DashBordResponce.AcceptedOrders> accepetarrylist = new ArrayList<>();
    ArrayList<DashBordResponce.ReadyOrders> readyarrylist = new ArrayList<>();
    DashbordRenuveResponce.Data revenu;


    int value = 1;
//    LisiviewAdapter OrderListAdapter;
//    AcceptLisiviewAdapter OrderacceptListAdapter;
//    ReadyLisiviewAdapter OrderReadyListAdapter;]

    OrderNewAdapter OrderListAdapter;
    OrderAccepteAdapter OrderacceptListAdapter;
    OrderReadyAdapter OrderReadyListAdapter;

    TextView noorderlayout;
    TextView delayorderstext, viewallorderstex;
    LinearLayout viewalllayout;
    ImageView new_image, acc_image, ready_image;
    //    int lastposition = -1;
    LinearLayoutManager linearLayoutManager;
    LinearLayout order_list_layout;
    Boolean servicebackground = true;
    SwipeDeckAdapter swipeDeckAdapter;
    SwipeDeck cardStack;
    SwipeFrameLayout swipe_layout;
//    Activity activity;
    CountDownTimer countDownTimer;
    ArrayList<SwipeDeckData> deckArrayList = new ArrayList<>();
    MediaPlayer mediaPlayer = null;
    TextView cancel ;
    boolean canShowPopup =true ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.arabic_home_fragment, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");
        username = userPrefs.getString("Name", "");

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("pushrecieved"));

        total_revenue = (TextView) rootView.findViewById(R.id.total_revenue);
        today_orders = (TextView) rootView.findViewById(R.id.today_orders);

        new_layout = (RelativeLayout) rootView.findViewById(R.id.new_layout);
        accept_layout = (RelativeLayout) rootView.findViewById(R.id.acc_layout);
        ready_layout = (RelativeLayout) rootView.findViewById(R.id.ready_layout);
        header_layout = (RelativeLayout) rootView.findViewById(R.id.header_layout);

        order_list_layout = (LinearLayout) rootView.findViewById(R.id.order_list_layout);

        new_cout = (TextView) rootView.findViewById(R.id.new_cout);
        accepted_cout = (TextView) rootView.findViewById(R.id.accepted_cout);
        ready_cout = (TextView) rootView.findViewById(R.id.ready_cout);
//        name = (TextView) rootView.findViewById(R.id.user_name);

        menu_btn = (ImageView) rootView.findViewById(R.id.menu_image);
        home = (ImageView) rootView.findViewById(R.id.home);
        profile = (ImageView) rootView.findViewById(R.id.profile);
        cancel = (TextView) rootView.findViewById(R.id.cancel);

        new_view = (View) rootView.findViewById(R.id.new_view);
        ready_view = (View) rootView.findViewById(R.id.ready_view);
        accept_view = (View) rootView.findViewById(R.id.accept_view);

        new_unview = (View) rootView.findViewById(R.id.new_unview);
        acc_unview = (View) rootView.findViewById(R.id.acc_unview);
        ready_unview = (View) rootView.findViewById(R.id.ready_unview);

        list_item = (RecyclerView) rootView.findViewById(R.id.list_item);
        deleylayout = (LinearLayout) rootView.findViewById(R.id.deleylayout);
        noorderlayout = (TextView) rootView.findViewById(R.id.noorderstext);
        delayorderstext = (TextView) rootView.findViewById(R.id.delayorders);
        viewallorderstex = (TextView) rootView.findViewById(R.id.viewalloeder);

        viewalllayout = (LinearLayout) rootView.findViewById(R.id.viewalllayout);

        pending_orders = rootView.findViewById(R.id.pending_orders_list);
        scheduled_orders = rootView.findViewById(R.id.schedule_orders_list);
        orders = rootView.findViewById(R.id.orders_list);

        new_order_count = rootView.findViewById(R.id.new_order_count);
        accepted_order_count = rootView.findViewById(R.id.accept_order_count);
        ready_order_count = rootView.findViewById(R.id.ready_order_count);

        scheduled_order_count = rootView.findViewById(R.id.schedule_order_count);

        cancel_order_count = rootView.findViewById(R.id.cancel_order_count);
        closed_order_count = rootView.findViewById(R.id.cloased_order_count);
        new_order = rootView.findViewById(R.id.new_order);
        accepted_order = rootView.findViewById(R.id.accept_order);
        ready_order = rootView.findViewById(R.id.ready_order);

        scheduled_order = rootView.findViewById(R.id.schedule_order);

        cancel_order = rootView.findViewById(R.id.cancel_order);
        closed_order = rootView.findViewById(R.id.cloased_order);

        pending_txt = rootView.findViewById(R.id.pending_txt);
        scheduled_txt = rootView.findViewById(R.id.schedule_txt);
        order_txt = rootView.findViewById(R.id.order_txt);

        pending_view = rootView.findViewById(R.id.pending_view);
        scheduled_view = rootView.findViewById(R.id.schedule_view);
        order_view = rootView.findViewById(R.id.order_view);
        new_image = rootView.findViewById(R.id.new_image);
        ready_image = rootView.findViewById(R.id.ready_image);
        acc_image = rootView.findViewById(R.id.acc_image);
        cardStack= (SwipeDeck) rootView.findViewById(R.id.swipe_deck);
        swipe_layout= (SwipeFrameLayout) rootView.findViewById(R.id.swipe_layout);

        mediaPlayer = MediaPlayer.create(getContext(), R.raw.tone);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        list_item.setLayoutManager(linearLayoutManager);

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).menuClick();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                canShowPopup = true;
                swipe_layout.setVisibility(View.GONE);
            }
        });


//        mlanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (language.equalsIgnoreCase("En")) {
//                    languagePrefsEditor.putString("language", "Ar");
//                    languagePrefsEditor.commit();
//                    language = languagePrefs.getString("language", "En");
////                    new ChangeLanguageApi().execute();
//                    Intent intent = new Intent(getContext(), MainActivity.class);
//                    startActivity(intent);
//                    getActivity().finish();
//                } else {
//                    languagePrefsEditor.putString("language", "En");
//                    languagePrefsEditor.commit();
//                    language = languagePrefs.getString("language", "En");
////                    new ChangeLanguageApi().execute();
//                    Intent intent = new Intent(getContext(), MainActivity.class);
//                    startActivity(intent);
//                    getActivity().finish();
//                }
//
//                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";
//
////                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
////                Crashlytics.setString(AppLanguage, language/* string value */);
////                Crashlytics.setString("Device Token", regId);
////
////                if (!userId.equals("0")) {
////                    Crashlytics.setString(UserId, userId/* string value */);
////                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
////                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
////                }
//
//            }
//        });


        new_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new_image.setBackgroundColor(getResources().getColor(R.color.gray2));
                acc_image.setBackgroundColor(getResources().getColor(R.color.gray));
                ready_image.setBackgroundColor(getResources().getColor(R.color.gray));

                value = 1;
                new Orderstatusapi().execute();

                new_view.setVisibility(View.VISIBLE);
                accept_view.setVisibility(View.GONE);
                ready_view.setVisibility(View.GONE);

                new_unview.setVisibility(View.GONE);
                acc_unview.setVisibility(View.VISIBLE);
                ready_unview.setVisibility(View.VISIBLE);

                if (newarrylist.size() > 0) {

                    noorderlayout.setVisibility(View.GONE);
                    list_item.setVisibility(View.VISIBLE);

                    if (value == 1) {
                        if (getActivity()!=null)
                        OrderListAdapter = new OrderNewAdapter(getContext(), newarrylist, value,language);
                        list_item.setAdapter(OrderListAdapter);
                        viewallorderstex.setText("" + newarrylist.size());
                        delayorderstext.setText("" + delayedNewOrders.size());

                    }

                } else {
                    if (language.equalsIgnoreCase("En")){
                        noorderlayout.setVisibility(View.VISIBLE);

                    }
                    else {
                        noorderlayout.setVisibility(View.VISIBLE);
                        noorderlayout.setText("لا يوجد طلبات متاحة");
                    }
                    list_item.setVisibility(View.GONE);

                }

            }
        });

        accept_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                value = 2;
                acc_image.setBackgroundColor(getResources().getColor(R.color.gray2));
                new_image.setBackgroundColor(getResources().getColor(R.color.gray));
                ready_image.setBackgroundColor(getResources().getColor(R.color.gray));

                new_view.setVisibility(View.GONE);
                accept_view.setVisibility(View.VISIBLE);
                ready_view.setVisibility(View.GONE);

                new_unview.setVisibility(View.VISIBLE);
                acc_unview.setVisibility(View.GONE);
                ready_unview.setVisibility(View.VISIBLE);
                new Orderstatusapi().execute();

                if (accepetarrylist.size() > 0) {

                    noorderlayout.setVisibility(View.GONE);
                    list_item.setVisibility(View.VISIBLE);
                    if (value == 2) {
                        OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), accepetarrylist, value,language);
                        list_item.setAdapter(OrderacceptListAdapter);
                        viewallorderstex.setText("" + accepetarrylist.size());
                        delayorderstext.setText("" + delayedacceptOrders.size());



                    }
                } else {
                    if (language.equalsIgnoreCase("En")){
                        noorderlayout.setVisibility(View.VISIBLE);

                    }
                    else {
                        noorderlayout.setVisibility(View.VISIBLE);
                        noorderlayout.setText("لا يوجد طلبات متاحة");
                    }
                    list_item.setVisibility(View.GONE);

                }
            }
        });

        ready_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new_image.setBackgroundColor(getResources().getColor(R.color.gray));
                acc_image.setBackgroundColor(getResources().getColor(R.color.gray));
                ready_image.setBackgroundColor(getResources().getColor(R.color.gray2));

                value = 3;

                new_view.setVisibility(View.GONE);
                accept_view.setVisibility(View.GONE);
                ready_view.setVisibility(View.VISIBLE);

                new_unview.setVisibility(View.VISIBLE);
                acc_unview.setVisibility(View.VISIBLE);
                ready_unview.setVisibility(View.GONE);
                new Orderstatusapi().execute();

                if (readyarrylist.size() > 0) {

                    noorderlayout.setVisibility(View.GONE);
                    list_item.setVisibility(View.VISIBLE);
                    if (value == 3) {
                        OrderReadyListAdapter = new OrderReadyAdapter(getContext(), readyarrylist, value,language);
                        list_item.setAdapter(OrderReadyListAdapter);
                        viewallorderstex.setText("" + readyarrylist.size());
                        delayorderstext.setText("" + delayedreadtOrders.size());
                    }
                } else {
                    if (language.equalsIgnoreCase("En")){
                        noorderlayout.setVisibility(View.VISIBLE);

                    }
                    else {
                        noorderlayout.setVisibility(View.VISIBLE);
                        noorderlayout.setText("لا يوجد طلبات متاحة");
                    }
                    list_item.setVisibility(View.GONE);

                }

            }

        });

        deleylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value == 1) {
                    delayedNewOrdersList();

                    if (delayedNewOrders.size() > 0) {
                        noorderlayout.setVisibility(View.GONE);
                        list_item.setVisibility(View.VISIBLE);

                        OrderListAdapter = new OrderNewAdapter(getContext(), delayedNewOrders, value,language);
                        list_item.setAdapter(OrderListAdapter);
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            noorderlayout.setVisibility(View.VISIBLE);

                        }
                        else {
                            noorderlayout.setVisibility(View.VISIBLE);
                            noorderlayout.setText("لا يوجد طلبات متاحة");
                        }
                        list_item.setVisibility(View.GONE);

                    }
                } else if (value == 2) {
                    acceptdelayedList();

                    if (delayedacceptOrders.size() > 0) {
                        noorderlayout.setVisibility(View.GONE);
                        list_item.setVisibility(View.VISIBLE);

                        OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), delayedacceptOrders, value, language);
                        list_item.setAdapter(OrderacceptListAdapter);
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            noorderlayout.setVisibility(View.VISIBLE);
                        }
                        else {
                            noorderlayout.setVisibility(View.VISIBLE);
                            noorderlayout.setText("لا يوجد طلبات متاحة");
                        }
                        list_item.setVisibility(View.GONE);
                    }

                } else if (value == 3) {
                    readydelayedNewOrdersList();

                    if (delayedreadtOrders.size() > 0) {
                        noorderlayout.setVisibility(View.GONE);
                        list_item.setVisibility(View.VISIBLE);

                        OrderReadyListAdapter = new OrderReadyAdapter(getContext(), delayedreadtOrders, value, language);
                        list_item.setAdapter(OrderReadyListAdapter);
                    } else {
                        noorderlayout.setVisibility(View.VISIBLE);
                        list_item.setVisibility(View.GONE);
                    }
                }
            }

        });

        timer = new Timer();
        timer.schedule(new MyTimerTask(), 15000, 15000);
        timer1 = new Timer();
        timer1.schedule(new MyTimerTask1(), 15000, 50000);

        viewalllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value == 1) {
                    if (viewall) {
                        if (newarrylist.size() > 0) {
                            noorderlayout.setVisibility(View.GONE);
                            list_item.setVisibility(View.VISIBLE);

                            OrderListAdapter = new OrderNewAdapter(getContext(), newarrylist, value, language);
                            list_item.setAdapter(OrderListAdapter);

                        } else {
                            if (language.equalsIgnoreCase("En")){
                                noorderlayout.setVisibility(View.VISIBLE);

                            }
                            else {
                                noorderlayout.setVisibility(View.VISIBLE);
                                noorderlayout.setText("لا يوجد طلبات متاحة");
                            }
                            list_item.setVisibility(View.GONE);

                        }

                    }
                } else if (value == 2) {

                    if (accepetarrylist.size() > 0) {
                        noorderlayout.setVisibility(View.GONE);
                        list_item.setVisibility(View.VISIBLE);
                        OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), accepetarrylist, value, language);
                        list_item.setAdapter(OrderacceptListAdapter);

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            noorderlayout.setVisibility(View.VISIBLE);

                        }
                        else {
                            noorderlayout.setVisibility(View.VISIBLE);
                            noorderlayout.setText("لا يوجد طلبات متاحة");
                        }
                        list_item.setVisibility(View.GONE);

                    }


                } else if (value == 3) {
                    if (readyarrylist.size() > 0) {
                        noorderlayout.setVisibility(View.GONE);
                        list_item.setVisibility(View.VISIBLE);

                        OrderReadyListAdapter = new OrderReadyAdapter(getContext(), readyarrylist, value, language);
                        list_item.setAdapter(OrderReadyListAdapter);

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            noorderlayout.setVisibility(View.VISIBLE);

                        }
                        else {
                            noorderlayout.setVisibility(View.VISIBLE);
                            noorderlayout.setText("لا يوجد طلبات متاحة");
                        }
                        list_item.setVisibility(View.GONE);
                    }
                }
            }
        });

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mupdate_order, new IntentFilter("UpdateOrder"));


        return rootView;
    }

    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            adapterLoading=true;
            new Orderstatusapi().execute();

        }
    };


    @Override
    public void onPause() {
        servicebackground=false;
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
        if (timer1 != null) {
            timer1.cancel();
        }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (servicebackground){

                            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                                new Orderstatusapi().execute();
                            } else {
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                                }
                            }

                        }

                    }
                });
            }
        }
    }


    private class MyTimerTask1 extends TimerTask {

        @Override
        public void run() {
            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (servicebackground){

                            String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                                new revenustatusapi().execute();
                            } else {
                                if (language.equalsIgnoreCase("En")) {
                                    Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    }
                });
            }
        }
    }


    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (loder) {

                Constants.showLoadingDialog(getActivity());
            }
            loder = false;
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashBordResponce> call = apiService.getorderstatus1(userId);
            call.enqueue(new Callback<DashBordResponce>() {
                @Override
                public void onResponse(Call<DashBordResponce> call, Response<DashBordResponce> response) {
                    if (response.isSuccessful()) {

                        DashBordResponce Response = response.body();

                        dashbordarrylist = Response.getData();

                        if (Response.getData().getNewOrders().size() != 0) {
                            Constants.Current_time = Response.getData().getNewOrders().get(0).getCurrentTime();
                        }

                        if (Response.getData().getAcceptedOrders().size() != 0) {
                            Constants.Current_time = Response.getData().getAcceptedOrders().get(0).getCurrentTime();
                        }

                        if (Response.getData().getReadyOrders().size() != 0) {
                            Constants.Current_time = Response.getData().getReadyOrders().get(0).getCurrentTime();
                        }

                        newarrylist = Response.getData().getNewOrders();
                        accepetarrylist = Response.getData().getAcceptedOrders();
                        readyarrylist = Response.getData().getReadyOrders();

//                        total_revenue.setText("" + dashbordarrylist.getCounters().getRevenue());
//                        today_orders.setText("" + Constants.decimalFormat.format(dashbordarrylist.getCounters().getTotalOrders()));

                        new_cout.setText("" + dashbordarrylist.getNewOrders().size());
                        accepted_cout.setText("" + dashbordarrylist.getAcceptedOrders().size());
                        ready_cout.setText("" + dashbordarrylist.getReadyOrders().size());



                        if (newarrylist.size() > 0){
//                            if (swipe_layout.getVisibility() != View.VISIBLE) {
                                try {
                                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.tone);
                                    mediaPlayer.start();
                                } catch (Exception e) {
                                    e.printStackTrace();

//                                }
                            }


                            if (PrevOrdersSize == 0) {
                                PrevOrdersSize = newarrylist.size();
                            }
                            if (PrevOrdersSize > newarrylist.size()) {
                                if (cardViewPosition > 0) {
                                    cardViewPosition = cardViewPosition - 1;
                                }
                                PrevOrdersSize = newarrylist.size();
                            }
                            else if (PrevOrdersSize < newarrylist.size()) {
                                adapterLoading = true;
//                                canShowPopup = true;
                                cardViewPosition = cardViewPosition + 1;
                                PrevOrdersSize = newarrylist.size();
                            }
                            if (canShowPopup) {
                                swipe_layout.setVisibility(View.VISIBLE);
                                setdata();
                            }
                        }
                        else {
                            swipe_layout.setVisibility(View.GONE);
                        }




                        delayedNewOrders.clear();

                        if (value==1){
                            if (newarrylist.size() == 0) {

                                viewallorderstex.setText("" + newarrylist.size());
                                delayorderstext.setText("" + delayedNewOrders.size());

                                if (language.equalsIgnoreCase("En")) {
                                    noorderlayout.setVisibility(View.VISIBLE);

                                } else {
                                    noorderlayout.setVisibility(View.VISIBLE);
                                    noorderlayout.setText("لا يوجد طلبات متاحة");
                                }
                            } else {

                                for (int i = 0; i < newarrylist.size(); i++) {
                                    SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//                            SimpleDateFormat current_date1 = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                    SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

                                    SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

                                    Date orderdate = null;
                                    String orderd_date;

                                    try {
                                        orderdate = current_date1.parse(newarrylist.get(i).getOrderDate());
                                        orderd_date = dateFormat.format(orderdate);

                                        String date, time;

                                        date = current_date2.format(orderdate);
                                        time = current_time.format(orderdate);


                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    Date currentdate = null;
                                    String current_timedate;
                                    Date startDate = null;

                                    try {
                                        currentdate = server_time.parse(Constants.Current_time);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    final long startMillis = orderdate.getTime();
                                    final long[] currentMillis = {currentdate.getTime()};

                                    long expriry30sce = startMillis + 30000;


                                    if (currentMillis[0] > expriry30sce) {


//                                        if (value==1){

                                        if (newarrylist.size() == 0) {


                                            viewallorderstex.setText("" + newarrylist.size());
                                            delayorderstext.setText("" + delayedNewOrders.size());
                                        } else {

                                            if (firstdata) {

                                                delayedNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//                                                Log.d("TAG", "newlistsize: " + delayedNewOrders.size());
                                                viewallorderstex.setText("" + newarrylist.size());
                                                delayorderstext.setText("" + delayedNewOrders.size());

                                            } else if (!firstdata) {
                                                if (value == 1) {
                                                    delayedNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//                                                    Log.d("TAG", "newlistsize: " + delayedNewOrders.size());
                                                    viewallorderstex.setText("" + newarrylist.size());
                                                    delayorderstext.setText("" + delayedNewOrders.size());
                                                } else {
                                                    delayedNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//                                                    Log.d("TAG", "newlistsize1: " + delayedNewOrders.size());
                                                    delayorderstext.setText("" + delayedNewOrders.size());
                                                }
                                            }
                                        }
                                    }
                                    else {

                                        viewallorderstex.setText("" + newarrylist.size());
                                        delayorderstext.setText("" + delayedNewOrders.size());

                                    }
//                            else
                                }{
//                                if (firstdata) {
//                                    viewallNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//                                    Log.d("TAG", "viewnew: " + viewallNewOrders.size());
//                                    viewallorderstex.setText("" + viewallNewOrders.size());
//                                } else if (!firstdata) {
//
//                                    if (value == 1) {
//                                        viewallNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//                                        viewallorderstex.setText("" + viewallNewOrders.size());
//                                    }
//
//                                }
//
//
//                            }
                                }
                            }

                        }

                        delayedacceptOrders.clear();


                        if (value == 2) {


                            if (accepetarrylist.size() == 0) {

                                viewallorderstex.setText("" + accepetarrylist.size());
                                delayorderstext.setText("" + delayedacceptOrders.size());
                            } else {


                                for (int i = 0; i < accepetarrylist.size(); i++) {
                                    SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                    SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

                                    SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

                                    Date orderdate = null;
                                    String orderd_date;

                                    try {
                                        orderdate = current_date1.parse(dashbordarrylist.getAcceptedOrders().get(i).getOrderDate());
                                        orderd_date = dateFormat.format(orderdate);

                                        String date, time;

                                        date = current_date2.format(orderdate);
                                        time = current_time.format(orderdate);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    Date currentdate = null;
                                    String current_timedate;
                                    Date startDate = null;


                                    try {
                                        currentdate = server_time.parse(Constants.Current_time);
                                        startDate = server_time.parse(dashbordarrylist.getAcceptedOrders().get(i).getOrderDate());
                                        current_timedate = dateFormat.format(currentdate);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    Date expect_date = null;
                                    String expected_date;

                                    try {
                                        expect_date = current_date.parse(dashbordarrylist.getAcceptedOrders().get(i).getExpectedDate());
                                        expected_date = dateFormat.format(expect_date);

                                        String date, time;

                                        date = current_date2.format(expect_date);
                                        time = current_time.format(expect_date);


                                    } catch (Exception e) {
//                                Crashlytics.logException(e);
                                        e.printStackTrace();
                                    }

                                    final long startMillis = orderdate.getTime();
                                    final long[] currentMillis = {currentdate.getTime()};
                                    final long end_millis = expect_date.getTime();


                                    currentMillis[0] = currentMillis[0] + 1000;

                                    long leftTimeInMilliseconds = currentMillis[0] - startMillis;
                                    long time90perstange = (long) ((end_millis - startMillis) * (0.9));
//                                    Log.d("TAG", "end_millis: " + end_millis);
//                                    Log.d("TAG", "startMillis: " + startMillis);
                                    long readyTime = startMillis + time90perstange;
//                                    Log.d("TAG", "90per: " + time90perstange);


                                    if (currentMillis[0] > readyTime) {

                                        if (firstdata) {
//                                        if (value==2) {
                                            delayedacceptOrders.add(accepetarrylist.get(i));
//                                            Log.d("TAG", "delayacclist: " + delayedacceptOrders.size());
                                            viewallorderstex.setText("" + accepetarrylist.size());
                                            delayorderstext.setText("" + delayedacceptOrders.size());

                                        } else if (!firstdata) {

                                            if (value == 2) {
                                                delayedacceptOrders.add(accepetarrylist.get(i));
//                                                Log.d("TAG", "delayacclist: " + delayedacceptOrders.size());
                                                viewallorderstex.setText("" + accepetarrylist.size());
                                                delayorderstext.setText("" + delayedacceptOrders.size());

                                            }
                                        }
                                    }
//                                else {
//                                    if (firstdata) {
//                                        viewallacceptOrders.add(accepetarrylist.get(i));
//                                        viewallorderstex.setText("" + viewallacceptOrders.size());
//                                        delayorderstext.setText("" + delayedacceptOrders.size());
//                                    } else if (!firstdata) {
//
//                                        if (value == 2) {
//                                            viewallacceptOrders.add(accepetarrylist.get(i));
//                                            delayorderstext.setText("" + delayedacceptOrders.size());
//                                            viewallorderstex.setText("" + viewallacceptOrders.size());
//                                        }
//                                    }

//                                }

                                }
                            }
                        }
//                        ****************************ready******************************************

                        if (value == 3) {

                            delayedreadtOrders.clear();


                            if (readyarrylist.size() == 0) {

                                viewallorderstex.setText("" + readyarrylist.size());
                                delayorderstext.setText("" + delayedreadtOrders.size());

                            } else {

                                for (int i = 0; i < readyarrylist.size(); i++) {

                                    SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                                    SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

                                    SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

                                    Date orderdate = null;
                                    String orderd_date;

                                    try {
                                        orderdate = current_date1.parse(dashbordarrylist.getReadyOrders().get(i).getOrderDate());
                                        orderd_date = dateFormat.format(orderdate);

                                        String date, time;

                                        date = current_date2.format(orderdate);
                                        time = current_time.format(orderdate);


                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    Date currentdate = null;
                                    String current_timedate;
                                    Date startDate = null;

                                    try {
                                        currentdate = server_time.parse(Constants.Current_time);
//                                startDate = server_time.parse(dashbordarrylist.getNewOrders().get(i).getOrderDate());
                                        current_timedate = dateFormat.format(currentdate);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    Date expect_date = null;
                                    String expected_date;

                                    try {
                                        expect_date = current_date.parse(dashbordarrylist.getReadyOrders().get(i).getExpectedDate());
                                        expected_date = dateFormat.format(expect_date);

                                        String date, time;

                                        date = current_date2.format(expect_date);
                                        time = current_time.format(expect_date);

                                    } catch (Exception e) {
//                                Crashlytics.logException(e);
                                        e.printStackTrace();
                                    }

                                    final long startMillis = orderdate.getTime();
                                    final long[] currentMillis = {currentdate.getTime()};
                                    final long end_millis = expect_date.getTime();

                                    currentMillis[0] = currentMillis[0] + 1000;
                                    long leftTimeInMilliseconds = currentMillis[0] - startMillis;

                                    if (currentMillis[0] > end_millis) {
                                        if (firstdata) {
//                                        if (value==3) {
                                            delayedreadtOrders.add(readyarrylist.get(i));
                                            viewallorderstex.setText("" + readyarrylist.size());
                                            delayorderstext.setText("" + delayedreadtOrders.size());
//
                                        } else if (!firstdata) {

                                            if (value == 3) {
                                                delayedreadtOrders.add(readyarrylist.get(i));
                                                delayorderstext.setText("" + delayedreadtOrders.size());
                                                viewallorderstex.setText("" + readyarrylist.size());
//                                                Log.d("TAG", "readylistsize: " + delayedreadtOrders.size());
                                            }
                                        }
                                    }
//                            else {
//
//                                if (firstdata) {
//                                    viewallreadyOrders.add(readyarrylist.get(i));
//                                    Log.d("TAG", "viewallready: " + viewallreadyOrders.size());
//                                    delayorderstext.setText("" + delayedreadtOrders.size());
//                                    viewallorderstex.setText("" + viewallreadyOrders.size());
////                                        }
//                                } else if (!firstdata) {
//
//                                    if (value == 3) {
//                                        viewallreadyOrders.add(readyarrylist.get(i));
//                                        Log.d("TAG", "viewallready: " + viewallreadyOrders.size());
//                                        viewallorderstex.setText("" + viewallreadyOrders.size());
//                                        delayorderstext.setText("" + delayedreadtOrders.size());
//                                    }
//
//
//                                }
                                }

                            }
                        }
                        if (!refreshData) {
                            refreshData = true;
                            firstdata = false;
                            if (value == 1) {
                                if (viewall) {
                                    OrderListAdapter = new OrderNewAdapter(getContext(), newarrylist, value,language);
                                    list_item.setAdapter(OrderListAdapter);

                                }
                            } else if (value == 2) {
                                if (viewall) {
                                    OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), accepetarrylist, value,language);
                                    list_item.setAdapter(OrderacceptListAdapter);

                                }

                            } else if (value == 3) {
                                if (viewall) {
                                    OrderReadyListAdapter = new OrderReadyAdapter(getContext(), readyarrylist, value,language);
                                    list_item.setAdapter(OrderReadyListAdapter);
                                }

                            }
                        } else {
                            if (value == 1) {
                                if (newarrylist.size() > 0) {
                                    OrderListAdapter = new OrderNewAdapter(getContext(), newarrylist, value,language);
                                    list_item.setAdapter(OrderListAdapter);
                                    OrderListAdapter.notifyDataSetChanged();
                                } else {
                                    OrderListAdapter = new OrderNewAdapter(getContext(), newarrylist, value,language);
                                    list_item.setAdapter(OrderListAdapter);
                                }

                            } else if (value == 2) {
                                if (accepetarrylist.size() > 0) {
                                    OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), accepetarrylist, value,language);
                                    list_item.setAdapter(OrderacceptListAdapter);
                                    OrderacceptListAdapter.notifyDataSetChanged();
                                } else {
                                    OrderacceptListAdapter = new OrderAccepteAdapter(getContext(), accepetarrylist, value,language);
                                    list_item.setAdapter(OrderacceptListAdapter);
                                }

                            } else if (value == 3) {
                                if (readyarrylist.size() > 0) {
                                    OrderReadyListAdapter = new OrderReadyAdapter(getContext(), readyarrylist, value,language);
                                    list_item.setAdapter(OrderReadyListAdapter);
                                    OrderReadyListAdapter.notifyDataSetChanged();
                                } else {
                                    OrderReadyListAdapter = new OrderReadyAdapter(getContext(), readyarrylist, value,language);
                                    list_item.setAdapter(OrderReadyListAdapter);
                                }
                            }

                        }
                    } else {
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }

                }



                @Override
                public void onFailure(Call<DashBordResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }
            });
            return null;
        }


    }

    private class revenustatusapi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (loder) {
                Constants.showLoadingDialog(getActivity());
            }
            loder = false;
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DashbordRenuveResponce> call = apiService.gettotalrevenu(userId);
            call.enqueue(new Callback<DashbordRenuveResponce>() {
                @Override
                public void onResponse(Call<DashbordRenuveResponce> call, Response<DashbordRenuveResponce> response) {
                    if (response.isSuccessful()) {

                        DashbordRenuveResponce Response = response.body();


                        revenu = Response.getData();

                        total_revenue.setText("" +  Constants.priceFormat1.format(revenu.getRevenue()));
                        today_orders.setText("" + revenu.getTotalOrders());


                    } else {
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<DashbordRenuveResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

//        if (timer == null) {
        servicebackground=true;
        adapterLoading=true;
//        timer = new Timer();
//        timer.schedule(new MyTimerTask(), 15000, 15000);
//        timer1 = new Timer();
//        timer1.schedule(new MyTimerTask1(), 15000, 50000);

//        }

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new Orderstatusapi().execute();
            new revenustatusapi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static class NetworkUtil {

        static int TYPE_WIFI = 1;
        static int TYPE_MOBILE = 2;
        static int TYPE_NOT_CONNECTED = 0;

        public static int getConnectivityStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static String getConnectivityStatusString(Context context) {
            int conn = NetworkUtil.getConnectivityStatus(context);
            String status = null;
            if (conn == NetworkUtil.TYPE_WIFI) {
                status = "Wifi enabled";
            } else if (conn == NetworkUtil.TYPE_MOBILE) {
                status = "Mobile data enabled";


            } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                status = "Not connected to Internet";
            }
            return status;
        }

    }

    private void delayedNewOrdersList() {
        showLoadingDialog(getActivity());
        delayedNewOrders.clear();
        for (int i = 0; i < dashbordarrylist.getNewOrders().size(); i++) {

            SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

            SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(dashbordarrylist.getNewOrders().get(i).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);


            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date currentdate = null;
            String current_timedate;
            Date startDate = null;

            try {
                currentdate = server_time.parse(dashbordarrylist.getNewOrders().get(i).getCurrentTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }

            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};

            long expriry30sce = startMillis + 30000;

            if (currentMillis[0] > expriry30sce) {

                delayedNewOrders.add(dashbordarrylist.getNewOrders().get(i));
//            delayorderstext.setText(delayedNewOrders.size());

            }
        }
        closeLoadingDialog();
    }

    private void acceptdelayedList() {
        showLoadingDialog(getActivity());
        delayedacceptOrders.clear();
        for (int i = 0; i < accepetarrylist.size(); i++) {

            SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
            SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);


            SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(accepetarrylist.get(i).getOrderDate());


                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);


            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date currentdate = null;
            String current_timedate;
            Date startDate = null;


            try {
                currentdate = server_time.parse(Constants.Current_time);


            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(accepetarrylist.get(i).getExpectedDate());
                expected_date = dateFormat.format(expect_date);

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);


            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }


            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};
            final long end_millis = expect_date.getTime();


            currentMillis[0] = currentMillis[0] + 1000;
            long leftTimeInMilliseconds = currentMillis[0] - startMillis;
            long time90perstange = (long) ((end_millis - startMillis) * (0.9));
            long readyTime = startMillis + time90perstange;


            if (currentMillis[0] > readyTime) {

                delayedacceptOrders.add(dashbordarrylist.getAcceptedOrders().get(i));

            }
        }
        closeLoadingDialog();
    }

    private void readydelayedNewOrdersList() {
        showLoadingDialog(getActivity());
        delayedreadtOrders.clear();
        for (int i = 0; i < readyarrylist.size(); i++) {

            SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);
            SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

            SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(readyarrylist.get(i).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);


            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date currentdate = null;
            String current_timedate;
            Date startDate = null;


            try {
                currentdate = server_time.parse(Constants.Current_time);



            } catch (ParseException e) {
                e.printStackTrace();
            }


            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(readyarrylist.get(i).getExpectedDate());

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);


            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};
            final long end_millis = expect_date.getTime();

            long expriry30sce = startMillis + 300000;

            if (currentMillis[0] > end_millis) {

                delayedreadtOrders.add(dashbordarrylist.getReadyOrders().get(i));


            }
        }
        closeLoadingDialog();
    }
    public static void showLoadingDialog(Activity context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.progress_bar_alert;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }




    public void setdata(){
        refreshDeck(0);
        if (adapterLoading){
            if (getActivity()!=null){
                swipeDeckAdapter = new SwipeDeckAdapter(newarrylist, deckArrayList, getActivity(), language,value,getActivity());
                cardStack.setAdapter(swipeDeckAdapter);
                swipeDeckAdapter.notifyDataSetChanged();
                cardStack.setSelection(cardViewPosition);
            }
        }else {
            swipeDeckAdapter.notifyDataSetChanged();
        }

        adapterLoading=false;

//        cardStack.animate();
        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
//                if(position > 0) {
                refreshDeck(deckArrayList.get(position).getOrderid());
                SwipeDeckAdapter adapter = new SwipeDeckAdapter(newarrylist, deckArrayList, getActivity(), language,value,getActivity());
                cardStack.setAdapter(adapter);
                if(cardViewPosition != 0){
                    cardViewPosition = cardViewPosition - 1;
                }

            }

            @Override
            public void cardSwipedRight(int position) {
//                Log.d("TAG", "deckArrayList: "+position);
                cardViewPosition = cardViewPosition + 1;
                if(position == (newarrylist.size()-1)){
                    SwipeDeckAdapter adapter = new SwipeDeckAdapter(newarrylist, deckArrayList, getActivity(), language,value,getActivity());
                    cardStack.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                    cardStack.animate();
                }
//                deckArrayList.remove(position);
            }

            @Override
            public void cardsDepleted() {

            }

            @Override
            public void cardActionDown() {

            }

            @Override
            public void cardActionUp() {

            }
        });
    }

    private void refreshDeck(int reqCode){
        int startPosition = 0;
        if(reqCode!=0) {
            for (int i = 0; i < newarrylist.size(); i++) {
                if (reqCode==(newarrylist.get(i).getOrderid())) {
                    if (i > 0) {
                        startPosition = i - 1;
                    }
                    break;
                }
            }
        }

        deckArrayList.clear();
        for (int i = startPosition; i < newarrylist.size(); i++){
            SwipeDeckData swipeDeckData = new SwipeDeckData();

            swipeDeckData.setCurrentTime(newarrylist.get(i).getCurrentTime());
            swipeDeckData.setExpectedDate(newarrylist.get(i).getExpectedDate());
            swipeDeckData.setFullname(newarrylist.get(i).getFullname());
            swipeDeckData.setStoreName(newarrylist.get(i).getStoreName());
            swipeDeckData.setStoreNameAr(newarrylist.get(i).getStoreNameAr());
            swipeDeckData.setOrderid(newarrylist.get(i).getOrderid());
            swipeDeckData.setOrderDate(newarrylist.get(i).getOrderDate());
            swipeDeckData.setMobile(newarrylist.get(i).getMobile());
            swipeDeckData.setOrderStatus(newarrylist.get(i).getOrderStatus());
            swipeDeckData.setOrderStatusAr(newarrylist.get(i).getOrderStatusAr());
            swipeDeckData.setOrderType(newarrylist.get(i).getOrderType());
            swipeDeckData.setOrderTypeAr(newarrylist.get(i).getOrderTypeAr());
            swipeDeckData.setOrderTypeAr(newarrylist.get(i).getOrderTypeAr());
            swipeDeckData.setTotalPrice(newarrylist.get(i).getTotalPrice());
            swipeDeckData.setPaymentMode(newarrylist.get(i).getPaymentMode());
            swipeDeckData.setPaymentType(newarrylist.get(i).getPaymentType());
            swipeDeckData.setPaymentTypeAr(newarrylist.get(i).getPaymentTypeAr());
            swipeDeckData.setInvoiceNo(newarrylist.get(i).getInvoiceNo());


            deckArrayList.add(swipeDeckData);
        }
//        Log.d("TAG", "refreshDeck: "+deckArrayList.size());
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            new Orderstatusapi().execute();
        }
    };

}
