package com.cs.dr_cafe_vendor.Fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.cs.dr_cafe_vendor.Activities.MainActivity;
import com.cs.dr_cafe_vendor.Activities.OrderTypeActivity;
import com.cs.dr_cafe_vendor.Adapters.OrderNewAdapter;
import com.cs.dr_cafe_vendor.Models.ChangeLanguageList;
import com.cs.dr_cafe_vendor.Models.OrderStatusList;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Activities.SplashScreenActivity.regId;


public class OrderStatus extends Fragment {

    View rootView;
    LinearLayout pending, scheduled, order;
    TextView pending_txt, scheduled_txt, order_txt, total_order, total_revenue;
    View pending_view, scheduled_view, order_view;
    String language, userId = "0";
    SharedPreferences userPrefs;
    private static boolean pending_boolean = false, scheduled_boolean = false, order_boolean = false;
    RelativeLayout pending_orders, scheduled_orders, orders;
    TextView new_order_count, accepted_order_count, ready_order_count;
    TextView cancel_order_count, closed_order_count;
    TextView scheduled_order_count, mlanguage;

    RelativeLayout new_order, accepted_order, ready_order;
    RelativeLayout cancel_order, closed_order;
    RelativeLayout scheduled_order;

    //    ImageView refresh;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    BroadcastReceiver mRegistrationBroadcastReceiver;
    private Timer timer = new Timer();


    OrderStatusList.Data orderLists ;

    OrderNewAdapter OrderListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_status, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.order_status_arabic, container, false);
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        total_order = rootView.findViewById(R.id.total_orders);
        total_revenue = rootView.findViewById(R.id.total_revenue);

        pending_orders = rootView.findViewById(R.id.pending_orders_list);
        scheduled_orders = rootView.findViewById(R.id.schedule_orders_list);
        orders = rootView.findViewById(R.id.orders_list);

        new_order_count = rootView.findViewById(R.id.new_order_count);
        accepted_order_count = rootView.findViewById(R.id.accept_order_count);
        ready_order_count = rootView.findViewById(R.id.ready_order_count);

        scheduled_order_count = rootView.findViewById(R.id.schedule_order_count);

        cancel_order_count = rootView.findViewById(R.id.cancel_order_count);
        closed_order_count = rootView.findViewById(R.id.cloased_order_count);
        new_order = rootView.findViewById(R.id.new_order);
        accepted_order = rootView.findViewById(R.id.accept_order);
        ready_order = rootView.findViewById(R.id.ready_order);

        scheduled_order = rootView.findViewById(R.id.schedule_order);

        cancel_order = rootView.findViewById(R.id.cancel_order);
        closed_order = rootView.findViewById(R.id.cloased_order);

        pending = rootView.findViewById(R.id.pending);
        scheduled = rootView.findViewById(R.id.schedule);
        order = rootView.findViewById(R.id.order);

        pending_txt = rootView.findViewById(R.id.pending_txt);
        scheduled_txt = rootView.findViewById(R.id.schedule_txt);
        order_txt = rootView.findViewById(R.id.order_txt);

        pending_view = rootView.findViewById(R.id.pending_view);
        scheduled_view = rootView.findViewById(R.id.schedule_view);
        order_view = rootView.findViewById(R.id.order_view);

//        refresh = rootView.findViewById(R.id.refresh);

        mlanguage = rootView.findViewById(R.id.language);

        mlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = languagePrefs.getString("language", "En");
                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = languagePrefs.getString("language", "En");
                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }

                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

//                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
//                Crashlytics.setString(AppLanguage, language/* string value */);
//                Crashlytics.setString("Device Token", regId);
//
//                if (!userId.equals("0")) {
//                    Crashlytics.setString(UserId, userId/* string value */);
//                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
//                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
//                }

            }
        });

        pending_orders.setVisibility(View.VISIBLE);
        scheduled_orders.setVisibility(View.GONE);
        orders.setVisibility(View.GONE);

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = true;
                scheduled_boolean = false;
                order_boolean = false;
                pending_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(getResources().getColor(R.color.black));

                pending_view.setVisibility(View.VISIBLE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.GONE);

                pending_orders.setVisibility(View.VISIBLE);
                scheduled_orders.setVisibility(View.GONE);
                orders.setVisibility(View.GONE);

            }
        });

        scheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = true;
                order_boolean = false;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                order_txt.setTextColor(getResources().getColor(R.color.black));

                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.VISIBLE);
                order_view.setVisibility(View.GONE);

                pending_orders.setVisibility(View.GONE);
                scheduled_orders.setVisibility(View.VISIBLE);
                orders.setVisibility(View.GONE);

            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pending_boolean = false;
                scheduled_boolean = false;
                order_boolean = true;

                pending_txt.setTextColor(getResources().getColor(R.color.black));
                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
                order_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                pending_view.setVisibility(View.GONE);
                scheduled_view.setVisibility(View.GONE);
                order_view.setVisibility(View.VISIBLE);

                pending_orders.setVisibility(View.GONE);
                scheduled_orders.setVisibility(View.GONE);
                orders.setVisibility(View.VISIBLE);

            }
        });


//        refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                new Orderstatusapi().execute();
//
//            }
//        });

        new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "New");
                } else {
                    a.putExtra("header", "طلبات جديدة");
                }
                a.putExtra("parameter", "New");
                startActivity(a);

            }
        });

        accepted_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "Pending");
                } else {
                    a.putExtra("header", "قبول طلبات ");
                }
                a.putExtra("parameter", "Accepted Orders");
                startActivity(a);

            }
        });

        ready_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "Ready");
                } else {
                    a.putExtra("header", "طلبات جاهزة");
                }
                a.putExtra("parameter", "Ready");
                startActivity(a);

            }
        });

        scheduled_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "Schedule Orders");
                } else {
                    a.putExtra("header", "طلبات مجدوله");
                }
                a.putExtra("parameter", "Schedule Orders");
                startActivity(a);

            }
        });

        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "Rejected");
                } else {
                    a.putExtra("header", "طلبات ملغيه");
                }
                a.putExtra("parameter", "Cancel Orders");
                startActivity(a);

            }
        });

        closed_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent a = new Intent(getActivity(), OrderTypeActivity.class);
                if (language.equalsIgnoreCase("En")) {
                    a.putExtra("header", "Close");
                } else {
                    a.putExtra("header", "طلبات مغلقه");
                }
                a.putExtra("parameter", "Closed Orders");
                startActivity(a);

            }
        });

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//
//
//            }
//        }, 60000);
//        timer.schedule(new MyTimerTask(), 60000, 60000);


        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (getActivity() != null) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new Orderstatusapi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });
            }
        }
    }

//    private String prepareChangePasswordJson() {
//        JSONObject parentObj = new JSONObject();
//
//        try {
//            parentObj.put("UserId", userId);
//            parentObj.put("DeviceToken", regId);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return parentObj.toString();
//    }

    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
//        String inputStr;
        AlertDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            inputStr = prepareChangePasswordJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderStatusList> call = apiService.getorderstatus(userId);
            call.enqueue(new Callback<OrderStatusList>() {
                @Override
                public void onResponse(Call<OrderStatusList> call, Response<OrderStatusList> response) {
                    if (response.isSuccessful()) {

                        OrderStatusList Response = response.body();

//                        if (regId.equals("-1") || regId == null) {
//
//                            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//                                @Override
//                                public void onReceive(Context context, Intent intent) {
//
////                                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
////
//////                                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
////
////                                        SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
////                                        regId = pref.getString("regId", "-1");
////
////                                        Log.i("TAG", "Firebase reg id: " + regId);
////                                    }
//                                }
//                            };
////
////                            SharedPreferences pref = getActivity().getSharedPreferences(Config.SHARED_PREF, 0);
////                            regId = pref.getString("regId", "-1");
//
//                            Log.i("TAG", "Firebase reg id: " + regId);
//
//                            String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";
//
////                            Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
////                            Crashlytics.setString(AppLanguage, language/* string value */);
////                            Crashlytics.setString("Device Token", regId);
////
////                            if (!userId.equals("0")) {
////                                Crashlytics.setString(UserId, userId/* string value */);
////                                Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
////                                Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
////                            }
////
////                            Crashlytics.getInstance();
////
//                        }

                        orderLists = Response.getData();
                        Log.d("TAG", "data: "+response);

//                        total_order.setText("" + orderLists.getTotalOrders());
//                        total_revenue.setText("" + Constants.decimalFormat.format(orderLists.getRevenue()) + " SAR");
//
//                        new_order_count.setText("" + orderLists.getNew());
//                        accepted_order_count.setText("" + orderLists.getAccepted());
//                        ready_order_count.setText("" + orderLists.getReady());
//
//                        scheduled_order_count.setText("" + orderLists.getScheduled());
//
//                        cancel_order_count.setText("" + orderLists.getCancelled());
//                        closed_order_count.setText("" + orderLists.getClosed());
//
//                        if (pending_boolean) {
//
//                            pending_orders.setVisibility(View.VISIBLE);
//                            scheduled_orders.setVisibility(View.GONE);
//                            orders.setVisibility(View.GONE);
//
//                            new_order_count.setText("" + orderLists.getNew());
//                            accepted_order_count.setText("" + orderLists.getAccepted());
//                            ready_order_count.setText("" + orderLists.getReady());
//
//                        } else if (scheduled_boolean) {
//
//                            pending_orders.setVisibility(View.GONE);
//                            scheduled_orders.setVisibility(View.VISIBLE);
//                            orders.setVisibility(View.GONE);
//
//                            scheduled_order_count.setText("" + orderLists.getScheduled());
//
//
//                        } else if (order_boolean) {
//
//                            pending_orders.setVisibility(View.GONE);
//                            scheduled_orders.setVisibility(View.GONE);
//                            orders.setVisibility(View.VISIBLE);
//
//                            cancel_order_count.setText("" + orderLists.getCancelled());
//                            closed_order_count.setText("" + orderLists.getClosed());
//
//                        }


                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
                        Log.i("TAG", "faler: ");
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<OrderStatusList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }
                }
            });
            return null;
        }
    }

    private String prepareChangeLangJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Flag", 2);
            parentObj.put("UserId", userId);
            parentObj.put("Language", language);
            parentObj.put("deviceToken", regId);
            parentObj.put("DeviceType", "andriod");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class ChangeLanguageApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangeLangJson();
//            dialog = new ACProgressFlower.Builder(getActivity())
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeLanguageList> call = apiService.getchangelang(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeLanguageList>() {
                @Override
                public void onResponse(Call<ChangeLanguageList> call, Response<ChangeLanguageList> response) {
                    if (response.isSuccessful()) {
                        ChangeLanguageList Response = response.body();


                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }
                }

                @Override
                public void onFailure(Call<ChangeLanguageList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                }
            });
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

//        if (timer == null) {
            timer = new Timer();
            timer.schedule(new MyTimerTask(), 60000, 60000);
//        }

        String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new Orderstatusapi().execute();
        } else {
            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static class NetworkUtil {

        static int TYPE_WIFI = 1;
        static int TYPE_MOBILE = 2;
        static int TYPE_NOT_CONNECTED = 0;

        public static int getConnectivityStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static String getConnectivityStatusString(Context context) {
            int conn = NetworkUtil.getConnectivityStatus(context);
            String status = null;
            if (conn == NetworkUtil.TYPE_WIFI) {
                status = "Wifi enabled";
            } else if (conn == NetworkUtil.TYPE_MOBILE) {
                status = "Mobile data enabled";


            } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                status = "Not connected to Internet";
            }
            return status;
        }

    }

}
