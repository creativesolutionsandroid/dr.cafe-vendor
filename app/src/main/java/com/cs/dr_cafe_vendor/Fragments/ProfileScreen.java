package com.cs.dr_cafe_vendor.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import com.cs.dr_cafe_vendor.Activities.ChangePasswordActivity;
import com.cs.dr_cafe_vendor.Activities.MainActivity;
import com.cs.dr_cafe_vendor.Activities.SignInActivity;
import com.cs.dr_cafe_vendor.Activities.SplashScreenActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.LogoutResponce;
import com.cs.dr_cafe_vendor.Models.LogoutServiceList;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileScreen extends Fragment implements View.OnClickListener {

    TextView tvChangePassword, tvLogout, app_version;
    EditText tvName, tvMobile, tvEmail;
    String strName, strMobile, strEmail;
    //    ImageView back_btn;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    ImageView backbtn;
    String userId, luserId;
    Context context;
    Toolbar toolbar;
    AlertDialog loaderDialog = null;

    private static String TAG = "TAG";
    private static int EDIT_REQUEST = 1;
    View rootView;
    SharedPreferences languagePrefs;
    String language;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.profile_activtiy, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.profile_activtiy_arabic, container, false);
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), resourseColor));
//        }

//        int currentapiVersion1 = Build.VERSION.SDK_INT;
//        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
//            getActivity().getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
//
//            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");
        userPrefsEditor = userPrefs.edit();

        tvName = (EditText) rootView.findViewById(R.id.edit_profile_input_name);
        tvEmail = (EditText) rootView.findViewById(R.id.edit_profile_input_email);
        tvMobile = (EditText) rootView.findViewById(R.id.edit_profile_input_mobile);
        backbtn = (ImageView) rootView.findViewById(R.id.menu_image);

        backbtn = (ImageView) rootView.findViewById(R.id.menu_image);

        tvLogout = (TextView) rootView.findViewById(R.id.logout);
        tvChangePassword = (TextView) rootView.findViewById(R.id.change_password);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager= getFragmentManager();

                Fragment mFragment = new DashBordFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mFragment).commit();
            }
        });

//        app_version = (TextView) rootView.findViewById(R.id.app_version);

        setTypeface();

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 Intent intent= new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);

            }
        });

        tvName.setText(userPrefs.getString("Name", "-"));
        tvEmail.setText(userPrefs.getString("email", "-"));
        tvMobile.setText("+" + userPrefs.getString("mobile", "-"));

        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

//        app_version.setText("App Version " + version);

        tvChangePassword.setOnClickListener(this);
        tvLogout.setOnClickListener(this);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.change_password:
                startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                break;

            case R.id.logout:

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.alert_dialog;
                } else {
                    layout = R.layout.alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.app_name));
                    desc.setText("Are You Sure to Logout ?");
                } else {
                    title.setText(getResources().getString(R.string.app_name_ar));
                    desc.setText("هل انت متأكد من تسجيل الخروج؟");
                }

                customDialog = dialogBuilder.create();
                customDialog.show();

                final AlertDialog finalCustomDialog = customDialog;
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        luserId = userId;
                        new GetLogoutApi().execute();
                        userPrefsEditor.clear();

//
//                        finalCustomDialog.dismiss();
                    }
                });

                final AlertDialog finalCustomDialog1 = customDialog;
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finalCustomDialog1.dismiss();

                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

                break;
        }
    }

    private String prepareLogoutJson() {
        JSONObject parentObj = new JSONObject();

        try {
//            parentObj.put("UserType", 2);
            parentObj.put("VendorId", luserId);
            parentObj.put("DeviceToken", SplashScreenActivity.regId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class GetLogoutApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLogoutJson();
            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<LogoutResponce> call = apiService.getLogout(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<LogoutResponce>() {
                @Override
                public void onResponse(Call<LogoutResponce> call, Response<LogoutResponce> response) {
                    if (response.isSuccessful()) {
                        try {
                            LogoutResponce logoutServiceList = response.body();
                            if (logoutServiceList.getStatus()) {
                                String message;
                                if (language.equalsIgnoreCase("En")) {
                                    message = logoutServiceList.getMessageEn();
                                } else {
                                    message = logoutServiceList.getMessageAr();
                                }

                                userPrefsEditor.clear();
                                userPrefsEditor.commit();
                                userId = userPrefs.getString("userId", "0");

                                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

//                                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
//                                Crashlytics.setString(AppLanguage, language/* string value */);
//                                Crashlytics.setString("Device Token", SplashScreenActivity.regId);
//
//                                if (!userId.equals("0")) {
//                                    Crashlytics.setString(UserId, userId/* string value */);
//                                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
//                                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
//                                }

                                Intent a = new Intent(getActivity(), SignInActivity.class);
                                startActivity(a);
                                getActivity().finish();

                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = logoutServiceList.getMessageEn();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), getActivity());
                                } else {
                                    String failureResponse = logoutServiceList.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), getActivity());
                                }
                            }
//                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<LogoutResponce> call, Throwable t) {

                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    public static class NetworkUtil {

        static int TYPE_WIFI = 1;
        static int TYPE_MOBILE = 2;
        static int TYPE_NOT_CONNECTED = 0;

        public static int getConnectivityStatus(Context context) {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
            return TYPE_NOT_CONNECTED;
        }

        public static String getConnectivityStatusString(Context context) {
            int conn = NetworkUtil.getConnectivityStatus(context);
            String status = null;
            if (conn == NetworkUtil.TYPE_WIFI) {
                status = "Wifi enabled";
            } else if (conn == NetworkUtil.TYPE_MOBILE) {
                status = "Mobile data enabled";


            } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
                status = "Not connected to Internet";
            }
            return status;
        }

    }

}
