package com.cs.dr_cafe_vendor.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GoogleMapApiClient {

//    public static final String BASE_URL = "http://csadms.com/CheffAppAPI/api/";
    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/";
//    public static final String BASE_URL = "http://api.thechefapp.net/api/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
