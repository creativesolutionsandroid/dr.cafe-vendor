package com.cs.dr_cafe_vendor.Rest;


import com.cs.dr_cafe_vendor.Models.ChangeLanguageList;
import com.cs.dr_cafe_vendor.Models.ChangePasswordlist;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.Models.DashbordRenuveResponce;
import com.cs.dr_cafe_vendor.Models.DriverAssignCancelList;
import com.cs.dr_cafe_vendor.Models.DriversList;
import com.cs.dr_cafe_vendor.Models.ForgetPassword;
import com.cs.dr_cafe_vendor.Models.LiverTrackingList;
import com.cs.dr_cafe_vendor.Models.LogoutResponce;
import com.cs.dr_cafe_vendor.Models.LogoutServiceList;
import com.cs.dr_cafe_vendor.Models.OrderDetailsList;
import com.cs.dr_cafe_vendor.Models.OrderStatusList;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.Models.ResetPasswordlist;
import com.cs.dr_cafe_vendor.Models.Sign_In;
import com.cs.dr_cafe_vendor.Models.StorelistResponce;
import com.cs.dr_cafe_vendor.Models.TrafficTimeList;
import com.cs.dr_cafe_vendor.Models.UpdateOrderList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
public interface APIInterface {

    @POST("api/Users/Login")
    Call<Sign_In> getSignIn(@Body RequestBody body);

    @POST("api/Users/SendOTP")
    Call<ForgetPassword> forgotPassword(@Body RequestBody body);

    @GET("api/Orders/{vendorId}/{status}")
    Call<OrderTypelist> orderstatus(@Path("vendorId") String string , @Path ("status") String string1, @Query("page") String page , @Query ("size") String size);

    @GET("api/Users/{id}/stores")
    Call<StorelistResponce> getstores(@Path("id") String string );

    @POST("api/users/changepassword")
    Call<ChangePasswordlist> getchangepass(@Body RequestBody body);

    @POST("api/Users/ResetPassword")
    Call<ResetPasswordlist> getresetpass(@Body RequestBody body);

    @POST("api/Orders/Accept")
    Call<UpdateOrderList> getupdateorder(@Body RequestBody body);

    @GET("api/Orders/{id}")
    Call<OrderStatusList> getorderstatus(@Path("id") String string);

    @GET("api/orders/{id}/revenue")
    Call<DashbordRenuveResponce> gettotalrevenu(@Path("id") String string);

    @GET("api/Orders/{id}")
    Call<DashBordResponce> getorderstatus1(@Path("id") String string);

    @POST("OrderInformation/GetOrderTracking")
    Call<OrderDetailsList> getorderdetails(@Body RequestBody body);
//
    @POST("UserAPI/languageChange")
    Call<ChangeLanguageList> getchangelang(@Body RequestBody body);

    @POST("api/Users/Logout")
    Call<LogoutResponce> getLogout(@Body RequestBody body);

    @POST("OrdersAdminUserAPI/GetDriversList")
    Call<DriversList> getDriverList(@Body RequestBody body);

    @POST("OrdersAdminUserAPI/AssignDrivers")
    Call<DriverAssignCancelList> getAssignCancel(@Body RequestBody body);

    @POST("Driver/NewGetLiveTracking")
    Call<LiverTrackingList> getLiveTracking(@Body RequestBody body);


    @POST("api/Orders/Ready")
    Call<UpdateOrderList> getreadyorder(@Body RequestBody body);

    @POST("api/Orders/Serve")
    Call<UpdateOrderList> getserveorder(@Body RequestBody body);

    @POST("api/Orders/Cancel")
    Call<UpdateOrderList> getcancelorder(@Body RequestBody body);

    @GET("api/Orders/{OrderId}/Details")
    Call<OrderDetailsList> getorderdetails(@Path("OrderId") String orderId);

    @GET("distancematrix/json")
    Call<TrafficTimeList> gettraffictimes(@Query("origins") String origin, @Query("destinations") String destination, @Query("departure_time") String departure_time, @Query("duration_in_traffic") String duration_in_traffic, @Query("mode") String mode, @Query("language") String language, @Query("mode") String mode1, @Query("key") String key);

 }
