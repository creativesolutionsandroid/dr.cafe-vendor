package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;


public  class OrderTypelist {


    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("ExpectedDate")
        private String ExpectedDate;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("CAddress")
        private String CAddress;
        @Expose
        @SerializedName("HouseNo")
        private String HouseNo;
        @Expose
        @SerializedName("AddressId")
        private int AddressId;
        @Expose
        @SerializedName("TranStatus")
        private boolean TranStatus;
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("VatPercentage")
        private int VatPercentage;
        @Expose
        @SerializedName("SubTotal")
        private String SubTotal;
        @Expose
        @SerializedName("Vat")
        private double Vat;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private int PaymentMode;
        @Expose
        @SerializedName("ReadyTime")
        private String ReadyTime;
        @Expose
        @SerializedName("AcceptedTime")
        private String AcceptedTime;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("PromoValue")
        private int PromoValue;
        @Expose
        @SerializedName("orderType")
        private String orderType;
        @Expose
        @SerializedName("Total_Price")
        private double Total_Price;
        @Expose
        @SerializedName("status")
        private int status;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("expectedtime")
        private String expectedtime;
        @Expose
        @SerializedName("countofItems")
        private int countofItems;
        @Expose
        @SerializedName("comments")
        private String comments;
        @Expose
        @SerializedName("DeviceToken")
        private String DeviceToken;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("orderid")
        private int orderid;
        @Expose
        @SerializedName("NickName")
        private String NickName;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("userId")
        private int userId;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreName")
        private String StoreName;
        @Expose
        @SerializedName("Tracking")
        private ArrayList<String> Tracking;
        @Expose
        @SerializedName("Items")
        private ArrayList<String> Items;

        public String getExpectedDate() {
            return ExpectedDate;
        }

        public void setExpectedDate(String ExpectedDate) {
            this.ExpectedDate = ExpectedDate;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getCAddress() {
            return CAddress;
        }

        public void setCAddress(String CAddress) {
            this.CAddress = CAddress;
        }

        public String getHouseNo() {
            return HouseNo;
        }

        public void setHouseNo(String HouseNo) {
            this.HouseNo = HouseNo;
        }

        public int getAddressId() {
            return AddressId;
        }

        public void setAddressId(int AddressId) {
            this.AddressId = AddressId;
        }

        public boolean getTranStatus() {
            return TranStatus;
        }

        public void setTranStatus(boolean TranStatus) {
            this.TranStatus = TranStatus;
        }

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String MessageAr) {
            this.MessageAr = MessageAr;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String MessageEn) {
            this.MessageEn = MessageEn;
        }

        public int getVatPercentage() {
            return VatPercentage;
        }

        public void setVatPercentage(int VatPercentage) {
            this.VatPercentage = VatPercentage;
        }

        public String getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(String SubTotal) {
            this.SubTotal = SubTotal;
        }

        public double getVat() {
            return Vat;
        }

        public void setVat(int Vat) {
            this.Vat = Vat;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public int getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(int PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getReadyTime() {
            return ReadyTime;
        }

        public void setReadyTime(String ReadyTime) {
            this.ReadyTime = ReadyTime;
        }

        public String getAcceptedTime() {
            return AcceptedTime;
        }

        public void setAcceptedTime(String AcceptedTime) {
            this.AcceptedTime = AcceptedTime;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public int getPromoValue() {
            return PromoValue;
        }

        public void setPromoValue(int PromoValue) {
            this.PromoValue = PromoValue;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public double getTotal_Price() {
            return Total_Price;
        }

        public void setTotal_Price(double Total_Price) {
            this.Total_Price = Total_Price;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getExpectedtime() {
            return expectedtime;
        }

        public void setExpectedtime(String expectedtime) {
            this.expectedtime = expectedtime;
        }

        public int getCountofItems() {
            return countofItems;
        }

        public void setCountofItems(int countofItems) {
            this.countofItems = countofItems;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getDeviceToken() {
            return DeviceToken;
        }

        public void setDeviceToken(String DeviceToken) {
            this.DeviceToken = DeviceToken;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getNickName() {
            return NickName;
        }

        public void setNickName(String NickName) {
            this.NickName = NickName;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public ArrayList<String> getTracking() {
            return Tracking;
        }

        public void setTracking(ArrayList<String> Tracking) {
            this.Tracking = Tracking;
        }

        public ArrayList<String> getItems() {
            return Items;
        }

        public void setItems(ArrayList<String> Items) {
            this.Items = Items;
        }
    }
}
