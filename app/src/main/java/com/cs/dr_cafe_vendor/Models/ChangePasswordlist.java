package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ChangePasswordlist {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("TransStatus")
        private boolean TransStatus;

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String MessageAr) {
            this.MessageAr = MessageAr;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String MessageEn) {
            this.MessageEn = MessageEn;
        }

        public boolean getTransStatus() {
            return TransStatus;
        }

        public void setTransStatus(boolean TransStatus) {
            this.TransStatus = TransStatus;
        }
    }
}
