package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LiverTrackingList {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("StoreLongitude")
        private String StoreLongitude;
        @Expose
        @SerializedName("StoreLatitude")
        private String StoreLatitude;
        @Expose
        @SerializedName("UserLongitude")
        private String UserLongitude;
        @Expose
        @SerializedName("UserLatitude")
        private String UserLatitude;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("OnlineStatus")
        private boolean OnlineStatus;
        @Expose
        @SerializedName("VehicleNumber")
        private String VehicleNumber;
        @Expose
        @SerializedName("ProfileImage")
        private String ProfileImage;
        @Expose
        @SerializedName("FullAddress")
        private String FullAddress;
        @Expose
        @SerializedName("MobileNo")
        private String MobileNo;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("DriverId")
        private int DriverId;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("Rotation")
        private String rotation;

        public String getStoreLongitude() {
            return StoreLongitude;
        }

        public void setStoreLongitude(String StoreLongitude) {
            this.StoreLongitude = StoreLongitude;
        }

        public String getStoreLatitude() {
            return StoreLatitude;
        }

        public void setStoreLatitude(String StoreLatitude) {
            this.StoreLatitude = StoreLatitude;
        }

        public String getUserLongitude() {
            return UserLongitude;
        }

        public void setUserLongitude(String UserLongitude) {
            this.UserLongitude = UserLongitude;
        }

        public String getUserLatitude() {
            return UserLatitude;
        }

        public void setUserLatitude(String UserLatitude) {
            this.UserLatitude = UserLatitude;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public boolean getOnlineStatus() {
            return OnlineStatus;
        }

        public void setOnlineStatus(boolean OnlineStatus) {
            this.OnlineStatus = OnlineStatus;
        }

        public String getVehicleNumber() {
            return VehicleNumber;
        }

        public void setVehicleNumber(String VehicleNumber) {
            this.VehicleNumber = VehicleNumber;
        }

        public String getProfileImage() {
            return ProfileImage;
        }

        public void setProfileImage(String ProfileImage) {
            this.ProfileImage = ProfileImage;
        }

        public String getFullAddress() {
            return FullAddress;
        }

        public void setFullAddress(String FullAddress) {
            this.FullAddress = FullAddress;
        }

        public String getMobileNo() {
            return MobileNo;
        }

        public void setMobileNo(String MobileNo) {
            this.MobileNo = MobileNo;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String currentDateTime) {
            CurrentDateTime = currentDateTime;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            OrderStatus = orderStatus;
        }

        public String getRotation() {
            return rotation;
        }

        public void setRotation(String rotation) {
            this.rotation = rotation;
        }
    }
}
