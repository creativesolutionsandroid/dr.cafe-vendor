package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriverAssignCancelList {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("BackEndMessage")
    private String BackEndMessage;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getBackEndMessage() {
        return BackEndMessage;
    }

    public void setBackEndMessage(String BackEndMessage) {
        this.BackEndMessage = BackEndMessage;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("DriverList")
        private List<String> DriverList;

        public List<String> getDriverList() {
            return DriverList;
        }

        public void setDriverList(List<String> DriverList) {
            this.DriverList = DriverList;
        }
    }
}
