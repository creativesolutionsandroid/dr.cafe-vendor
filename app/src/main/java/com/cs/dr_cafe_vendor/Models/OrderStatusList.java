package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrderStatusList {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("ReadyOrders")
        private List<ReadyOrders> ReadyOrders;
        @Expose
        @SerializedName("NewOrders")
        private List<String> NewOrders;
        @Expose
        @SerializedName("AcceptedOrders")
        private List<AcceptedOrders> AcceptedOrders;
        @Expose
        @SerializedName("Counters")
        private Counters Counters;

        public List<ReadyOrders> getReadyOrders() {
            return ReadyOrders;
        }

        public void setReadyOrders(List<ReadyOrders> ReadyOrders) {
            this.ReadyOrders = ReadyOrders;
        }

        public List<String> getNewOrders() {
            return NewOrders;
        }

        public void setNewOrders(List<String> NewOrders) {
            this.NewOrders = NewOrders;
        }

        public List<AcceptedOrders> getAcceptedOrders() {
            return AcceptedOrders;
        }

        public void setAcceptedOrders(List<AcceptedOrders> AcceptedOrders) {
            this.AcceptedOrders = AcceptedOrders;
        }

        public Counters getCounters() {
            return Counters;
        }

        public void setCounters(Counters Counters) {
            this.Counters = Counters;
        }
    }

    public static class ReadyOrders {
        @Expose
        @SerializedName("ExpectedDate")
        private String ExpectedDate;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private int PaymentMode;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("orderType")
        private String orderType;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("orderid")
        private int orderid;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreName")
        private String StoreName;

        public String getExpectedDate() {
            return ExpectedDate;
        }

        public void setExpectedDate(String ExpectedDate) {
            this.ExpectedDate = ExpectedDate;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public int getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(int PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }
    }

    public static class AcceptedOrders {
        @Expose
        @SerializedName("ExpectedDate")
        private String ExpectedDate;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private int PaymentMode;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("orderType")
        private String orderType;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("orderid")
        private int orderid;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreName")
        private String StoreName;

        public String getExpectedDate() {
            return ExpectedDate;
        }

        public void setExpectedDate(String ExpectedDate) {
            this.ExpectedDate = ExpectedDate;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public int getPaymentMode() {
            return PaymentMode;
        }

        public void setPaymentMode(int PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }
    }

    public static class Counters {
        @Expose
        @SerializedName("Revenue")
        private double Revenue;
        @Expose
        @SerializedName("TotalOrders")
        private int TotalOrders;
        @Expose
        @SerializedName("Cancelled")
        private int Cancelled;
        @Expose
        @SerializedName("Closed")
        private int Closed;
        @Expose
        @SerializedName("Scheduled")
        private int Scheduled;
        @Expose
        @SerializedName("Ready")
        private int Ready;
        @Expose
        @SerializedName("Accepted")
        private int Accepted;
        @Expose
        @SerializedName("New")
        private int New;

        public double getRevenue() {
            return Revenue;
        }

        public void setRevenue(double Revenue) {
            this.Revenue = Revenue;
        }

        public int getTotalOrders() {
            return TotalOrders;
        }

        public void setTotalOrders(int TotalOrders) {
            this.TotalOrders = TotalOrders;
        }

        public int getCancelled() {
            return Cancelled;
        }

        public void setCancelled(int Cancelled) {
            this.Cancelled = Cancelled;
        }

        public int getClosed() {
            return Closed;
        }

        public void setClosed(int Closed) {
            this.Closed = Closed;
        }

        public int getScheduled() {
            return Scheduled;
        }

        public void setScheduled(int Scheduled) {
            this.Scheduled = Scheduled;
        }

        public int getReady() {
            return Ready;
        }

        public void setReady(int Ready) {
            this.Ready = Ready;
        }

        public int getAccepted() {
            return Accepted;
        }

        public void setAccepted(int Accepted) {
            this.Accepted = Accepted;
        }

        public int getNew() {
            return New;
        }

        public void setNew(int New) {
            this.New = New;
        }
    }
}
