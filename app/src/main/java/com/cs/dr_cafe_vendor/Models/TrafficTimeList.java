package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TrafficTimeList implements Serializable {

    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("rows")
    private ArrayList<Rows> rows;
    @Expose
    @SerializedName("origin_addresses")
    private ArrayList<String> origin_addresses;
    @Expose
    @SerializedName("destination_addresses")
    private ArrayList<String> destination_addresses;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Rows> getRows() {
        return rows;
    }

    public void setRows(ArrayList<Rows> rows) {
        this.rows = rows;
    }

    public ArrayList<String> getOrigin_addresses() {
        return origin_addresses;
    }

    public void setOrigin_addresses(ArrayList<String> origin_addresses) {
        this.origin_addresses = origin_addresses;
    }

    public ArrayList<String> getDestination_addresses() {
        return destination_addresses;
    }

    public void setDestination_addresses(ArrayList<String> destination_addresses) {
        this.destination_addresses = destination_addresses;
    }

    public static class Rows implements Serializable {
        @Expose
        @SerializedName("elements")
        private ArrayList<Elements> elements;

        public ArrayList<Elements> getElements() {
            return elements;
        }

        public void setElements(ArrayList<Elements> elements) {
            this.elements = elements;
        }
    }

    public static class Elements implements Serializable {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("duration_in_traffic")
        private Duration_in_traffic duration_in_traffic;
        @Expose
        @SerializedName("duration")
        private Duration duration;
        @Expose
        @SerializedName("distance")
        private Distance distance;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Duration_in_traffic getDuration_in_traffic() {
            return duration_in_traffic;
        }

        public void setDuration_in_traffic(Duration_in_traffic duration_in_traffic) {
            this.duration_in_traffic = duration_in_traffic;
        }

        public Duration getDuration() {
            return duration;
        }

        public void setDuration(Duration duration) {
            this.duration = duration;
        }

        public Distance getDistance() {
            return distance;
        }

        public void setDistance(Distance distance) {
            this.distance = distance;
        }
    }

    public static class Duration_in_traffic implements Serializable {
        @Expose
        @SerializedName("value")
        private int value;
        @Expose
        @SerializedName("text")
        private String text;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class Duration implements Serializable {
        @Expose
        @SerializedName("value")
        private int value;
        @Expose
        @SerializedName("text")
        private String text;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class Distance implements Serializable {
        @Expose
        @SerializedName("value")
        private int value;
        @Expose
        @SerializedName("text")
        private String text;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
