package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class OrderDetailsList {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("ExpectedDate")
        private String ExpectedDate;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("CAddress")
        private String CAddress;
        @Expose
        @SerializedName("HouseNo")
        private String HouseNo;
        @Expose
        @SerializedName("AddressId")
        private int AddressId;
        @Expose
        @SerializedName("TranStatus")
        private boolean TranStatus;
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("VatPercentage")
        private int VatPercentage;
        @Expose
        @SerializedName("SubTotal")
        private double SubTotal;
        @Expose
        @SerializedName("Vat")
        private double Vat;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentTypeAr")
        private String PaymentTypeAr;
        @Expose
        @SerializedName("PaymentType")
        private String PaymentType;
        @Expose
        @SerializedName("PaymentMode")
        private int PaymentMode;
        @Expose
        @SerializedName("ReadyTime")
        private String ReadyTime;
        @Expose
        @SerializedName("AcceptedTime")
        private String AcceptedTime;
        @Expose
        @SerializedName("TotalPrice")
        private double TotalPrice;
        @Expose
        @SerializedName("PromoValue")
        private double PromoValue;
        @Expose
        @SerializedName("orderTypeAr")
        private String orderTypeAr;
        @Expose
        @SerializedName("orderType")
        private String orderType;
        @Expose
        @SerializedName("Total_Price")
        private String Total_Price;
        @Expose
        @SerializedName("status")
        private int status;
        @Expose
        @SerializedName("OrderStatusAr")
        private String OrderStatusAr;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("expectedtime")
        private String expectedtime;
        @Expose
        @SerializedName("storeId")
        private int storeId;
        @Expose
        @SerializedName("CurrentTime")
        private String CurrentTime;
        @Expose
        @SerializedName("countofItems")
        private int countofItems;
        @Expose
        @SerializedName("comments")
        private String comments;
        @Expose
        @SerializedName("DeviceToken")
        private String DeviceToken;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("OrderDate")
        private String OrderDate;
        @Expose
        @SerializedName("orderid")
        private int orderid;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("userId")
        private int userId;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreName")
        private String StoreName;
        @Expose
        @SerializedName("Tracking")
        private ArrayList<Tracking> Tracking;
        @Expose
        @SerializedName("Items")
        private ArrayList<Items> Items;

        public String getExpectedDate() {
            return ExpectedDate;
        }

        public void setExpectedDate(String ExpectedDate) {
            this.ExpectedDate = ExpectedDate;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getCAddress() {
            return CAddress;
        }

        public void setCAddress(String CAddress) {
            this.CAddress = CAddress;
        }

        public String getHouseNo() {
            return HouseNo;
        }

        public void setHouseNo(String HouseNo) {
            this.HouseNo = HouseNo;
        }

        public int getAddressId() {
            return AddressId;
        }

        public void setAddressId(int AddressId) {
            this.AddressId = AddressId;
        }

        public boolean getTranStatus() {
            return TranStatus;
        }

        public void setTranStatus(boolean TranStatus) {
            this.TranStatus = TranStatus;
        }

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String MessageAr) {
            this.MessageAr = MessageAr;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String MessageEn) {
            this.MessageEn = MessageEn;
        }

        public int getVatPercentage() {
            return VatPercentage;
        }

        public void setVatPercentage(int VatPercentage) {
            this.VatPercentage = VatPercentage;
        }

        public double getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(double SubTotal) {
            this.SubTotal = SubTotal;
        }

        public double getVat() {
            return Vat;
        }

        public void setVat(double Vat) {
            this.Vat = Vat;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public String getPaymentTypeAr() {
            return PaymentTypeAr;
        }

        public void setPaymentTypeAr(String PaymentTypeAr) {
            this.PaymentTypeAr = PaymentTypeAr;
        }

        public String getPaymentType() {
            return PaymentType;
        }

        public void setPaymentType(String PaymentType) {
            this.PaymentType = PaymentType;
        }

        public int getPaymentMode() {
            return PaymentMode;
        }

        public String getCurrentTime() {
            return CurrentTime;
        }

        public void setCurrentTime(String currentTime) {
            CurrentTime = currentTime;
        }

        public void setPaymentMode(int PaymentMode) {
            this.PaymentMode = PaymentMode;
        }

        public String getReadyTime() {
            return ReadyTime;
        }

        public void setReadyTime(String ReadyTime) {
            this.ReadyTime = ReadyTime;
        }

        public String getAcceptedTime() {
            return AcceptedTime;
        }

        public void setAcceptedTime(String AcceptedTime) {
            this.AcceptedTime = AcceptedTime;
        }

        public double getTotalPrice() {
            return TotalPrice;
        }

        public void setTotalPrice(double TotalPrice) {
            this.TotalPrice = TotalPrice;
        }

        public double getPromoValue() {
            return PromoValue;
        }

        public void setPromoValue(double PromoValue) {
            this.PromoValue = PromoValue;
        }

        public String getOrderTypeAr() {
            return orderTypeAr;
        }

        public void setOrderTypeAr(String orderTypeAr) {
            this.orderTypeAr = orderTypeAr;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getTotal_Price() {
            return Total_Price;
        }

        public void setTotal_Price(String Total_Price) {
            this.Total_Price = Total_Price;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getOrderStatusAr() {
            return OrderStatusAr;
        }

        public void setOrderStatusAr(String OrderStatusAr) {
            this.OrderStatusAr = OrderStatusAr;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getExpectedtime() {
            return expectedtime;
        }

        public void setExpectedtime(String expectedtime) {
            this.expectedtime = expectedtime;
        }

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public int getCountofItems() {
            return countofItems;
        }

        public void setCountofItems(int countofItems) {
            this.countofItems = countofItems;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getDeviceToken() {
            return DeviceToken;
        }

        public void setDeviceToken(String DeviceToken) {
            this.DeviceToken = DeviceToken;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOrderDate() {
            return OrderDate;
        }

        public void setOrderDate(String OrderDate) {
            this.OrderDate = OrderDate;
        }

        public int getOrderid() {
            return orderid;
        }

        public void setOrderid(int orderid) {
            this.orderid = orderid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public ArrayList<Tracking> getTracking() {
            return Tracking;
        }

        public void setTracking(ArrayList<Tracking> Tracking) {
            this.Tracking = Tracking;
        }

        public ArrayList<Items> getItems() {
            return Items;
        }

        public void setItems(ArrayList<Items> Items) {
            this.Items = Items;
        }
    }

    public static class Tracking {


        @Expose
        @SerializedName("ActionBy")
        private String ActionBy;
        @Expose
        @SerializedName("AcceptedBy")
        private int AcceptedBy;
        @Expose
        @SerializedName("TrackingTime")
        private String TrackingTime;
        @Expose
        @SerializedName("OrderStatusAr")
        private String OrderStatusAr;
        @Expose
        @SerializedName("OrderStatus")
        private String OrderStatus;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getActionBy() {
            return ActionBy;
        }

        public void setActionBy(String ActionBy) {
            this.ActionBy = ActionBy;
        }

        public int getAcceptedBy() {
            return AcceptedBy;
        }

        public void setAcceptedBy(int AcceptedBy) {
            this.AcceptedBy = AcceptedBy;
        }

        public String getTrackingTime() {
            return TrackingTime;
        }

        public void setTrackingTime(String TrackingTime) {
            this.TrackingTime = TrackingTime;
        }

        public String getOrderStatusAr() {
            return OrderStatusAr;
        }

        public void setOrderStatusAr(String OrderStatusAr) {
            this.OrderStatusAr = OrderStatusAr;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Items {
        @Expose
        @SerializedName("RowNumber")
        private int RowNumber;
        @Expose
        @SerializedName("Amount")
        private double Amount;
        @Expose
        @SerializedName("itemType")
        private String itemType;
        @Expose
        @SerializedName("Size")
        private int Size;
        @Expose
        @SerializedName("ItemPrice")
        private double ItemPrice;
        @Expose
        @SerializedName("comments")
        private String comments;
        @Expose
        @SerializedName("quantity")
        private int quantity;
        @Expose
        @SerializedName("ItemImage")
        private String ItemImage;
        @Expose
        @SerializedName("ItemNo")
        private String ItemNo;
        @Expose
        @SerializedName("ItemNameAr")
        private String ItemNameAr;
        @Expose
        @SerializedName("ItemNameEn")
        private String ItemNameEn;
        @Expose
        @SerializedName("itemId")
        private int itemId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("orderItemID")
        private int orderItemID;
        @Expose
        @SerializedName("Additionals")
        private ArrayList<Additionals> Additionals;

        public int getRowNumber() {
            return RowNumber;
        }

        public void setRowNumber(int RowNumber) {
            this.RowNumber = RowNumber;
        }

        public double getAmount() {
            return Amount;
        }

        public void setAmount(double Amount) {
            this.Amount = Amount;
        }

        public String getItemType() {
            return itemType;
        }

        public void setItemType(String itemType) {
            this.itemType = itemType;
        }

        public int getSize() {
            return Size;
        }

        public void setSize(int Size) {
            this.Size = Size;
        }

        public double getItemPrice() {
            return ItemPrice;
        }

        public void setItemPrice(double ItemPrice) {
            this.ItemPrice = ItemPrice;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getItemImage() {
            return ItemImage;
        }

        public void setItemImage(String ItemImage) {
            this.ItemImage = ItemImage;
        }

        public String getItemNo() {
            return ItemNo;
        }

        public void setItemNo(String ItemNo) {
            this.ItemNo = ItemNo;
        }

        public String getItemNameAr() {
            return ItemNameAr;
        }

        public void setItemNameAr(String ItemNameAr) {
            this.ItemNameAr = ItemNameAr;
        }

        public String getItemNameEn() {
            return ItemNameEn;
        }

        public void setItemNameEn(String ItemNameEn) {
            this.ItemNameEn = ItemNameEn;
        }

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getOrderItemID() {
            return orderItemID;
        }

        public void setOrderItemID(int orderItemID) {
            this.orderItemID = orderItemID;
        }

        public ArrayList<Additionals> getAdditionals() {
            return Additionals;
        }

        public void setAdditionals(ArrayList<Additionals> Additionals) {
            this.Additionals = Additionals;
        }
    }

    public static class Additionals {
        @Expose
        @SerializedName("additionalprice")
        private double additionalprice;
        @Expose
        @SerializedName("AdditionalAr")
        private String AdditionalAr;
        @Expose
        @SerializedName("AdditionalEn")
        private String AdditionalEn;
        @Expose
        @SerializedName("itemId")
        private int itemId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public double getAdditionalprice() {
            return additionalprice;
        }

        public void setAdditionalprice(double additionalprice) {
            this.additionalprice = additionalprice;
        }

        public String getAdditionalAr() {
            return AdditionalAr;
        }

        public void setAdditionalAr(String AdditionalAr) {
            this.AdditionalAr = AdditionalAr;
        }

        public String getAdditionalEn() {
            return AdditionalEn;
        }

        public void setAdditionalEn(String AdditionalEn) {
            this.AdditionalEn = AdditionalEn;
        }

        public int getItemId() {
            return itemId;
        }

        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
