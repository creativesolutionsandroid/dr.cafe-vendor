package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SwipeDeckData implements Serializable {

    private String CurrentTime;

    private String ExpectedDate;

    private String InvoiceNo;

    private String PaymentTypeAr;

    private String PaymentType;

    private int PaymentMode;

    private double TotalPrice;

    private String orderTypeAr;

    private String orderType;

     private String OrderStatusAr;

    private String OrderStatus;

    private String mobile;

    private String OrderDate;

    private int orderid;

    private String fullname;

    private String StoreNameAr;

    private String StoreName;


    public String getCurrentTime() {
        return CurrentTime;
    }

    public void setCurrentTime(String currentTime) {
        CurrentTime = currentTime;
    }

    public String getExpectedDate() {
        return ExpectedDate;
    }

    public void setExpectedDate(String expectedDate) {
        ExpectedDate = expectedDate;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getPaymentTypeAr() {
        return PaymentTypeAr;
    }

    public void setPaymentTypeAr(String paymentTypeAr) {
        PaymentTypeAr = paymentTypeAr;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public int getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(int paymentMode) {
        PaymentMode = paymentMode;
    }

    public double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getOrderTypeAr() {
        return orderTypeAr;
    }

    public void setOrderTypeAr(String orderTypeAr) {
        this.orderTypeAr = orderTypeAr;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatusAr() {
        return OrderStatusAr;
    }

    public void setOrderStatusAr(String orderStatusAr) {
        OrderStatusAr = orderStatusAr;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getStoreNameAr() {
        return StoreNameAr;
    }

    public void setStoreNameAr(String storeNameAr) {
        StoreNameAr = storeNameAr;
    }

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }


}
