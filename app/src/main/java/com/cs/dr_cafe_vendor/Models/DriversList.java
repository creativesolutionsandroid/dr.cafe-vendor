package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DriversList implements Serializable {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("AssignDriverId")
        private ArrayList<AssignDriverId> AssignDriverId;
        @Expose
        @SerializedName("DriverList")
        private ArrayList<DriverList> DriverList;

        public ArrayList<AssignDriverId> getAssignDriverId() {
            return AssignDriverId;
        }

        public void setAssignDriverId(ArrayList<AssignDriverId> AssignDriverId) {
            this.AssignDriverId = AssignDriverId;
        }

        public ArrayList<DriverList> getDriverList() {
            return DriverList;
        }

        public void setDriverList(ArrayList<DriverList> DriverList) {
            this.DriverList = DriverList;
        }
    }

    public static class AssignDriverId implements Serializable {
        @Expose
        @SerializedName("DriverId")
        private int DriverId;

        public int getDriverId() {
            return DriverId;
        }

        public void setDriverId(int DriverId) {
            this.DriverId = DriverId;
        }
    }

    public static class DriverList implements Serializable {
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("Orders")
        private int Orders;
        @Expose
        @SerializedName("Rating")
        private double Rating;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("Stage")
        private int Stage;
        @Expose
        @SerializedName("VehicleImage")
        private String VehicleImage;
        @Expose
        @SerializedName("VehicleNumber")
        private String VehicleNumber;
        @Expose
        @SerializedName("DLimage")
        private String DLimage;
        @Expose
        @SerializedName("DLexpireDate")
        private String DLexpireDate;
        @Expose
        @SerializedName("DLno")
        private String DLno;
        @Expose
        @SerializedName("IqamaImage")
        private String IqamaImage;
        @Expose
        @SerializedName("IqamaExipreDate")
        private String IqamaExipreDate;
        @Expose
        @SerializedName("IqamaNumber")
        private String IqamaNumber;
        @Expose
        @SerializedName("ProfileImage")
        private String ProfileImage;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Mobile")
        private String Mobile;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double distance) {
            Distance = distance;
        }

        public int getOrders() {
            return Orders;
        }

        public void setOrders(int Orders) {
            this.Orders = Orders;
        }

        public double getRating() {
            return Rating;
        }

        public void setRating(double Rating) {
            this.Rating = Rating;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public int getStage() {
            return Stage;
        }

        public void setStage(int Stage) {
            this.Stage = Stage;
        }

        public String getVehicleImage() {
            return VehicleImage;
        }

        public void setVehicleImage(String VehicleImage) {
            this.VehicleImage = VehicleImage;
        }

        public String getVehicleNumber() {
            return VehicleNumber;
        }

        public void setVehicleNumber(String VehicleNumber) {
            this.VehicleNumber = VehicleNumber;
        }

        public String getDLimage() {
            return DLimage;
        }

        public void setDLimage(String DLimage) {
            this.DLimage = DLimage;
        }

        public String getDLexpireDate() {
            return DLexpireDate;
        }

        public void setDLexpireDate(String DLexpireDate) {
            this.DLexpireDate = DLexpireDate;
        }

        public String getDLno() {
            return DLno;
        }

        public void setDLno(String DLno) {
            this.DLno = DLno;
        }

        public String getIqamaImage() {
            return IqamaImage;
        }

        public void setIqamaImage(String IqamaImage) {
            this.IqamaImage = IqamaImage;
        }

        public String getIqamaExipreDate() {
            return IqamaExipreDate;
        }

        public void setIqamaExipreDate(String IqamaExipreDate) {
            this.IqamaExipreDate = IqamaExipreDate;
        }

        public String getIqamaNumber() {
            return IqamaNumber;
        }

        public void setIqamaNumber(String IqamaNumber) {
            this.IqamaNumber = IqamaNumber;
        }

        public String getProfileImage() {
            return ProfileImage;
        }

        public void setProfileImage(String ProfileImage) {
            this.ProfileImage = ProfileImage;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String Mobile) {
            this.Mobile = Mobile;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
