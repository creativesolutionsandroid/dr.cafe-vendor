package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class Sign_In {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("Status")
        private String Status;
        @Expose
        @SerializedName("stores")
        private List<Stores> stores;
        @Expose
        @SerializedName("ModifiedBy")
        private int ModifiedBy;
        @Expose
        @SerializedName("ModifiedOn")
        private String ModifiedOn;
        @Expose
        @SerializedName("CreatedBy")
        private int CreatedBy;
        @Expose
        @SerializedName("CreatedOn")
        private String CreatedOn;
        @Expose
        @SerializedName("isActive")
        private boolean isActive;
        @Expose
        @SerializedName("UserType")
        private int UserType;
        @Expose
        @SerializedName("Lang")
        private String Lang;
        @Expose
        @SerializedName("Phone")
        private String Phone;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public List<Stores> getStores() {
            return stores;
        }

        public void setStores(List<Stores> stores) {
            this.stores = stores;
        }

        public int getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(int ModifiedBy) {
            this.ModifiedBy = ModifiedBy;
        }

        public String getModifiedOn() {
            return ModifiedOn;
        }

        public void setModifiedOn(String ModifiedOn) {
            this.ModifiedOn = ModifiedOn;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public String getCreatedOn() {
            return CreatedOn;
        }

        public void setCreatedOn(String CreatedOn) {
            this.CreatedOn = CreatedOn;
        }

        public boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public int getUserType() {
            return UserType;
        }

        public void setUserType(int UserType) {
            this.UserType = UserType;
        }

        public String getLang() {
            return Lang;
        }

        public void setLang(String Lang) {
            this.Lang = Lang;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Stores {
        @Expose
        @SerializedName("Lng")
        private String Lng;
        @Expose
        @SerializedName("Lat")
        private String Lat;
        @Expose
        @SerializedName("StoreCode")
        private String StoreCode;
        @Expose
        @SerializedName("StoreAddress")
        private String StoreAddress;
        @Expose
        @SerializedName("StoreName")
        private String StoreName;
        @Expose
        @SerializedName("Phone")
        private String Phone;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;

        public String getLng() {
            return Lng;
        }

        public void setLng(String Lng) {
            this.Lng = Lng;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String Lat) {
            this.Lat = Lat;
        }

        public String getStoreCode() {
            return StoreCode;
        }

        public void setStoreCode(String StoreCode) {
            this.StoreCode = StoreCode;
        }

        public String getStoreAddress() {
            return StoreAddress;
        }

        public void setStoreAddress(String StoreAddress) {
            this.StoreAddress = StoreAddress;
        }

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }
    }
}
