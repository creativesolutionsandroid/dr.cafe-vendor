package com.cs.dr_cafe_vendor.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public  class DashbordRenuveResponce  {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("Revenue")
        private double Revenue;
        @Expose
        @SerializedName("TotalOrders")
        private int TotalOrders;
        @Expose
        @SerializedName("Cancelled")
        private int Cancelled;
        @Expose
        @SerializedName("Closed")
        private int Closed;
        @Expose
        @SerializedName("Scheduled")
        private int Scheduled;

        public double getRevenue() {
            return Revenue;
        }

        public void setRevenue(double Revenue) {
            this.Revenue = Revenue;
        }

        public int getTotalOrders() {
            return TotalOrders;
        }

        public void setTotalOrders(int TotalOrders) {
            this.TotalOrders = TotalOrders;
        }

        public int getCancelled() {
            return Cancelled;
        }

        public void setCancelled(int Cancelled) {
            this.Cancelled = Cancelled;
        }

        public int getClosed() {
            return Closed;
        }

        public void setClosed(int Closed) {
            this.Closed = Closed;
        }

        public int getScheduled() {
            return Scheduled;
        }

        public void setScheduled(int Scheduled) {
            this.Scheduled = Scheduled;
        }
    }
}
