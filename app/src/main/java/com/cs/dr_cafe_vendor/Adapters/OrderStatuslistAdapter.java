package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.OrderTypeActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.Models.OrderStatusList;
import com.cs.dr_cafe_vendor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class OrderStatuslistAdapter extends RecyclerView.Adapter<OrderStatuslistAdapter.MyViewHolder> {

    Context context;
    List<DashBordResponce.NewOrders> newOrders ;
    List<DashBordResponce.AcceptedOrders> acceptedOrders;
    List<DashBordResponce.ReadyOrders> readyOrders;
    Activity activity;
    LayoutInflater inflater;
    String userid;
    int value;

    public OrderStatuslistAdapter(Context context, List<DashBordResponce.NewOrders>  newOrders, List<DashBordResponce.AcceptedOrders> acceptedOrders, List<DashBordResponce.ReadyOrders>  readyOrders, int value) {
        this.context = context;
        this.newOrders = newOrders;
        this.acceptedOrders = acceptedOrders;
        this.readyOrders = readyOrders;
        this.value = value;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_dashbord_listi, parent, false);

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SimpleDateFormat current_date = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);




        if (value == 1){
            holder.username.setText("" + newOrders.get(position).getFullname());
            holder.invoice_no.setText(""+newOrders.get(position).getInvoiceNo());
            holder.storename.setText(""+newOrders.get(position).getStoreName());
            holder.payment_option.setText(""+newOrders.get(position).getPaymentType());
            holder.totalcash.setText(""+ Constants.priceFormat1.format(newOrders.get(position).getTotalPrice()));

            if (newOrders.get(position). getPaymentMode()==2){
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            }
            else  {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(newOrders.get(position).getExpectedDate());
                expected_date = dateFormat.format(expect_date);
                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);
            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(newOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

//            orderd_date = dateFormat.format(orderdate);
//
//
//
//            holder.order_date_time.setText("" +orderd_date);


        }else if (value == 2){
            holder.username.setText("" + acceptedOrders.get(position).getFullname());
            holder.invoice_no.setText(""+acceptedOrders.get(position).getInvoiceNo());
            holder.storename.setText(""+acceptedOrders.get(position).getStoreName());
            holder.payment_option.setText(""+acceptedOrders.get(position).getPaymentType());
            holder.totalcash.setText(""+ Constants.priceFormat1.format(acceptedOrders.get(position).getTotalPrice()));
            if (acceptedOrders.get(position). getPaymentMode()==2){
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            }
            else  {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(acceptedOrders.get(position).getExpectedDate());
                expected_date = dateFormat.format(expect_date);

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);

            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(acceptedOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);


            } catch (ParseException e) {
                e.printStackTrace();
            }
//
//            orderd_date = dateFormat.format(orderdate);
//
//            holder.order_date_time.setText("" +orderd_date);


        }else if (value==3){
            holder.username.setText("" + readyOrders.get(position).getFullname());
            holder.invoice_no.setText(""+readyOrders.get(position).getInvoiceNo());
            holder.storename.setText(""+readyOrders.get(position).getStoreName());
            holder.payment_option.setText(""+readyOrders.get(position).getPaymentType());
            holder.totalcash.setText(""+ Constants.decimalFormat.format(readyOrders.get(position).getTotalPrice()));
            if (readyOrders.get(position). getPaymentMode()==2){
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            }
            else  {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(readyOrders.get(position).getExpectedDate());

                expected_date = dateFormat.format(expect_date);

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);



            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(readyOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.delivertime.setText("" + time + "\n" + date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            orderd_date = dateFormat.format(orderdate);

            holder.order_date_time.setText("" +orderd_date);


        }

    }

    @Override
    public int getItemCount() {

        if (value == 1) {
            return newOrders.size();
        } else if (value == 2){
            return acceptedOrders.size();
        } else {
            return readyOrders.size();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username, invoice_no,storename,order_date_time,delivertime,payment_option,totalcash;
        ImageView paymeticon;

        public MyViewHolder(final View convertView) {
            super(convertView);

            username = (TextView) convertView.findViewById(R.id.username);
            invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            storename = (TextView) convertView.findViewById(R.id.storename);
            order_date_time = (TextView) convertView.findViewById(R.id.order_date_time);
            delivertime = (TextView) convertView.findViewById(R.id.delivertime);
            payment_option = (TextView) convertView.findViewById(R.id.payment_option);
            totalcash = (TextView) convertView.findViewById(R.id.totalcash);

            paymeticon=(ImageView)convertView.findViewById( R.id.paymeticon);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(context, OrderTypeActivity.class);
                    context.startActivity(a);

                }
            });
        }

    }

}
