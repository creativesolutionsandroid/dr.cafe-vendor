package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.dr_cafe_vendor.Activities.LiveTrackingActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DriverAssignCancelList;
import com.cs.dr_cafe_vendor.Models.SearchDriverList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;


public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.MyViewHolder> {

    Context context;
    Activity activity;
    LayoutInflater inflater;
    String userid, language, cancel_reason;
    ArrayList<SearchDriverList> orderItems = new ArrayList<>();
    int pos, flag;
    int order_id, brand_id, branch_id;
    String expectedTime, orderstatus;

    public DriverListAdapter(Context context, ArrayList<SearchDriverList> orderItems, String language, int order_id, int brand_id, int branch_id, String userid, String expectedTime, String orderstatus, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.userid = userid;
        this.language = language;
        this.orderItems = orderItems;
        this.order_id = order_id;
        this.brand_id = brand_id;
        this.branch_id = branch_id;
        this.expectedTime = expectedTime;
        this.orderstatus = orderstatus;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.driver_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.driver_list_arabic, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.name.setText("" + orderItems.get(position).getDrivername());
        holder.phone.setText("" + orderItems.get(position).getPhone_no());
        holder.vehicalno.setText("" + orderItems.get(position).getVehical_no());
        holder.orders.setText("No. of Orders : " + orderItems.get(position).getOrders());
        holder.rating.setText("" + orderItems.get(position).getRating());
        holder.KM.setText("" + orderItems.get(position).getKM() + " KM");

        Glide.with(context)
                .load(Constants.DRIVER_IMAGE_URL + orderItems.get(position).getImage())
                .into(holder.driver_pic);

        if (orderItems.size() == 1) {

            holder.assign_cancel.setBackground(context.getResources().getDrawable(R.drawable.canceldriver_bg));
            holder.assign_cancel.setTextColor(context.getResources().getColor(R.color.red));
            holder.assign_cancel.setText("Cancel Driver");
            holder.live_tracking.setVisibility(View.VISIBLE);

        } else {

            holder.assign_cancel.setBackground(context.getResources().getDrawable(R.drawable.assigndriver_bg));
            holder.assign_cancel.setTextColor(context.getResources().getColor(R.color.assign_driver));
            holder.assign_cancel.setText("Assign Driver");
            holder.live_tracking.setVisibility(View.INVISIBLE);

        }

        holder.assign_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pos = position;


                if (holder.assign_cancel.getText().toString().equals("Assign Driver")) {

                    flag = 1;
                    new AssignCancelApi().execute();

                } else {

                    AlertDialog customDialog = null;
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = activity.getLayoutInflater();
                    int layout = 0;
//                    if (language.equalsIgnoreCase("En")) {
                        layout = R.layout.comment_alert_dialog;
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        layout = R.layout.comment_alert_dialog_arabic;
//                    }
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                    final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                    TextView done = (TextView) dialogView.findViewById(R.id.done);


                    customDialog = dialogBuilder.create();
                    customDialog.show();

                    final AlertDialog finalCustomDialog1 = customDialog;
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finalCustomDialog1.dismiss();

                        }
                    });

                    final AlertDialog finalCustomDialog = customDialog;
                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            cancel_reason = comment.getText().toString();
                            if (cancel_reason.equals("") || cancel_reason == null) {
                                cancel_reason = "";
                            }
                            flag = 2;
                            new AssignCancelApi().execute();
                            finalCustomDialog.dismiss();

                        }
                    });

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = activity.getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                }

            }
        });

        holder.live_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent a = new Intent(context, LiveTrackingActivity.class);
                a.putExtra("order_id", order_id);
                a.putExtra("exp_time", expectedTime);
                a.putExtra("orderstatus", orderstatus);
                context.startActivity(a);

            }
        });


    }

    @Override
    public int getItemCount() {
//        if (userid.equalsIgnoreCase("1")) {
//            return allAdminOrders.size();
//        } else {
//            return orderLists.size();
//        }
        return orderItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView rating, name, phone, vehicalno, orders, assign_cancel, live_tracking, KM;
        CircleImageView driver_pic;

        public MyViewHolder(final View convertView) {
            super(convertView);

            rating = (TextView) convertView.findViewById(R.id.rating);
            name = (TextView) convertView.findViewById(R.id.name);
            KM = (TextView) convertView.findViewById(R.id.km);
            phone = (TextView) convertView.findViewById(R.id.phone_no);
            vehicalno = (TextView) convertView.findViewById(R.id.vehical_no);
            orders = (TextView) convertView.findViewById(R.id.orders);
            assign_cancel = (TextView) convertView.findViewById(R.id.assign_cancel);
            live_tracking = (TextView) convertView.findViewById(R.id.live_tracking);

            driver_pic = (CircleImageView) convertView.findViewById(R.id.driver_pic);


//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }

    }


    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("DriverId", orderItems.get(pos).getDrive_id());
            parentObj.put("BrandId", brand_id);
            parentObj.put("BranchId", branch_id);
            parentObj.put("OrderId", order_id);
            parentObj.put("UserId", userid);
            parentObj.put("CancelReason", cancel_reason);
            parentObj.put("Flag", flag);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class AssignCancelApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        AlertDialog customDialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
//            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = activity.getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DriverAssignCancelList> call = apiService.getAssignCancel(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DriverAssignCancelList>() {
                @Override
                public void onResponse(Call<DriverAssignCancelList> call, Response<DriverAssignCancelList> response) {
                    if (response.isSuccessful()) {
                        DriverAssignCancelList changePasswordResponse = response.body();
                        if (changePasswordResponse.getStatus()) {

                            if (flag == 1) {
//                                Intent a = new Intent(context, MainActivity.class);
//                                context.startActivity(a);
                                activity.recreate();
                            } else {
                                activity.recreate();
                            }

                        } else {
                            String failureResponse = changePasswordResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok), activity);
                            } else {
                                showOneButtonAlertDialog(changePasswordResponse.getMessageAr(), context.getResources().getString(R.string.app_name_ar),
                                        context.getResources().getString(R.string.ok_ar), activity);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (customDialog != null) {

                        customDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<DriverAssignCancelList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (customDialog != null) {

                        customDialog.dismiss();

                    }
                }
            });
            return null;
        }
    }

}
