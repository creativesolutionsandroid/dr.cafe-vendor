package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.OrderDetailsList;
import com.cs.dr_cafe_vendor.R;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.MyViewHolder> {

    Context context;
    Activity activity;
    LayoutInflater inflater;
    String userid, language;
    ArrayList<OrderDetailsList.Items> orderItems = new ArrayList<>();
    OrderDetailsAdditionalsAdapter orderDetailsAdditionalsAdapter;

    public OrderDetailsAdapter(Context context, ArrayList<OrderDetailsList.Items> orderItems, String language) {
        this.context = context;
        this.userid = userid;
        this.language = language;
        this.orderItems = orderItems;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_details_list_arabic, parent, false);
        }

        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.item_name_qty.setText("" + orderItems.get(position).getItemNameEn() + " - " + orderItems.get(position).getItemType() + " X " + "" + orderItems.get(position).getQuantity());
            holder.item_price.setText("" + Constants.decimalFormat.format(orderItems.get(position).getItemPrice()) + " SAR");
            if (orderItems.get(position).getComments().equals("")) {
                holder.notes.setText("No notes");
            } else {
                holder.notes.setText("" + orderItems.get(position).getComments());
            }

        } else {
            holder.item_name_qty.setText("" + orderItems.get(position).getQuantity() + " X " + "" + orderItems.get(position).getItemNameAr());
            holder.item_price.setText("" + Constants.decimalFormat.format(orderItems.get(position).getItemPrice()) + " SAR");
            if (orderItems.get(position).getComments().equals("")) {
                holder.notes.setText("ملاحظة : لا ملاحظة");
            } else {
                holder.notes.setText("" + orderItems.get(position).getComments() + "ملاحظة : ");
            }
        }

        Glide.with(context).load(Constants.ITEM_IMAGE_URL + orderItems.get(position).getItemImage()).into(holder.item_img);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(context);
        holder.additional_list.setLayoutManager(layoutManager1);
        if (orderItems.get(position).getAdditionals().size() == 0) {
            holder.additional_layout.setVisibility(View.GONE);
        } else {
            holder.additional_layout.setVisibility(View.VISIBLE);
            orderDetailsAdditionalsAdapter = new OrderDetailsAdditionalsAdapter(context, orderItems.get(position).getAdditionals(), language);
            holder.additional_list.setAdapter(orderDetailsAdditionalsAdapter);
        }

    }

    @Override
    public int getItemCount() {
//        if (userid.equalsIgnoreCase("1")) {
//            return allAdminOrders.size();
//        } else {
//            return orderLists.size();
//        }
        return orderItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item_name_qty, item_price, notes;
        ImageView item_img;
        LinearLayout additional_layout;
        RecyclerView additional_list;

        public MyViewHolder(final View convertView) {
            super(convertView);

            item_name_qty = (TextView) convertView.findViewById(R.id.item_name_qty);
            item_price = (TextView) convertView.findViewById(R.id.item_price);
            notes = (TextView) convertView.findViewById(R.id.note);
            item_img = (ImageView) convertView.findViewById(R.id.item_img);
            additional_layout = (LinearLayout) convertView.findViewById(R.id.additional_layout);
            additional_list = (RecyclerView) convertView.findViewById(R.id.additional_list);


//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent a = new Intent(context, OrderTypeActivity.class);
//                    context.startActivity(a);
//
//                }
//            });
        }

    }

}
