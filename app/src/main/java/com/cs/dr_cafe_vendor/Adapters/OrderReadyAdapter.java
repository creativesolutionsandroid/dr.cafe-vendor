package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class OrderReadyAdapter extends RecyclerView.Adapter<OrderReadyAdapter.MyViewHolder> {

    Context context;
    List<DashBordResponce.ReadyOrders> readyOrders;
    Activity activity;
    LayoutInflater inflater;
    String userid;
    int value;
    long remainingMillis = 60 * 2880 * 1000;
    CountDownTimer countDownTimer;
    String language;

    public OrderReadyAdapter(Context context,List<DashBordResponce.ReadyOrders> readyOrders, int value,String Language) {
        this.context = context;
        this.readyOrders = readyOrders;
        this.value = value;
        this.userid = userid;
        language=Language;
        language=Language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")){

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_dashbord_listi, parent, false);
        }
        else {

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.arabic_adapter_dashbord_listi, parent, false);
        }
//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

        SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


            holder.username.setText("" + readyOrders.get(position).getFullname());
            holder.invoice_no.setText("# " + readyOrders.get(position).getInvoiceNo());
            holder.totalcash.setText("" + Constants.decimalFormat.format(readyOrders.get(position).getTotalPrice()));
            if (readyOrders.get(position).getPaymentMode() == 2) {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            } else {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

        if (language.equals("En")){
            holder.storename.setText("" + readyOrders.get(position).getStoreName());
            holder.payment_option.setText(""+readyOrders.get(position).getPaymentType());
        }
        else {
            holder.storename.setText("" + readyOrders.get(position).getStoreNameAr());
            holder.payment_option.setText(""+readyOrders.get(position).getPaymentTypeAr());
        }


        Date expect_date = null, currentdate = null;

        String expected_date;

            try {
                expect_date = current_date.parse(readyOrders.get(position).getExpectedDate());

                expected_date = dateFormat.format(expect_date);

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);


            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(readyOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        Date startDate = null;



        try {
            currentdate = server_time.parse(Constants.Current_time);
//            startDate = server_time.parse(readyOrders.get(position).getOrderDate());


        } catch (ParseException e) {
            e.printStackTrace();
        }



        final long startMillis = orderdate.getTime();
        final long[] currentMillis = {currentdate.getTime()};
        final long end_millis = expect_date.getTime();

        if (holder.leftTimer != null) {
            holder.leftTimer.cancel();
        }

        if (holder.rightTimer != null) {
            holder.rightTimer.cancel();
        }


        if (currentMillis[0] > end_millis){
            holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutred));
        }
        else{
            holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
        }


//        holder.leftTimer = new CountDownTimer(1000000000, 1000) {
//            // 1000 means, onTick function will be called at every 1000 milliseconds
//
//            @Override
//            public void onTick(long l) {
//                currentMillis[0] = currentMillis[0] + 1000;
//                long leftTimeInMilliseconds = currentMillis[0] - startMillis;
//
//                if (currentMillis[0] > end_millis){
//                    holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutred));
//                }
//                else{
//                    holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//                }
//
//                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                        TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
//
//                holder.dateleft.setText(hms);
//            }
//            @Override
//            public void onFinish() {
//                holder.dateleft.setText("00:00:00");
//            }
//        }.start();

//        holder.rightTimer = new CountDownTimer(1000000000, 1000) {
//            // 1000 means, onTick function will be called at every 1000 milliseconds
//
//            @Override
//            public void onTick(long l) {
////                currentMillis[0] = currentMillis[0] + 1000;
//                long leftTimeInMilliseconds = end_millis - currentMillis[0];
//
//                if (currentMillis[0] > end_millis){
//                    holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//                }
//                else{
//                    holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutgreen));
//                }
//
//                String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                         TimeUnit.MINUTES.toSeconds(1));
//
//                holder.tvTimeLeft.setText(hms);
//            }
//            @Override
//            public void onFinish() {
//                holder.tvTimeLeft.setText("00:00:00");
//            }
//        }.start();




    }




    @Override
    public int getItemCount() {


            return readyOrders.size();


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username, invoice_no, storename, order_date_time, delivertime, payment_option, totalcash;
        TextView dateleft;
        ImageView paymeticon;
        ProgressBar progressBar;
        TextView tvTimeLeft;
        LinearLayout timerlayout,righttlayout ;
        CountDownTimer leftTimer;
        CountDownTimer rightTimer;


        public MyViewHolder(final View convertView) {
            super(convertView);

            username = (TextView) convertView.findViewById(R.id.username);
            invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            storename = (TextView) convertView.findViewById(R.id.storename);
            order_date_time = (TextView) convertView.findViewById(R.id.order_date_time);
            delivertime = (TextView) convertView.findViewById(R.id.delivertime);
            payment_option = (TextView) convertView.findViewById(R.id.payment_option);
            totalcash = (TextView) convertView.findViewById(R.id.totalcash);
            tvTimeLeft = (TextView) convertView.findViewById(R.id.timeLeft);
            dateleft = (TextView) convertView.findViewById(R.id.coutdown);
            paymeticon = (ImageView) convertView.findViewById(R.id.paymeticon);
            timerlayout=(LinearLayout)convertView.findViewById(R.id.timerlayout);
            righttlayout=(LinearLayout)convertView.findViewById(R.id.colorlayout);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String header_title = "", parameter = "";
                    int orderid = 0;

                    if (value == 1) {

                        header_title = "New Order";
                        parameter = "New";
                        orderid = readyOrders.get(getAdapterPosition()).getOrderid();

                    } else if (value == 2) {

                        header_title = "Accepted Order";
                        parameter = "Pending";
                        orderid = readyOrders.get(getAdapterPosition()).getOrderid();

                    } else if (value == 3) {

                        if (language.equalsIgnoreCase("En")){

                            header_title = "Ready Order";
                        }
                        else {

                            header_title = "جاهز الطلبات";
                        }
                        parameter = "Ready";
                        orderid = readyOrders.get(getAdapterPosition()).getOrderid();

                    }

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("header", header_title);
                    a.putExtra("order_id", orderid);
                    a.putExtra("parameter", parameter);

                    context.startActivity(a);

                }
            });
        }

    }


}
