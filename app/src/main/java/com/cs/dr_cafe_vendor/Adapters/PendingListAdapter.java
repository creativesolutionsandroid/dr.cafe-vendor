package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;


//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.LiveTrackingActivity;
import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.Models.PaginationOrderlResponce;
import com.cs.dr_cafe_vendor.Models.UpdateOrderList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;


//***************Puli 5/5/2020*******************

public class PendingListAdapter extends RecyclerView.Adapter<PendingListAdapter.MyViewHolder> {

    Context context;
    ArrayList<PaginationOrderlResponce> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid, language,header;
    SharedPreferences userPrefs;
    LinearLayout colorlayout;

    int pos = -1;

    public PendingListAdapter(Context context, Activity activity, ArrayList<PaginationOrderlResponce> orderLists, String userid, String language,String header) {
        this.context = context;
        this.orderLists = orderLists;
        this.userid = userid;
        this.header = header;
        this.language = language;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_list_adapter, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.arabic_adapter_pending_listi, parent, false);
        }

//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);


        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        Log.i("TAG", "getView: " + userid);

        holder.name.setText("" + orderLists.get(position).getFullname());

        holder.totalcash.setText("" + Constants.priceFormat1.format(orderLists.get(position).getTotalPrice()));
        holder.cashtext.setText(orderLists.get(position).getPaymentType());
        holder.invoicenumber.setText("#"+orderLists.get(position).getInvoiceNo());
        holder.storename.setText(orderLists.get(position).getStoreName());
//        if (orderLists.get(position).getComments().equals("")) {
//            holder.comment.setText("No notes");
//        } else {
//            holder.comment.setText("" + orderLists.get(position).getComments());
//        }
//        holder.comment.setText(orderLists.get(position).getComments());



        if (orderLists.get(position).getPaymentType().equals("Cash")){
            holder.paymetimage.setImageDrawable(context.getDrawable(R.drawable.cash));
        }
        else {
            holder.paymetimage.setImageDrawable(context.getDrawable(R.drawable.visa));
        }

        if (orderLists.get(position).getOrderType().equals("Dine In") || orderLists.get(position).getOrderType().equals("Pick Up")) {
            if (language.equalsIgnoreCase("En")) {
                holder.title_delivery_date.setText("Serving Time / Date");
            }
            else {
                holder.title_delivery_date.setText("موعد و تاريخ التسليم");
            }
        }
        else {
            if (language.equalsIgnoreCase("En")) {
                holder.title_delivery_date.setText("Delivery Time / Date");
            }
            else {
                holder.title_delivery_date.setText("التوصيل  التاريخ   الوقت");
            }
        }


            Date expect_date = null;
        String expected_date;

        try {
            expect_date = current_date.parse(orderLists.get(position).getExpectedDate());
            expected_date = dateFormat.format(expect_date);
            String date, time;

            date = current_date2.format(expect_date);
            time = current_time.format(expect_date);

            holder.exptedtime.setText("" + time + "\n" + date);
            Log.d("TAG", "onBindViewHolder: "+date+time);
        } catch (Exception e) {
//            Crashlytics.logException(e);
            e.printStackTrace();
        }

        Date orderdate = null;
        String orderd_date;

        try {
            orderdate = current_date1.parse(orderLists.get(position).getOrderDate());
            orderd_date = dateFormat.format(orderdate);

            String date, time;

            date = current_date2.format(orderdate);
            time = current_time.format(orderdate);

            holder.orderdate.setText("" + time + "\n" + date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {

        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, mobile, orderdate, price,exptedtime,title_delivery_date,totalcash,invoicenumber,cashtext,storename,comment;
        ImageView paymetimage;
        LinearLayout colorlayout;

        public MyViewHolder(View convertView) {
            super(convertView);

            name = (TextView) convertView.findViewById(R.id.username);
            exptedtime = (TextView) convertView.findViewById(R.id.delivertime);
            title_delivery_date = (TextView) convertView.findViewById(R.id.title_delivery_date);
            orderdate = (TextView) convertView.findViewById(R.id.order_date_time);
            price = (TextView) convertView.findViewById(R.id.price);
            totalcash = (TextView) convertView.findViewById(R.id.totalcash);
            invoicenumber = (TextView) convertView.findViewById(R.id.invoice_no);
            cashtext = (TextView) convertView.findViewById(R.id.payment_option);
            storename = (TextView) convertView.findViewById(R.id.storename);
            comment = (TextView) convertView.findViewById(R.id.comment);
            paymetimage = (ImageView) convertView.findViewById(R.id.paymeticon);
            colorlayout = (LinearLayout) convertView.findViewById(R.id.colorlayout);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String header_title = "", parameter = "";
                    int orderid = 0;

                    if (header.equals("Close")){

                            if (language.equalsIgnoreCase("En")){

                                header_title = "Closed Order";
                            }
                            else {
                                header_title="الطلبات المتمة";
                            }
                        parameter = "Close";
                        orderid = orderLists.get(getAdapterPosition()).getOrderid();

                    } else if (header.equals("Rejected")) {

                        if (language.equalsIgnoreCase("En")){
                            header_title = "Canclled Order";
                        }else {
                            header_title="الطلبات الملغاة";
                        }

                        parameter = "Rejected";
                        orderid = orderLists.get(getAdapterPosition()).getOrderid();

                    }



                        Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("header", header_title);
                    a.putExtra("order_id", orderLists.get(getAdapterPosition()).getOrderid());
                    a.putExtra("parameter", parameter);
                    context.startActivity(a);

                }
            });

        }

    }
}