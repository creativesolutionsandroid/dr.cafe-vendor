package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Fragments.DashBordFragment;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.Models.SwipeDeckData;
import com.cs.dr_cafe_vendor.Models.UpdateOrderList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;

public class SwipeDeckAdapter extends BaseAdapter {

    Activity activity;
    private ArrayList<DashBordResponce.NewOrders> data;
    private Context context;
    String inputStr;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ArrayList<SwipeDeckData> newOrders;
    long remainingMillis = 60 * 1440 * 1000;
    TextView requestCount,accept;
    String language;
    AlertDialog loaderDialog;
    SharedPreferences languagePrefs;
    public LayoutInflater inflater;
    int value;
    TextView ordercount;
    String mcomment = "", order_id = "";
    CountDownTimer countDownTimer;


    public SwipeDeckAdapter(ArrayList<DashBordResponce.NewOrders> data, ArrayList<SwipeDeckData> deckData, Context context, String Language, int value, Activity activity) {
        this.data = data;
        this.activity=activity;
        this.newOrders = deckData;
        this.language=Language;
        this.value=value;
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
//        Log.d("TAG", "getCount: "+newOrders.size());
        return newOrders.size();
    }

    @Override
    public Object getItem(int position) {
        return newOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView username, invoice_no, storename, order_date_time, delivertime, payment_option, totalcash,orderdetails,order_type;
        TextView dateleft;
        ImageView paymeticon;
        ProgressBar progressBar;
        TextView tvTimeLeft;
        LinearLayout colorlayout, mainLayout ;
        ImageView red_image;
        TextView totalcout;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            languagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = languagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                v = inflater.inflate(R.layout.adapter_swipedesk_listi, parent, false);
            } else {
                v = inflater.inflate(R.layout.swipe_adapter_arabic, parent, false);
            }
            userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
            userId = userPrefs.getString("userId", "0");
            Log.d("TAG", "getView: "+userId);

            holder.username = (TextView) v.findViewById(R.id.username);
            holder.invoice_no = (TextView) v.findViewById(R.id.invoice_no);
            holder.storename = (TextView) v.findViewById(R.id.storename);
            holder.order_date_time = (TextView) v.findViewById(R.id.order_date_time);
            holder.delivertime = (TextView) v.findViewById(R.id.delivertime);
            holder.payment_option = (TextView) v.findViewById(R.id.payment_option);
            holder.totalcash = (TextView) v.findViewById(R.id.totalcash);
            holder.tvTimeLeft = (TextView) v.findViewById(R.id.timeLeft);
            holder.dateleft = (TextView) v.findViewById(R.id.coutdown);
            holder.orderdetails = (TextView) v.findViewById(R.id.orderdetails);
            holder.paymeticon = (ImageView) v.findViewById(R.id.paymeticon);
            holder.colorlayout = (LinearLayout) v.findViewById(R.id.leftlayout);
            holder.mainLayout = (LinearLayout) v.findViewById(R.id.layout);

            accept = v.findViewById(R.id.accept);
            holder.progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

            holder.totalcout= (TextView) v.findViewById(R.id.totalcout);
            holder.order_type= (TextView) v.findViewById(R.id.order_type);
            holder.red_image= (ImageView) v.findViewById(R.id.red_image);

            v.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }


        for (int i = 0; i < data.size(); i++) {
            if (newOrders.get(position).getOrderid() == data.get(i).getOrderid()) {
                holder.totalcout.setText((i+1) +"/"+ data.size());
                break;
            }
        }

        SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

        SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

//        if (value == 1) {
            holder.username.setText("" + newOrders.get(position).getFullname());
            holder.invoice_no.setText("# " + newOrders.get(position).getInvoiceNo());
            holder.storename.setText("" + newOrders.get(position).getStoreName());
            holder.order_type.setText("" + newOrders.get(position).getOrderType());
//            Log.d("TAG", "test: "+newOrders.get(position).getOrderType());
//            Log.d("TAG", "test: "+newOrders.get(position).getFullname());

            if (language.equals("En")){
                holder.storename.setText("" + newOrders.get(position).getStoreName());
                holder.payment_option.setText(""+newOrders.get(position).getPaymentType());
            }
            else {
                holder.storename.setText("" + newOrders.get(position).getStoreNameAr());
                holder.payment_option.setText(""+newOrders.get(position).getPaymentTypeAr());
            }

            holder.totalcash.setText("SR  " + Constants.priceFormat1.format(newOrders.get(position).getTotalPrice()));

            if (newOrders.get(position).getPaymentMode() == 2) {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            } else {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(newOrders.get(position).getExpectedDate());
                expected_date = dateFormat.format(expect_date);
                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);
            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;

            try {
                orderdate = current_date1.parse(newOrders.get(position).getOrderDate());

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date currentdate = null;
            String current_timedate;
            Date startDate = null;


            try {
                currentdate = server_time.parse(Constants.Current_time);

            } catch (ParseException e) {
                e.printStackTrace();
            }

//            Log.d("TAG", "orderdate: "+ orderdate);
//            Log.d("TAG", "currentdate: "+ currentdate);

            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};

            long leftTimeInMilliseconds = currentMillis[0] - startMillis;
            if (leftTimeInMilliseconds > 30000){ //30secs
                holder.tvTimeLeft.setText("0");
                holder.tvTimeLeft.setVisibility(View.GONE);
                holder.red_image.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.GONE);
                holder.totalcout.setBackgroundColor(context.getResources().getColor(R.color.red));
                holder.invoice_no.setBackgroundColor(context.getResources().getColor(R.color.red));
                holder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_redbox));
//                countDownTimer = null;
            }
            else {
//                holder.tvTimeLeft.setText("30");
//                progressBar.setSecondaryProgress(30);
                holder.tvTimeLeft.setVisibility(View.VISIBLE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.red_image.setVisibility(View.GONE);
                holder.totalcout.setBackgroundColor(context.getResources().getColor(R.color.layoutblack));
                holder.invoice_no.setBackgroundColor(context.getResources().getColor(R.color.layoutblack));
                holder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_blackbox));

                leftTimeInMilliseconds = 30000 - leftTimeInMilliseconds;
//                final int remTime = (int) remainingMillis / 1000;
//                int percent = ((30 - remTime) / 30) * 100;
//                progressBar.setProgress(percent);

//                if (countDownTimer == null) {
                    countDownTimer = new CountDownTimer(leftTimeInMilliseconds, 1000) {

                        public void onTick(long millisUntilFinished) {

                            currentMillis[0] = currentMillis[0] + 1000;
                            long leftTimeInMilliseconds = currentMillis[0] - startMillis;
                            leftTimeInMilliseconds = 30000 - leftTimeInMilliseconds;

                            int seconds = (int) (leftTimeInMilliseconds / 1000);
                            holder.tvTimeLeft.setText("" + seconds);
                            slideToBottom(holder.tvTimeLeft);
//                            Log.d("TAG", "seconds: " + seconds);
                            holder.progressBar.setSecondaryProgress(seconds);

                            if (seconds <= 5) {
                                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.circular_progress_bar_red));
                                holder.tvTimeLeft.setTextColor(context.getResources().getColor(R.color.red));
                            } else {
                                holder.progressBar.setProgressDrawable(context.getResources().getDrawable(R.drawable.circular_progress_bar));
                                holder.tvTimeLeft.setTextColor(context.getResources().getColor(R.color.layoutblack));
                            }
                        }

                        public void onFinish() {
//                        progressBar.setVisibility(View.GONE);
                            if (countDownTimer != null) {
                                countDownTimer.cancel();
                                holder.tvTimeLeft.setVisibility(View.GONE);
                                holder.red_image.setVisibility(View.GONE);
                                holder.progressBar.setVisibility(View.GONE);
                                holder.totalcout.setBackgroundColor(context.getResources().getColor(R.color.red));
                                holder.invoice_no.setBackgroundColor(context.getResources().getColor(R.color.red));
                                holder.mainLayout.setBackground(context.getResources().getDrawable(R.drawable.bg_redbox));
//                                countDownTimer = null;
                            }
                        }

                    }.start();
                }

//            }
//            }




        holder.orderdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String header_title = "", parameter = "";
                int orderid = 0;

//                if (value == 1) {

                    if (language.equalsIgnoreCase("En")) {

                        header_title = "New Order";
                    } else {

                        header_title = "طلبات جديدة";
                    }
                    parameter = "New";

//                }
                orderid = data.get(position).getOrderid();

                Intent a = new Intent(context, OrderDetailsActivity.class);
                a.putExtra("header", header_title);
                a.putExtra("order_id", orderid);
                a.putExtra("parameter", parameter);

                context.startActivity(a);

            }
        });


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                order_id = String.valueOf(newOrders.get(position).getOrderid());
                Log.d("TAG", "detailsordedrid: "+order_id);


                accept.setClickable(false);
                inputStr = prepareJson(order_id);
                new UpdateOrderApi().execute();

            }
        });


        return v;

    }

    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("TAG", "onBroadcastReceive: UpdateOrder");


        }
    };


        private class UpdateOrderApi extends AsyncTask<String, Integer, String> {

//        String inputStr;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Constants.showLoadingDialog(activity);

            }

            @Override
            protected String doInBackground(String... strings) {
                APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
                Call<UpdateOrderList> call = null;

                call = apiService.getupdateorder(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));


                call.enqueue(new Callback<UpdateOrderList>() {
                    @Override
                    public void onResponse(Call<UpdateOrderList> call, Response<UpdateOrderList> response) {
                        if (response.isSuccessful()) {

                            UpdateOrderList updateorderList = response.body();
                            if (updateorderList.getStatus()) {
                                String message = updateorderList.getMessageEn();
                                Log.i("TAG", "onResponse: " + message);

                                Intent a = new Intent("UpdateOrder");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(a);
                                accept.setClickable(true);
                                //                                Constants.showOneButtonAlertDialog("Request Successful", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), context);
//
//                            } else {
//
//                                Constants.showOneButtonAlertDialog("Order Rejected Successfully", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), context);
//
//                            }
//                            if (OrderListActivity.fa != null) {
//                                OrderListActivity.fa.finish();
//                                Intent a = new Intent(context, OrderListActivity.class);
//                                if (language.equalsIgnoreCase("En")) {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                } else {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                }
//                                a.putExtra("parameter", getIntent().getStringExtra("parameter"));
//                                startActivity(a);
//                                finish();
//                            } else {
//                            activity.finish();
//                            }


                            } else {
                                String failureResponse = updateorderList.getMessageEn();
                                if (language.equalsIgnoreCase("En")) {
                                    showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                            context.getResources().getString(R.string.ok), activity);
                                } else {
                                    showOneButtonAlertDialog(updateorderList.getMessageAr(), context.getResources().getString(R.string.app_name_ar),
                                            context.getResources().getString(R.string.ok_ar), activity);
                                }
                            }
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                        Constants.closeLoadingDialog();
                    }

                    @Override
                    public void onFailure(Call<UpdateOrderList> call, Throwable t) {
                        final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            accept.setClickable(true);
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            accept.setClickable(true);
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                        Constants.closeLoadingDialog();
                    }
                });
                return null;
            }
        }

        private String prepareJson(String orderid) {
            JSONObject parentObj = new JSONObject();
            try {

                parentObj.put("OrderId", orderid);
                parentObj.put("VendorId", userId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("TAG", "prepareJson: " + parentObj);

            return parentObj.toString();
        }

    public static void slideToBottom(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,view.getHeight());
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

}

