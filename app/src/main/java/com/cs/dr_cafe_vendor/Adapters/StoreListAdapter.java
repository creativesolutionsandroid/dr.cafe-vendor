package com.cs.dr_cafe_vendor.Adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.Models.StorelistResponce;
import com.cs.dr_cafe_vendor.R;

import java.util.ArrayList;

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.MyViewHolder> {


    Context context;
    ArrayList<StorelistResponce.Data> orderLists = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;
    String userid, language, header;
    SharedPreferences userPrefs;



    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    public StoreListAdapter(Context context, Activity activity, ArrayList<StorelistResponce.Data> orderLists, String userid, String language) {
        this.context = context;
        this.orderLists = orderLists;
        this.userid = userid;
        this.header = header;
        this.language = language;
        this.activity = activity;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public StoreListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (language.equalsIgnoreCase("En")){

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.store_list_adapter, parent, false);
        }
        else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.store_list_adapter_arabic, parent, false);

        }


        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull StoreListAdapter.MyViewHolder holder, int position) {

        holder.phone_no.setText("" + orderLists.get(position).getPhone());
        holder.storename.setText("" + orderLists.get(position).getStoreName());
        holder.storeadress.setText("" + orderLists.get(position).getStoreAddress());
        String[] storeCode = orderLists.get(position).getStoreCode().split("-");
        holder.storeid.setText("" + storeCode[1]);

    }
    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }


    @Override
    public int getItemCount() {
        return orderLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView   storeid, storename,storeadress,phone_no;
        ImageView calling,location;

        public MyViewHolder(View convertView) {
            super(convertView);


            storeid = (TextView) convertView.findViewById(R.id.storeid);
            storename = (TextView) convertView.findViewById(R.id.storename);
            storeadress = (TextView) convertView.findViewById(R.id.storeadress);
            phone_no = (TextView) convertView.findViewById(R.id.phone_no);
            calling = (ImageView) convertView.findViewById(R.id.calling);
            location = (ImageView) convertView.findViewById(R.id.location);


            calling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentapiVersion = Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        if (!canAccessPhonecalls()) {
                            activity.requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +966" + orderLists.get(getAdapterPosition()).getPhone()));
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            context.startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +966" + orderLists.get(getAdapterPosition()).getPhone()));
                        context.startActivity(intent);
                    }

                }
            });

            location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!orderLists.get(getAdapterPosition()).getLat().equals("") && !orderLists.get(getAdapterPosition()).getLng().equals("")) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + orderLists.get(getAdapterPosition()).getLat() + "," + orderLists.get(getAdapterPosition()).getLng());
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        context.startActivity(mapIntent);
                    }

                }
            });


        }

    }

}
