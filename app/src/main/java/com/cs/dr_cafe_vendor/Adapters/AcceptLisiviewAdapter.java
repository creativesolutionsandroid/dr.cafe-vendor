package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AcceptLisiviewAdapter extends BaseAdapter {

    Context context;

    List<DashBordResponce.AcceptedOrders> acceptedOrders;
    Activity activity;
    LayoutInflater inflater;
    String userid;
    int value;
    String language;
    long remainingMillis = 60 * 2880 * 1000;


    public AcceptLisiviewAdapter(Context context, List<DashBordResponce.AcceptedOrders> newOrders, int value, String Language) {
        this.context = context;
        this.acceptedOrders = newOrders;
        this.acceptedOrders = acceptedOrders;
        this.value = value;
        language=Language;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return acceptedOrders.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public static class ViewHolder {

        TextView username, invoice_no, storename, order_date_time, delivertime, payment_option, totalcash;
        TextView dateleft;
        ImageView paymeticon;
        TextView tvTimeLeft;
        LinearLayout righttlayout ;
        CountDownTimer leftTimer;
        CountDownTimer rightTimer;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.adapter_dashbord_listi, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.arabic_adapter_dashbord_listi, null);
            }

            holder.username = (TextView) convertView.findViewById(R.id.username);
            holder.invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            holder.storename = (TextView) convertView.findViewById(R.id.storename);
            holder.order_date_time = (TextView) convertView.findViewById(R.id.order_date_time);
            holder.delivertime = (TextView) convertView.findViewById(R.id.delivertime);
            holder.payment_option = (TextView) convertView.findViewById(R.id.payment_option);
            holder.totalcash = (TextView) convertView.findViewById(R.id.totalcash);
            holder.tvTimeLeft = (TextView) convertView.findViewById(R.id.timeLeft);
            holder.dateleft = (TextView) convertView.findViewById(R.id.coutdown);
            holder.paymeticon = (ImageView) convertView.findViewById(R.id.paymeticon);

            holder.righttlayout=(LinearLayout)convertView.findViewById(R.id.colorlayout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SimpleDateFormat current_date = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("MM/dd/yyyy hh:mma", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

        SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);

        if (value == 2) {
            holder.username.setText("" + acceptedOrders.get(position).getFullname());
            holder.invoice_no.setText("# " + acceptedOrders.get(position).getInvoiceNo());
            if (language.equals("En")){
                holder.storename.setText("" + acceptedOrders.get(position).getStoreName());
                holder.payment_option.setText("" + acceptedOrders.get(position).getPaymentType());
            }else {
                holder.storename.setText("" + acceptedOrders.get(position).getStoreNameAr());
                holder.payment_option.setText("" + acceptedOrders.get(position).getPaymentTypeAr());
            }


            holder.totalcash.setText("" + Constants.priceFormat1.format(acceptedOrders.get(position).getTotalPrice()));

            if (acceptedOrders.get(position).getPaymentMode() == 2) {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            } else {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null, currentdate = null;
            String expected_date;

            try {
                expect_date = current_date.parse(acceptedOrders.get(position).getExpectedDate());
                expected_date = dateFormat.format(expect_date);

                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);

            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");



            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(acceptedOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);


            } catch (ParseException e) {
                e.printStackTrace();
            }


            String current_timedate;
            Date startDate = null;


            try {
                currentdate = server_time.parse(Constants.Current_time);
                startDate = server_time.parse(acceptedOrders.get(position).getOrderDate());
                current_timedate = dateFormat.format(currentdate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};
            final long end_millis = expect_date.getTime();



            currentMillis[0] = currentMillis[0] + 1000;
            long leftTimeInMilliseconds = currentMillis[0] - startMillis;
            long time90perstange= (long) ((end_millis-startMillis)*(0.9));
            long readyTime = startMillis + time90perstange;
//             Log.d("TAG", "end_millis: "+end_millis);
//             Log.d("TAG", "startMillis: "+startMillis);
//             Log.d("TAG", "90per: "+time90perstange);


            if (currentMillis[0] > readyTime){
                holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutred));
            }
            else{
                holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String header_title = "", parameter = "";
                    int orderid = 0;

                    if (value == 1) {

                        header_title = "New Order";
                        parameter = "New";
                        orderid = acceptedOrders.get(position).getOrderid();

                    } else if (value == 2) {

                        header_title = "Accepted Order";
                        parameter = "Pending";
                        orderid = acceptedOrders.get(position).getOrderid();

                    } else if (value == 3) {

                        header_title = "Ready Order";
                        parameter = "Ready";
                        orderid = acceptedOrders.get(position).getOrderid();
                        Log.d("TAG", "orderid: "+acceptedOrders.get(position).getOrderid());

                    }

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("header", header_title);
                    a.putExtra("order_id", orderid);
                    a.putExtra("parameter", parameter);

                    context.startActivity(a);

                }
            });





//     holder.leftTimer = new CountDownTimer(1000000000, 60000) {
//         // 1000 means, onTick function will be called at every 1000 milliseconds
//
//         @Override
//         public void onTick(long l) {
//             currentMillis[0] = currentMillis[0] + 1000;
//             long leftTimeInMilliseconds = currentMillis[0] - startMillis;
//             long time90perstange= (long) ((end_millis-startMillis)*(0.9));
//             long readyTime = startMillis + time90perstange;
////             Log.d("TAG", "end_millis: "+end_millis);
////             Log.d("TAG", "startMillis: "+startMillis);
////             Log.d("TAG", "90per: "+time90perstange);
//
//
//             if (currentMillis[0] > readyTime){
//                 holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutred));
//             }
//             else{
//                 holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//             }
//
//             String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                     TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
//
//
//             holder.dateleft.setText(hms);
//         }
//         @Override
//         public void onFinish() {
//             holder.dateleft.setText("00:00:00");
//         }
//     }.start();

//            holder.rightTimer = new CountDownTimer(1000000000, 60000) {
//                // 1000 means, onTick function will be called at every 1000 milliseconds
//
//                @Override
//                public void onTick(long l) {
////             currentMillis[0] = currentMillis[0] + 1000;
//                    long leftTimeInMilliseconds = end_millis - currentMillis[0];
//
//                    if (currentMillis[0] > end_millis){
//                        holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//                    }
//                    else{
//                        holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutgreen));
//                    }
//
//                    String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                            TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
//
//                    holder.tvTimeLeft.setText(hms);
//                }
//                @Override
//                public void onFinish() {
//                    holder.tvTimeLeft.setText("00:00:00");
//                }
//            }.start();


        }
//            holder.leftTimer = new CountDownTimer(1000000000, 1000) {
//                // 1000 means, onTick function will be called at every 1000 milliseconds
//
//                @Override
//                public void onTick(long l) {
//                    currentMillis[0] = currentMillis[0] + 1000;
//                    long leftTimeInMilliseconds = currentMillis[0] - startMillis;
//                    long expriry30sce=startMillis+30000;
//                    if (currentMillis[0] > expriry30sce){
//                        holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutred));
//                    }
//                    else{
//                        holder.timerlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//                    }
//
//                    String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                            TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
//
//                    holder.dateleft.setText(hms);
//                }
//                @Override
//                public void onFinish() {
//                    holder.dateleft.setText("00:00:00");
//                }
//            }.start();

//            holder.rightTimer = new CountDownTimer(1000000000, 1000) {
//                // 1000 means, onTick function will be called at every 1000 milliseconds
//
//                @Override
//                public void onTick(long l) {
////                    currentMillis[0] = currentMillis[0] + 1000;
//                    long leftTimeInMilliseconds = end_millis - currentMillis[0];
//
//                    if (currentMillis[0] > end_millis){
//                        holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
//                    }
//                    else{
//                        holder.righttlayout.setBackgroundColor(context.getColor(R.color.layoutgreen));
//                    }
//
//                    String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
//                            TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
//
//                    holder.tvTimeLeft.setText(hms);
//                }
//                @Override
//                public void onFinish() {
//                    holder.tvTimeLeft.setText("00:00:00");
//                }
//            }.start();




        return convertView;
    }
}
