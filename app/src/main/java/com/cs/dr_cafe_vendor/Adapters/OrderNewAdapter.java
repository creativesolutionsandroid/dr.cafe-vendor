package com.cs.dr_cafe_vendor.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;


//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Activities.OrderDetailsActivity;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.DashBordResponce;
import com.cs.dr_cafe_vendor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class OrderNewAdapter extends RecyclerView.Adapter<OrderNewAdapter.MyViewHolder> {

    Context context;
    List<DashBordResponce.NewOrders> newOrders;
    List<DashBordResponce.AcceptedOrders> acceptedOrders;
    List<DashBordResponce.ReadyOrders> readyOrders;
    Activity activity;
    LayoutInflater inflater;
    String userid;
    int value;
    String language;
    long remainingMillis = 60 * 2880 * 1000;


    public OrderNewAdapter(Context context, List<DashBordResponce.NewOrders> newOrders, int value, String Language) {
        this.context = context;
        this.newOrders = newOrders;
        this.acceptedOrders = acceptedOrders;
        this.readyOrders = readyOrders;
        this.value = value;
        language=Language;
        this.userid = userid;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")){

            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_dashbord_listi, parent, false);
        }
        else {
            itemView = LayoutInflater.from(parent.getContext())

                    .inflate(R.layout.arabic_adapter_dashbord_listi, parent, false);
        }//
//        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//        language = languagePrefs.getString("language", "En");


//     Collections.sort(storesArrayList.get(pos).getBrands(), Brands.distanceSort);
        return new MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

        SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


        if (value == 1) {
            holder.username.setText("" + newOrders.get(position).getFullname());
            holder.invoice_no.setText("# " + newOrders.get(position).getInvoiceNo());
            holder.storename.setText("" + newOrders.get(position).getStoreName());

            if (language.equals("En")){
                holder.storename.setText("" + newOrders.get(position).getStoreName());
                holder.payment_option.setText(""+newOrders.get(position).getPaymentType());
            }
            else {
                holder.storename.setText("" + newOrders.get(position).getStoreNameAr());
                holder.payment_option.setText(""+newOrders.get(position).getPaymentTypeAr());
            }

            holder.totalcash.setText("" + Constants.priceFormat1.format(newOrders.get(position).getTotalPrice()));

            if (newOrders.get(position).getPaymentMode() == 2) {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.cash));

            } else {
                holder.paymeticon.setImageDrawable(context.getDrawable(R.drawable.visa));
            }

            Date expect_date = null;
            String expected_date;

            try {
                expect_date = current_date.parse(newOrders.get(position).getExpectedDate());
                expected_date = dateFormat.format(expect_date);
                String date, time;

                date = current_date2.format(expect_date);
                time = current_time.format(expect_date);

                holder.delivertime.setText("" + time + "\n" + date);
            } catch (Exception e) {
//                Crashlytics.logException(e);
                e.printStackTrace();
            }

            Date orderdate = null;
            String orderd_date;

            try {
                orderdate = current_date1.parse(newOrders.get(position).getOrderDate());
                orderd_date = dateFormat.format(orderdate);

                String date, time;

                date = current_date2.format(orderdate);
                time = current_time.format(orderdate);

                holder.order_date_time.setText("" + time + "\n" + date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

         Date currentdate = null;
            String current_timedate;
            Date startDate = null;


            try {
                currentdate = server_time.parse(Constants.Current_time);
                startDate = server_time.parse(newOrders.get(position).getOrderDate());
                current_timedate = dateFormat.format(currentdate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            final long startMillis = orderdate.getTime();
            final long[] currentMillis = {currentdate.getTime()};
            final long end_millis = expect_date.getTime();

            if (holder.leftTimer != null) {
                holder.leftTimer.cancel();
            }

            if (holder.rightTimer != null) {
                holder.rightTimer.cancel();
            }

            long expriry30sce=startMillis+30000;
            if (currentMillis[0] > expriry30sce){
                holder.colorlayout.setBackgroundColor(context.getColor(R.color.layoutred));
            }
            else{
                holder.colorlayout.setBackgroundColor(context.getColor(R.color.layoutblack));
            }
        }
    }



    @Override
    public int getItemCount() {

        return newOrders.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username, invoice_no, storename, order_date_time, delivertime, payment_option, totalcash;
        TextView dateleft;
        ImageView paymeticon;
        ProgressBar progressBar;
        TextView tvTimeLeft;
        LinearLayout colorlayout ;
        CountDownTimer leftTimer;
        CountDownTimer rightTimer;


        public MyViewHolder(final View convertView) {
            super(convertView);

            username = (TextView) convertView.findViewById(R.id.username);
            invoice_no = (TextView) convertView.findViewById(R.id.invoice_no);
            storename = (TextView) convertView.findViewById(R.id.storename);
            order_date_time = (TextView) convertView.findViewById(R.id.order_date_time);
            delivertime = (TextView) convertView.findViewById(R.id.delivertime);
            payment_option = (TextView) convertView.findViewById(R.id.payment_option);
            totalcash = (TextView) convertView.findViewById(R.id.totalcash);
            tvTimeLeft = (TextView) convertView.findViewById(R.id.timeLeft);
            dateleft = (TextView) convertView.findViewById(R.id.coutdown);
            paymeticon = (ImageView) convertView.findViewById(R.id.paymeticon);
            colorlayout=(LinearLayout)convertView.findViewById(R.id.colorlayout);



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String header_title = "", parameter = "";
                    int orderid = 0;

                    if (value == 1) {

                        if (language.equalsIgnoreCase("En")){

                            header_title = "New Order";
                        }
                        else {

                            header_title = "طلبات جديدة";
                        }
                        parameter = "New";
                        orderid = newOrders.get(getAdapterPosition()).getOrderid();

                    } else if (value == 2) {

                        header_title = "Accepted Order";
                        parameter = "Pending";
                        orderid = acceptedOrders.get(getAdapterPosition()).getOrderid();

                    } else if (value == 3) {

                        header_title = "Ready Order";
                        parameter = "Ready";
                        orderid = readyOrders.get(getAdapterPosition()).getOrderid();

                    }

                    Intent a = new Intent(context, OrderDetailsActivity.class);
                    a.putExtra("header", header_title);
                    a.putExtra("order_id", orderid);
                    a.putExtra("parameter", parameter);

                    context.startActivity(a);

                }
            });
        }

    }


}
