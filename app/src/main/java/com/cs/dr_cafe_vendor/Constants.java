package com.cs.dr_cafe_vendor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Constants {

    public static String TAG = "TAG";
    public static String Country_Code = "+966 ";
    public static String UserType = "1";
	public static String STORE_IMAGE_URL = "http://csadms.com/chefvenderappfAppAPI/Uploads/StoreImages/";
//    public static String ITEM_IMAGE_URL = "http://csadms.com/CheffAppAPI/Uploads/ItemImages/";
    public static String ITEM_IMAGE_URL = "http://api.thechefapp.net/Uploads/ItemImages/";
    public static String DRIVER_IMAGE_URL = "http://csadms.com/ChefAppAPITest/Uploads/DriverImages/";
	public static String CATEGORY_IMAGE_URL = "http://csadms.com/chefvenderappfAppAPI/Uploads/CategoryImages/";
	public static String Current_time ;
    public static String IMAGE_URL = "http://csadms.com/chefvenderappfAppAPI/Uploads/StoreImages/";
    public static float vatper = (float) 0.05;
    public static boolean offers = false;
    public static int max_days ;
//    String language, userId;
    SharedPreferences userPrefs;
    public static SharedPreferences languagePrefs;
    public static String language;

    public static AlertDialog customDialog;
    public static AlertDialog customDialog1;

    public static DecimalFormat decimalFormat = new DecimalFormat("0.00");
    public static final DecimalFormat priceFormat1 = new DecimalFormat("##,##,##0.00");


    public static String getDeviceType(Context context){
        String device = "Andr v";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            device = device+version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return device;
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void requestEditTextFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static Typeface getTypeFace(Context context){

        Typeface typeface = Typeface.createFromAsset(context.getAssets(),
                "helvetica.ttf");
        return typeface;
    }

    public static void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
        SharedPreferences languagePrefs;
        String language;
        languagePrefs =  context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);

        language = languagePrefs.getString("language", "En");

        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();

        int layout;


             layout = R.layout.alert_dialog;

        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        no.setVisibility(View.GONE);
        vert.setVisibility(View.GONE);

        title.setText(titleStr);
        yes.setText(buttonStr);
        desc.setText(descriptionStr);

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth*0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public class LocationConstants {
        public static final int SUCCESS_RESULT = 0;

        public static final int FAILURE_RESULT = 1;

        public static final String PACKAGE_NAME = "com.cs.chefvenderapp.";

        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

        public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
        public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
        public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";

    }


    public static String convertToArabic(String value) {
        String newValue = (value
                .replaceAll("١", "1").replaceAll("٢", "2")
                .replaceAll("٣", "3").replaceAll("٤", "4")
                .replaceAll("٥", "5").replaceAll("٦", "6")
                .replaceAll("٧", "7").replaceAll("٨", "8")
                .replaceAll("٩", "9").replaceAll("٠", "0")
                .replaceAll("٫", ".").replaceAll("ص", "AM")
                .replaceAll("م", "PM").replaceAll("،", ","));
        return newValue;
    }

    public static void showLoadingDialog(Activity context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.progress_bar_alert;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static void closeLoadingDialog(){
        if(customDialog != null) {
            customDialog.dismiss();
        }
    }public static void closeLoadingDialog1(){
        if(customDialog != null) {
            customDialog.dismiss();
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }
}
