package com.cs.dr_cafe_vendor.Firebase;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.cs.dr_cafe_vendor.Activities.MainActivity;
import com.cs.dr_cafe_vendor.Activities.SplashScreenActivity;
import com.cs.dr_cafe_vendor.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    String title, icon;
    Intent intent;
    Context mContext;
    Uri sound;


    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        storeRegIdInPref(token);

//        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
//        registrationComplete.putExtra("Token", token);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

//        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
    private void storeRegIdInPref(String Token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", Token);
        SplashScreenActivity.regId = Token;
        editor.commit();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;


        if (remoteMessage.getData().size() > 0) {

            //Message data payload: {tripId=1, kidStatus=0, kidId=1, notificationType=3}
            //Message Notification Body: Your Kid first kid status is In Bus

            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

//            int type = Integer.parseInt(remoteMessage.getData().get("notificationType"));
//            Log.e(TAG, "Message data type " + type);

                sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);

//            if (type == SESSION_STARTED) {
//
//                SessionStartedBroadcast.start(this ,
//                        remoteMessage.getData().get("session_id") ,
//                        remoteMessage.getData().get("date") ,  remoteMessage.getData().get("to")  );
//            }
//            sendNotification(remoteMessage.getData().get(""), remoteMessage.getNotification().getTitle());

            String message = "", title = "";
            Map<String, String> notificationMap = remoteMessage.getData();
            for (Map.Entry<String, String> e : notificationMap.entrySet()) {
                if(e.getKey().equals("title")){
                    title = e.getValue();
                }
                if(e.getKey().equals("body")){
                    message = e.getValue();
                }
            }
            Log.d(TAG, "onMessageReceived: "+message+","+title);

            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra("time", System.currentTimeMillis());
//            resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // This ensures that the back button follows the recommended
            // convention for the back key.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

            // Adds the back stack for the Intent (but not the Intent itself).
            stackBuilder.addParentStack(MainActivity.class);

            // Adds the Intent that starts the Activity to the top of the stack.
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                    0, PendingIntent.FLAG_UPDATE_CURRENT);

            if(!MainActivity.MainActivityVisible) {

                long futureInMillis = System.currentTimeMillis();
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

//                if (Build.VERSION.SDK_INT >= 23) {
//                    alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, resultPendingIntent);
//
//                } else if (Build.VERSION.SDK_INT >= 19) {
//                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, futureInMillis, resultPendingIntent);
//                } else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, resultPendingIntent);
//                }
//                sendNotification(message, title);
//
//                long futureInMillis = System.currentTimeMillis();
//                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//                alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, resultPendingIntent);
                sendNotification(message, title);
            }
            else{
                Intent intent = new Intent("pushrecieved");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//            }
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            String body = remoteMessage.getNotification().getBody();
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification());

//            if (remoteMessage.getNotification().getSound().equals("tone")){
                sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);
//            }
//            else {
//                sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            }
            sendNotification(body, remoteMessage.getNotification().getTitle());
        }
    }

    private void sendNotification(String messageBody, String title) {

        Bitmap remote_picture = null;
        Bitmap large_icon = null;

        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        //        Notification myNotification = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.more_aboutus)
//                .setAutoCancel(true)
//                .setContentTitle("dr.CAFE")
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
//                .setContentText(msg)
//				.setTicker(msg)
//                .setDefaults(NotificationCompat.DEFAULT_ALL).build();
//
//        mNotificationManager.notify(NOTIFICATION_ID, myNotification);

        // Creates an explicit intent for an ResultActivity to receive.
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("time", System.currentTimeMillis());

        // This ensures that the back button follows the recommended
        // convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself).
        stackBuilder.addParentStack(MainActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
        0, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);

        String channelId = getString(R.string.default_notification_channel_id);
        String channelName = getString(R.string.default_notification_channel_name);
        Notification noti = new NotificationCompat.Builder(MyFirebaseMessagingService.this, channelId)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setSmallIcon(R.drawable.app_logo)
                .setColor(Color.parseColor("#000000"))
                .setPriority(Notification.PRIORITY_MAX)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setSound(sound)
                .setContentIntent(resultPendingIntent).build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (sound != null) {
                // Changing Default mode of notification
//                notificatioudioAtnBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
                // Creating an Audio Attribute
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();

                NotificationChannel channel = new NotificationChannel(channelId,
                        channelName,
                        NotificationManager.IMPORTANCE_MAX);
                channel.setSound(sound, audioAttributes);
                notificationManager.createNotificationChannel(channel);
            }
        }
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        wl.acquire(7000);

        notificationManager.notify((int) System.currentTimeMillis(), noti);

//        Notification myNotification = new NotificationCompat.Builder(this)
//        .setSmallIcon(R.drawable.app_logo)
//        .setAutoCancel(true)
//        .setContentIntent(resultPendingIntent)
//        .setContentTitle("dr.Cafe Brew")
//        .setContentText(messageBody)
//        .setTicker(messageBody)
//        .setSound(sound)
//        .setPriority(NotificationCompat.PRIORITY_HIGH)
//        .setChannelId("test")
////                .setDefaults(NotificationCompat.DEFAULT_ALL)
//        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
//        .build();
//
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
////        {
////            String channelId = "Your_channel_id";
////            NotificationChannel channel = new NotificationChannel(
////                    channelId,
////                    "Channel human readable title",
////                    NotificationManager.IMPORTANCE_HIGH);
////            mNotificationManager.createNotificationChannel(channel);
////            myNotification.buil.setChannelId(channelId);
////        }
//        mNotificationManager.notify(1, myNotification);
    }
}