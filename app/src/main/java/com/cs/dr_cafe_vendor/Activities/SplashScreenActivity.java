package com.cs.dr_cafe_vendor.Activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.cs.dr_cafe_vendor.Firebase.Config;
import com.cs.dr_cafe_vendor.R;
import com.google.firebase.messaging.FirebaseMessaging;



public class SplashScreenActivity extends AppCompatActivity {

    public static String regId = "";
    BroadcastReceiver mRegistrationBroadcastReceiver;
    String userid, language;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userid = userPrefs.getString("userId", "0");
//
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
//        languagePrefsEditor = languagePrefs.edit();
//        if(languagePrefs.getBoolean("isAppLoadingFirstTime", true)) {
//            if (Locale.getDefault().getLanguage().equalsIgnoreCase("ar")) {
//                languagePrefsEditor.putString("language", "Ar");
//            }
//            languagePrefsEditor.putBoolean("isAppLoadingFirstTime", false);
//            languagePrefsEditor.commit();
//        }
//

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", "-1");

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }
        };
//
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", "-1");

        Log.i("TAG", "Firebase reg id: " + regId);


//        Crashlytics.getInstance().crash();

//        if (!userid.equalsIgnoreCase("0")) {
//            Log.i("TAG", "onCreate: " + userid);

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    if (!userid.equalsIgnoreCase("0")) {

                        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
//                i.putExtra("class","splash");
                        startActivity(i);

                    } else {

                        Intent i = new Intent(SplashScreenActivity.this, SignInActivity.class);
                        startActivity(i);

                    }

                    // close this activity
                    finish();
                }
            }, 1000);

        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tone);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            NotificationChannel channel = new NotificationChannel(channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_HIGH);
            if (sound != null) {
                AudioAttributes audioAttributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_ALARM)
                        .build();
                channel.setSound(sound, audioAttributes);
            }
            notificationManager.createNotificationChannel(channel);
        }

//        Log.i("TAG", "AutoStartpermissions: " +  AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(SplashScreenActivity.this));
//
//        if (!AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(SplashScreenActivity.this)){
//            AutoStartPermissionHelper.getInstance().getAutoStartPermission(SplashScreenActivity.this);
//        }
//        Log.i("TAG", "AutoStartpermissions: " +  AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(SplashScreenActivity.this));
//
//


//        } else {
//
//            new Handler().postDelayed(new Runnable() {
//
//                /*
//                 * Showing splash screen with a timer. This will be useful when you
//                 * want to show case your app logo / company
//                 */
//
//                @Override
//                public void run() {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    if (!userid.equalsIgnoreCase("0")) {
//
//                        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
////                i.putExtra("class","splash");
//                        startActivity(i);
//
//                    } else {
//
//                        Intent i = new Intent(SplashScreenActivity.this, SignInActivity.class);
//                        startActivity(i);
//
//                    }
//
//                    // close this activity
//                    finish();
//                }
//            }, 5000);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Config.REGISTRATION_COMPLETE));

    }

    @Override
    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
