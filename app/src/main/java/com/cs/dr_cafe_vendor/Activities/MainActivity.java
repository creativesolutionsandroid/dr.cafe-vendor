package com.cs.dr_cafe_vendor.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.cs.dr_cafe_vendor.Adapters.SideMenuAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Fragments.DashBordFragment;
import com.cs.dr_cafe_vendor.Fragments.ProfileScreen;
import com.cs.dr_cafe_vendor.Models.LogoutResponce;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int ACCOUNT_INTENT = 1;
    String language, userId, luserId;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    public static DrawerLayout drawer;
    ImageView closeMenu, logout;

    String[] sideMenuItems;
    Integer[] sideMenuImages;
    ListView sideMenuListView;
    SideMenuAdapter mSideMenuAdapter;
    int itemSelectedPostion = 0;
    private static final String TAG = "TAG";
    AlertDialog loaderDialog;

//    TextView mlanguage;

    SharedPreferences.Editor languagePrefsEditor;
    SharedPreferences.Editor userPrefsEditor;
    public static boolean MainActivityVisible = false;

    private BackgroundService mService;
    private boolean mBound = false;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
            mService = binder.getService(MainActivity.this);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, BackgroundService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection);
            mBound = false;
        }

        super.onStop();
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        languagePrefsEditor = languagePrefs.edit();
        if (language.equals("En")) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_arabic);
        }

//        Button crashButton = new Button(this);
//        crashButton.setText("Crash!");
//        crashButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Crashlytics.getInstance().crash(); // Force a crash
//            }
//        });
//
//        addContentView(crashButton, new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", "0");
        openSettings();

        if (SplashScreenActivity.regId.equals("-1")) {
            SplashScreenActivity.regId = FirebaseInstanceId.getInstance().getToken();
        }

//        navigation = (BottomNavigationView) findViewById(R.id.navigation);
//        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mlanguage = findViewById(R.id.language);

        sideMenuListView = (ListView) findViewById(R.id.side_menu_list_view);
        logout = (ImageView) findViewById(R.id.logout_btn);


//        if (language.equals("En")){
//            mlanguage.setText(getResources().getString(R.string.arabic));
//        }
//        else {
//            mlanguage.setText("En");
//        }

//        mlanguage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (language.equalsIgnoreCase("En")) {
//                    languagePrefsEditor.putString("language", "Ar");
//                    languagePrefsEditor.commit();
//                    language = languagePrefs.getString("language", "En");
////                    new ChangeLanguageApi().execute();
//                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                } else {
//                    languagePrefsEditor.putString("language", "En");
//                    languagePrefsEditor.commit();
//                    language = languagePrefs.getString("language", "En");
////                    new ChangeLanguageApi().execute();
//                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                }
//
//                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";
//
////                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
////                Crashlytics.setString(AppLanguage, language/* string value */);
////                Crashlytics.setString("Device Token", regId);
////
////                if (!userId.equals("0")) {
////                    Crashlytics.setString(UserId, userId/* string value */);
////                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
////                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
////                }
//
//            }
//        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }

        });


        if (language.equals("En")) {
            sideMenuItems = new String[]{

                    getResources().getString(R.string.menu_sheduld),
                    getResources().getString(R.string.menu_delivery),
                    getResources().getString(R.string.menu_cancle),
                    getResources().getString(R.string.menu_profile),
                    getResources().getString(R.string.menu_store),
                    getResources().getString(R.string.arabic),

            };

        } else {
            sideMenuItems = new String[]{

                    getResources().getString(R.string.menu_sheduld_ar),
                    getResources().getString(R.string.menu_delivery_ar),
                    getResources().getString(R.string.menu_cancle_ar),
                    getResources().getString(R.string.menu_profile_ar),
                    getResources().getString(R.string.menu_store_ar),
                    getResources().getString(R.string.menu_En),

            };

        }


        sideMenuImages = new Integer[]{R.drawable.sheduled3x,
                R.drawable.delivered3x,
                R.drawable.cancel3x,
                R.drawable.profile_bg,
                R.drawable.store_icon,
                R.drawable.langueage,};

        mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);

        sideMenuListView.setAdapter(mSideMenuAdapter);
        sideMenuListView.setOnItemClickListener(new DrawerItemClickListener());


        Fragment mainFragment = new DashBordFragment();
        fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            itemSelectedPostion = position;
//            if (position != 4 && position != 6 && position != 7 && position != 8) {
////                mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
//                if (language.equalsIgnoreCase("En")) {
//                    mSideMenuAdapter = new SideMenuAdapter(MainActivity.this, R.layout.list_side_menu, sideMenuItems, sideMenuImages, itemSelectedPostion);
//
//                } else {
//
//                }
//                sideMenuListView.setAdapter(mSideMenuAdapter);
//                mSideMenuAdapter.notifyDataSetChanged();
//            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACCOUNT_INTENT && resultCode == RESULT_OK) {
//            Fragment accountFragment = new AccountFragment();
//            FragmentTransaction ft = fragmentManager.beginTransaction().replace(R.id.fragment_layout, accountFragment);
//            ft.commit();
        } else if (resultCode == RESULT_CANCELED) {

        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    // Method for disabling ShiftMode of BottomNavigationView
    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public void menuClick() {
        if (language.equalsIgnoreCase("En")) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);

            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        } else {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);

            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        }
    }


    @SuppressLint("ResourceType")
    @Override
    protected void onResume() {
        super.onResume();
        MainActivityVisible = true;
//        Menu menu = navigation.getMenu();

    }


    public void showtwoButtonsAlertDialog() {
        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout;
        if (language.equalsIgnoreCase("En")) {
            layout = R.layout.alert_dialog;
        } else {
            layout = R.layout.alert_dialog_arabic;
        }
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
        View vert = (View) dialogView.findViewById(R.id.vert_line);

        if (language.equalsIgnoreCase("En")) {
            title.setText(getResources().getString(R.string.app_name));
            desc.setText("Are You Sure to Logout ?");
        } else {
            title.setText(getResources().getString(R.string.app_name_ar));
            desc.setText("هل انت متأكد من تسجيل الخروج؟");
        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                luserId = userId;
                new GetLogoutApi().execute();
                userPrefsEditor.clear();


            }
        });

        final AlertDialog finalCustomDialog1 = customDialog;
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog1.dismiss();

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = MainActivity.this.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);


    }

    @Override
    protected void onPause() {
        super.onPause();
        MainActivityVisible = false;
    }

    public void selectItem(int position) {
        switch (position) {
            case 0:

//                Intent intent = new Intent(MainActivity.this, OrderTypeActivity.class);
//                intent.putExtra("header", "1");
//                startActivity(intent);

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);

                }
                break;

            case 1:
                Intent intent1 = new Intent(MainActivity.this, OrderListActivity.class);
                intent1.putExtra("header", "Close");
                startActivity(intent1);

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;

            case 2:
                Intent intent2 = new Intent(MainActivity.this, OrderListActivity.class);
                intent2.putExtra("header", "Rejected");
                startActivity(intent2);

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;

            case 3:
                Fragment mainFragment = new ProfileScreen();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, mainFragment).commit();

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;
            case 4:
                Intent intent3 = new Intent(MainActivity.this, StoresActivity.class);
                intent3.putExtra("header", "Rejected");
                startActivity(intent3);

                if (language.equalsIgnoreCase("En")) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;


            case 5:

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = languagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = languagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

//     if (language.equalsIgnoreCase("En")) {
//          drawer.closeDrawer(GravityCompat.START);
//      } else {
//          drawer.closeDrawer(GravityCompat.END);
//      }
                break;
        }
    }

    private class GetLogoutApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLogoutJson();
            Constants.showLoadingDialog(MainActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = ProfileScreen.NetworkUtil.getConnectivityStatusString(MainActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<LogoutResponce> call = apiService.getLogout(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<LogoutResponce>() {
                @Override
                public void onResponse(Call<LogoutResponce> call, Response<LogoutResponce> response) {
                    if (response.isSuccessful()) {
                        try {
                            LogoutResponce logoutServiceList = response.body();
                            if (logoutServiceList.getStatus()) {
                                String message;
                                if (language.equalsIgnoreCase("En")) {
                                    message = logoutServiceList.getMessageEn();
                                } else {
                                    message = logoutServiceList.getMessageAr();
                                }

                                mService.stopUsingService();
                                userPrefsEditor.clear();
                                userPrefsEditor.commit();
                                userId = userPrefs.getString("userId", "0");

                                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

//                                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
//                                Crashlytics.setString(AppLanguage, language/* string value */);
//                                Crashlytics.setString("Device Token", SplashScreenActivity.regId);
//
//                                if (!userId.equals("0")) {
//                                    Crashlytics.setString(UserId, userId/* string value */);
//                                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
//                                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
//                                }

                                Intent a = new Intent(MainActivity.this, SignInActivity.class);
                                startActivity(a);
                                MainActivity.this.finish();

                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

                            } else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = logoutServiceList.getMessageEn();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), MainActivity.this);
                                } else {
                                    String failureResponse = logoutServiceList.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), MainActivity.this);
                                }
                            }
//                            Log.i("TAG", "onResponse: " + data.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<LogoutResponce> call, Throwable t) {

                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MainActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareLogoutJson() {
        JSONObject parentObj = new JSONObject();

        try {
//            parentObj.put("UserType", 2);
            parentObj.put("VendorId", luserId);
            parentObj.put("DeviceToken", SplashScreenActivity.regId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private void openSettings() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

        if (!userPrefs.getBoolean("isFirstTime", false)) {
            openPermission();
        }
    }

    private void openAutoStart() {
        String manufacturer = Build.MANUFACTURER.toLowerCase(Locale.ROOT);
        Intent intent = new Intent();
        if ("xiaomi".equalsIgnoreCase(manufacturer)) {
            intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            alertdialogue(intent);
        } else if ("oppo".equalsIgnoreCase(manufacturer)) {
            intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            alertdialogue(intent);
        } else if ("vivo".equalsIgnoreCase(manufacturer)) {
            intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            alertdialogue(intent);
        } else if ("Letv".equalsIgnoreCase(manufacturer)) {
            intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            alertdialogue(intent);
        } else if ("Honor".equalsIgnoreCase(manufacturer)) {
            intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            alertdialogue(intent);
        }
        else if ("Samsung".equalsIgnoreCase(manufacturer)) {
            openSamsungBackgroundSettings();
        }
    }

    private void alertdialogue(final Intent intent){
        new AlertDialog.Builder(this)
                .setTitle("Please Enable the additional permissions")
                .setMessage("You will not receive notifications while the app is in background if you disable these permissions")
                .setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(intent);
                    }
                })
//                            .setIcon(android.R.drawable.ic_dialog_info)
                .setCancelable(false)
                .show();
    }

    private void openSamsungBackgroundSettings() {
        ArrayList<Intent> possibleIntents = new ArrayList<Intent>();
        String battery1 = "com.samsung.android.sm.ui.battery.BatteryActivity";
        String battery2 = "com.samsung.android.sm.battery.ui.BatteryActivity";
        String pkg = "";
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N)
            pkg = "com.samsung.android.lool";
        else
            pkg = "com.samsung.android.sm";

        Intent intent = new Intent();
        possibleIntents.add(intent.setComponent(new ComponentName(pkg, battery1)));
        possibleIntents.add(intent.setComponent(new ComponentName(pkg, battery2)));
        //general settings as backup
        possibleIntents.add(new Intent(Settings.ACTION_SETTINGS));
        for (int i = 0; i < possibleIntents.size(); i++) {
                try {
                    startActivity(possibleIntents.get(i));
                    return;
                } catch (Exception ex){

                }
            }
    }

    private void openPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                if ("xiaomi".equals(Build.MANUFACTURER.toLowerCase(Locale.ROOT))) {
                    final Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
                    intent.setClassName("com.miui.securitycenter",
                            "com.miui.permcenter.permissions.PermissionsEditorActivity");
                    intent.putExtra("extra_pkgname", getPackageName());
                    new AlertDialog.Builder(this)
                            .setTitle("Please Enable the additional permissions")
                            .setMessage("You will not receive notifications while the app is in background if you disable these permissions")
                            .setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    userPrefsEditor.putBoolean("isFirstTime", true);
                                    userPrefsEditor.commit();
                                    startActivity(intent);
                                }
                            })
//                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setCancelable(false)
                            .show();
                }
                else {
//                    Intent overlaySettings = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
//                    startActivityForResult(overlaySettings, 1111);
                }
            }
        }
    }
}