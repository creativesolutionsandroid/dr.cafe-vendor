package com.cs.dr_cafe_vendor.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.cs.dr_cafe_vendor.Adapters.DriverListAdapter;
import com.cs.dr_cafe_vendor.Models.DriversList;
import com.cs.dr_cafe_vendor.Models.SearchDriverList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;


public class DriverListActivity extends AppCompatActivity {

    Context context;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    Toolbar toolbar;
    ImageView back_btn;
    ArrayList<DriversList.DriverList> driverLists = new ArrayList<>();
    ArrayList<DriversList.AssignDriverId> assignDriverIds = new ArrayList<>();
    ArrayList<SearchDriverList> searchDriverLists = new ArrayList<>();
    DriverListAdapter madapter;
    RecyclerView order_list;
    int order_id, brand_id, branch_id;
    EditText search_text;
    RelativeLayout search_layout;
    String search = "";
    ImageView msearch;
    boolean searchclick;
    TextView emptyView;
    String expectedTime, orderstatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_driver_list);
        } else {
            setContentView(R.layout.activity_driver_list_arabic);
        }

        searchDriverLists.clear();

        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        userId = userPrefs.getString("userId", "0");

        order_id = getIntent().getIntExtra("Orderid", 0);
        brand_id = getIntent().getIntExtra("Brandid", 0);
        branch_id = getIntent().getIntExtra("Branchid", 0);
        expectedTime = getIntent().getStringExtra("exp_time");
        orderstatus = getIntent().getStringExtra("orderstatus");

        back_btn = (ImageView) findViewById(R.id.back_btn);

        order_list = (RecyclerView) findViewById(R.id.order_list);
        msearch = findViewById(R.id.search);
        emptyView = (TextView) findViewById(R.id.empty_view);
        search_text = findViewById(R.id.search_text);
        search_layout = (RelativeLayout) findViewById(R.id.search_layout);

        search_layout.setVisibility(View.GONE);

        emptyView.setVisibility(View.GONE);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        msearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("TAG", "onClick: " + searchclick);

                if (searchclick) {
                    search_layout.setVisibility(View.GONE);
                    searchclick = false;
                } else {
                    search_layout.setVisibility(View.VISIBLE);
                    searchclick = true;
                }

            }
        });


        String networkStatus = NetworkUtil.getConnectivityStatusString(DriverListActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

            new DriverListApi().execute();

        } else {

            if (language.equalsIgnoreCase("En")) {
                Toast.makeText(DriverListActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DriverListActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }

        }

        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
//                if (MenuFragment.mAdapter != null) {
//                    MenuFragment.mAdapter.getFilter().filter(charSequence);
//                }

                Log.i("TAG", "orderhistorylist: " + searchDriverLists.size());
//                Log.i("TAG", "orderhistorylist: " + assignDriverIds.get(0).getDriverId());

                search = search_text.getText().toString().toLowerCase();

                searchDriverLists.clear();
                for (int i = 0; i < driverLists.size(); i++) {

                    if (!search.equals("")) {
                        if (driverLists.get(i).getFullName().toLowerCase().contains(search) ||
                                driverLists.get(i).getFullName().contains(search)) {

                            if (assignDriverIds.size() != 0) {
                                if (driverLists.get(i).getId() == assignDriverIds.get(0).getDriverId()) {

                                    SearchDriverList historySearch = new SearchDriverList();


                                    historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                                    historySearch.setImage(driverLists.get(i).getProfileImage());
                                    historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                                    historySearch.setDrivername(driverLists.get(i).getFullName());
                                    historySearch.setPhone_no(driverLists.get(i).getMobile());
                                    historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                                    historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                                    historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                                    searchDriverLists.add(historySearch);

                                }
                            } else {

                                SearchDriverList historySearch = new SearchDriverList();

                                historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                                historySearch.setImage(driverLists.get(i).getProfileImage());
                                historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                                historySearch.setDrivername(driverLists.get(i).getFullName());
                                historySearch.setPhone_no(driverLists.get(i).getMobile());
                                historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                                historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                                historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                                searchDriverLists.add(historySearch);

                            }


                        }
                    } else {

                        if (assignDriverIds.size() != 0) {
                            if (driverLists.get(i).getId() == assignDriverIds.get(0).getDriverId()) {

                                SearchDriverList historySearch = new SearchDriverList();

                                historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                                historySearch.setImage(driverLists.get(i).getProfileImage());
                                historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                                historySearch.setDrivername(driverLists.get(i).getFullName());
                                historySearch.setPhone_no(driverLists.get(i).getMobile());
                                historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                                historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                                historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                                searchDriverLists.add(historySearch);

                            }
                        } else {

                            SearchDriverList historySearch = new SearchDriverList();

                            historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                            historySearch.setImage(driverLists.get(i).getProfileImage());
                            historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                            historySearch.setDrivername(driverLists.get(i).getFullName());
                            historySearch.setPhone_no(driverLists.get(i).getMobile());
                            historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                            historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                            historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                            searchDriverLists.add(historySearch);

                        }

                    }


                }

                if (searchDriverLists.size() == 0) {

                    order_list.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);

                } else {

                    emptyView.setVisibility(View.GONE);
                    order_list.setVisibility(View.VISIBLE);
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DriverListActivity.this, LinearLayoutManager.VERTICAL, false);
                    order_list.setLayoutManager(linearLayoutManager);

                    madapter = new DriverListAdapter(DriverListActivity.this, searchDriverLists, language, order_id, branch_id, brand_id, userId, expectedTime, orderstatus, DriverListActivity.this);
                    order_list.setAdapter(madapter);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    private String prepareChangePasswordJson() {

        JSONObject parentObj = new JSONObject();
        try {

            parentObj.put("OrderId", order_id);
            parentObj.put("UserId", userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareChangePasswordJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class DriverListApi extends AsyncTask<String, Integer, String> {

        //        ACProgressFlower dialog;
        AlertDialog customDialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();
//            dialog = new ACProgressFlower.Builder(DriverListActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//            Constants.showLoadingDialog(DriverListActivity.this);

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<DriversList> call = apiService.getDriverList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DriversList>() {
                @Override
                public void onResponse(Call<DriversList> call, Response<DriversList> response) {
                    if (response.isSuccessful()) {
                        DriversList changePasswordResponse = response.body();
                        if (changePasswordResponse.getStatus()) {

                            driverLists = changePasswordResponse.getData().getDriverList();
                            assignDriverIds = changePasswordResponse.getData().getAssignDriverId();

                            Log.i("TAG", "onResponse: " + driverLists.size());

                            for (int i = 0; i < driverLists.size(); i++) {

                                if (assignDriverIds.size() != 0) {
                                    if (driverLists.get(i).getId() == assignDriverIds.get(0).getDriverId()) {

                                        SearchDriverList historySearch = new SearchDriverList();

                                        historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                                        historySearch.setImage(driverLists.get(i).getProfileImage());
                                        historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                                        historySearch.setDrivername(driverLists.get(i).getFullName());
                                        historySearch.setPhone_no(driverLists.get(i).getMobile());
                                        historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                                        historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                                        historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                                        searchDriverLists.add(historySearch);

                                    }
                                } else {

                                    SearchDriverList historySearch = new SearchDriverList();

                                    historySearch.setKM(String.valueOf(driverLists.get(i).getDistance()));
                                    historySearch.setImage(driverLists.get(i).getProfileImage());
                                    historySearch.setRating(String.valueOf(driverLists.get(i).getRating()));
                                    historySearch.setDrivername(driverLists.get(i).getFullName());
                                    historySearch.setPhone_no(driverLists.get(i).getMobile());
                                    historySearch.setVehical_no(driverLists.get(i).getVehicleNumber());
                                    historySearch.setOrders(String.valueOf(driverLists.get(i).getOrders()));
                                    historySearch.setDrive_id(String.valueOf(driverLists.get(i).getId()));

                                    searchDriverLists.add(historySearch);

                                }
                            }

                            if (searchDriverLists.size() == 0) {

                                order_list.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);

                            } else {

                                emptyView.setVisibility(View.GONE);
                                order_list.setVisibility(View.VISIBLE);
                                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DriverListActivity.this, LinearLayoutManager.VERTICAL, false);
                                order_list.setLayoutManager(linearLayoutManager);

                                madapter = new DriverListAdapter(DriverListActivity.this, searchDriverLists, language, order_id, branch_id, brand_id, userId, expectedTime, orderstatus, DriverListActivity.this);
                                order_list.setAdapter(madapter);

                            }


                        } else {
                            String failureResponse = changePasswordResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), DriverListActivity.this);
                            } else {
                                showOneButtonAlertDialog(changePasswordResponse.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), DriverListActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(DriverListActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DriverListActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (customDialog != null) {

                        customDialog.dismiss();

                    }
                }

                @Override
                public void onFailure(Call<DriversList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(DriverListActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(DriverListActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DriverListActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(DriverListActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DriverListActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (customDialog != null) {

                        customDialog.dismiss();

                    }
                }
            });
            return null;
        }
    }

}
