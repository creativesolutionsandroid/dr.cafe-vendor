package com.cs.dr_cafe_vendor.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.dr_cafe_vendor.Adapters.PendingListAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Fragments.OrderStatus;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.Models.PaginationOrderlResponce;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListActivity extends AppCompatActivity {

    String language, userId = "";
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    ImageView back_btn;
    boolean loder = true;
    boolean isLoadingFirstTime = true;
    TextView noorderlayout;

    private int PAGE_SIZE = 25;
    private int pageNumber = 1;
    private LinearLayoutManager mLayoutManager;
    private int TOTAL_STORES_COUNT;
    private boolean isSearching = false;


    ImageView titelimage;
    TextView titel, header_desc;

    public static Activity fa;
    ArrayList<OrderTypelist.Data> orderLists = new ArrayList<>();
    ArrayList<OrderTypelist.Data> itemsLists = new ArrayList<>();

    ArrayList<OrderTypelist.Data> dine_inorderLists = new ArrayList<>();
    ArrayList<OrderTypelist.Data> pickuporderLists = new ArrayList<>();
    ArrayList<OrderTypelist.Data> deliveryorderLists = new ArrayList<>();

    RecyclerView mpending_list;
    PendingListAdapter pendingListAdapter;
    private int recyclerViewScrollPosition = 0;

    public static String header, header_title, parameter;
    String header_txt;
    public static int assigndriver;
    private Timer timer = new Timer();
    RecyclerView list_item;

    int page_no = 1, page_size = 10;
    int lastVisibleItem, no_of_rows = 1000, lastvisibleposition = 8;

    ArrayList<PaginationOrderlResponce> paginationOrderlResponces = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_order_list);
        } else {
            setContentView(R.layout.arabic_activity_order_list);
        }
//        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        fa = this;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");

        paginationOrderlResponces.clear();

        back_btn = findViewById(R.id.menu_image);

        titelimage = (ImageView) findViewById(R.id.header_img);
        titel = (TextView) findViewById(R.id.header);
//        header_desc=(TextView)findViewById(R.id.header_desc);
        list_item=(RecyclerView)findViewById(R.id.list_item);

        header_txt = getIntent().getStringExtra("header");
        header = getIntent().getStringExtra("header");
        parameter = getIntent().getStringExtra("parameter");
        header_title = getIntent().getStringExtra("header");

        noorderlayout = (TextView) findViewById(R.id.noorderstext);

        mpending_list = findViewById(R.id.list_item);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        if (header_txt.equals("Close")) {

            if (language.equals("En")){
                titel.setText("Closed Orders");
            }else {
                titel.setText("الطلبات المتمة");
            }

            titelimage.setImageDrawable(getResources().getDrawable(R.drawable.delivered3x));
//            header_desc.setText("Orders that are closed ");

        } else {
            if (language.equals("En")){
                titel.setText("Cancelled Orders");
            }else {
                titel.setText("الطلبات الملغاة");
            }


            titelimage.setImageDrawable(getResources().getDrawable(R.drawable.cancel3x));
//            header_desc.setText("Orders that are cancelled by store / customer ");
        }


        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(OrderListActivity.this);
        mpending_list.setLayoutManager(mLayoutManager);

        mpending_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                lastVisibleItem = mLayoutManager
                        .findLastVisibleItemPosition();
                Log.i("TAG", "onScrolled: " + lastVisibleItem);

                if (lastvisibleposition < lastVisibleItem && lastVisibleItem == (paginationOrderlResponces.size() - 1)) {

                    if ((page_no * page_size) <= no_of_rows) {
//                        if () {

//                        page_size = page_size + 10;
                        page_no = page_no + 1;
                        new Orderstatusapi().execute();
                        lastvisibleposition = lastVisibleItem;


//                        }
                    }
                }
            }
        });


        new Orderstatusapi().execute();

//        LocalBroadcastManager.getInstance(OrderListActivity.this).registerReceiver(
//                mupdate_order, new IntentFilter("UpdateOrder"));
    }

//    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d("TAG", "onBroadcastReceive: UpdateOrder");
//
//            pendingListAdapter = new PendingListAdapter(OrderListActivity.this, OrderListActivity.this, pickuporderLists, userId, language, header_txt);
//            mpending_list.setAdapter(pendingListAdapter);
//
//            pendingListAdapter = new PendingListAdapter(OrderListActivity.this, OrderListActivity.this, deliveryorderLists, userId, language, header_txt);
//            mpending_list.setAdapter(pendingListAdapter);
//
//            new Orderstatusapi().execute();
//
//        }
//    };


    @Override
    public void onPause() {
        super.onPause();
        if (timer != null) {
            timer.cancel();
        }
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if (OrderListActivity.this != null) {

                OrderListActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String networkStatus = OrderStatus.NetworkUtil.getConnectivityStatusString(OrderListActivity.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new Orderstatusapi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OrderListActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderListActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });
            }
        }
    }


    private class Orderstatusapi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (loder) {
                Constants.showLoadingDialog(OrderListActivity.this);
            }
            loder = false;
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            String page, size;

            page = String.valueOf(page_no);
            size = String.valueOf(page_size);

            Call<OrderTypelist> call = apiService.orderstatus(userId, header_txt, page, size);


            call.enqueue(new Callback<OrderTypelist>() {
                @Override
                public void onResponse(Call<OrderTypelist> call, Response<OrderTypelist> response) {

                    Log.i("TAG", "onResponse: " + response.isSuccessful());
                    if (response.isSuccessful()) {
                        OrderTypelist Response = response.body();

                        if (Response.getData().size() > 0) {
                            orderLists = Response.getData();
                            Log.d("TAG", "onResponse: " + response);

//                        pickuporderLists.clear();
//                        deliveryorderLists.clear();
//                        dine_inorderLists.clear();

                            Log.i("TAG", "count: " + orderLists.size());


                            if (orderLists.size() > 0) {
                                noorderlayout.setVisibility(View.GONE);
                                mpending_list.setVisibility(View.VISIBLE);

                                for (int i = 0; i < orderLists.size(); i++) {

                                    PaginationOrderlResponce productMangPagination = new PaginationOrderlResponce();

                                    productMangPagination.setExpectedDate(orderLists.get(i).getExpectedDate());
                                    productMangPagination.setFullname(orderLists.get(i).getFullname());
                                    productMangPagination.setInvoiceNo(orderLists.get(i).getInvoiceNo());
                                    productMangPagination.setMobile(orderLists.get(i).getMobile());
                                    productMangPagination.setOrderDate(orderLists.get(i).getOrderDate());
                                    productMangPagination.setOrderid(orderLists.get(i).getOrderid());
                                    productMangPagination.setOrderStatus(orderLists.get(i).getOrderStatus());
                                    productMangPagination.setOrderType(orderLists.get(i).getOrderType());
                                    productMangPagination.setPaymentMode(orderLists.get(i).getPaymentMode());
                                    productMangPagination.setPaymentType(orderLists.get(i).getPaymentType());
                                    productMangPagination.setStoreName(orderLists.get(i).getStoreName());
                                    productMangPagination.setStoreNameAr(orderLists.get(i).getStoreNameAr());
                                    productMangPagination.setTotalPrice(orderLists.get(i).getTotalPrice());


                                    paginationOrderlResponces.add(productMangPagination);

                                }

                                Log.i("TAG", "pagination: " + (paginationOrderlResponces.size()));

                                pendingListAdapter = new PendingListAdapter(OrderListActivity.this, OrderListActivity.this, paginationOrderlResponces, userId, language, header_txt);
                                mpending_list.setAdapter(pendingListAdapter);
                                    list_item.getLayoutManager().scrollToPosition(paginationOrderlResponces.size() - 10);
                            } else {

                                noorderlayout.setVisibility(View.VISIBLE);
                                mpending_list.setVisibility(View.GONE);

                            }
                        }
                        else {
                            if (loder) {
                                noorderlayout.setVisibility(View.VISIBLE);
                                mpending_list.setVisibility(View.GONE);
                            }
                        }

                        Log.i("TAG", "onResponse: " + Response.getMessage());

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderListActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderListActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderTypelist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderListActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderListActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderListActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderListActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderListActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}


