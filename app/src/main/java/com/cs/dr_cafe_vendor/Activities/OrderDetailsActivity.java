package com.cs.dr_cafe_vendor.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.dr_cafe_vendor.Adapters.OrderDetailsAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Dialogs.CustomDialog;
import com.cs.dr_cafe_vendor.Models.OrderDetailsList;
import com.cs.dr_cafe_vendor.Models.UpdateOrderList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView order_details_total_price, service_type, item_qty, reject, accept, delivery_date_time, order_date_time;
    TextView payment_mode, total_amount, total_amount1, delivery_charger, vat, net_amount, day, user_name, address_txt,promo,subtotal;
    ImageView mobile_no, whats_img, google_img;
    //    TextView month, discount_amount;
    RecyclerView order_details_list;
    String mcomment = "", order_id = "", orderstatus = "", customer_name = "", customer_email = "";
    SharedPreferences userPrefs;
    TextView header_txt;
    String header_txt1;
    OrderDetailsAdapter orderDetailsAdapter;
    //    ImageView back_btn;
//    View view;
    String userId;
    int morder_id;
    //    int assigndriver;
    OrderDetailsList.Data orderDetailsLists;
    TextView invoice_no, order_status;
    int pos = -1;

    SharedPreferences LanguagePrefs;
    String language;
    //    RelativeLayout vendor_delivery_layout, deliver_charge_layout;
    View view4;
    RelativeLayout address_layout;
    NestedScrollView scrollView;
    LinearLayout mobile_no_layout;
    CustomDialog customDialog;
    SimpleDateFormat currentDate = new SimpleDateFormat("dd MMM yyyy", Locale.US);
    SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a", Locale.US);
    ImageView back_btn, header_img;
    CountDownTimer leftTimer;
    CountDownTimer rightTimer;
    LinearLayout colorlayout;
    LinearLayout righttlayout;
    TextView tvTimeLeft;
    TextView dateleft, deliveryDateTitle, deliveryToTitle, storeName;
    String parameter;
    LinearLayout timerlayout;
    String header;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

//    LinearLayout admin_delivery_layout;
    //    double str_vendor_charge, str_totalcharge, str_drivershare, str_chefshare, str_driverpers, str_chefpers;
//    TextView vendor_delivery_charge, customer_charge, vendor_charge, total_charge, driver_share_txt, driver_share, chef_share_txt, chef_share;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activityorder_details);
        } else {
            setContentView(R.layout.activity_order_details);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        customer_email = userPrefs.getString("email", "");
        userId = userPrefs.getString("userId", "0");

        header_txt = findViewById(R.id.header);

        header_txt.setText("" + getIntent().getStringExtra("header"));
        parameter=getIntent().getStringExtra("parameter");
        morder_id = getIntent().getIntExtra("order_id", 0);
        header = getIntent().getStringExtra("header");
        Log.i("TAG", "onCreate: " + morder_id);

//        assigndriver = getIntent().getIntExtra("assigndriver", 0);
//        str_vendor_charge = getIntent().getDoubleExtra("vendorcharge", 0);
//        str_totalcharge = getIntent().getDoubleExtra("totalcharge", 0);
//        str_drivershare = getIntent().getDoubleExtra("drivershare", 0);
//        str_chefshare = getIntent().getDoubleExtra("chefshare", 0);
//        str_driverpers = getIntent().getDoubleExtra("driverpers", 0);
//        str_chefpers = getIntent().getDoubleExtra("chefpers", 0);

        order_details_total_price = findViewById(R.id.order_details_total_amount);
        service_type = findViewById(R.id.service_type);
        item_qty = findViewById(R.id.item_qty);
        reject = findViewById(R.id.reject);
        accept = findViewById(R.id.accept);
//        driver_details = findViewById(R.id.driver_details);
        delivery_date_time = findViewById(R.id.delivery_date_time);
        order_date_time = findViewById(R.id.order_date_time);
        payment_mode = findViewById(R.id.payment_option);
        total_amount = findViewById(R.id.total_amount);
        total_amount1 = findViewById(R.id.total_amount1);
//        discount_amount = findViewById(R.id.discount_amount);
        delivery_charger = findViewById(R.id.delivery_charger);
        vat = findViewById(R.id.vat);
        promo = findViewById(R.id.promo);
        net_amount = findViewById(R.id.net_amount);
//        note = findViewById(R.id.note);
        day = findViewById(R.id.day);
//        month = findViewById(R.id.month);
        user_name = findViewById(R.id.user_name);
        address_txt = findViewById(R.id.address_txt);
        mobile_no = findViewById(R.id.mobile_number);
        google_img = findViewById(R.id.google_img);
        whats_img = findViewById(R.id.whats_img);
        order_details_list = findViewById(R.id.order_details_list);
        back_btn = (ImageView) findViewById(R.id.menu_image);
        subtotal=(TextView)findViewById(R.id.sub_amount) ;
//        view = findViewById(R.id.view1);

        invoice_no = findViewById(R.id.invoice_no);
        order_status = findViewById(R.id.order_status);

        scrollView = (NestedScrollView) findViewById(R.id.scroll_view);
        address_layout = (RelativeLayout) findViewById(R.id.address_layout);
        mobile_no_layout = (LinearLayout) findViewById(R.id.mobile_no_layout);

        colorlayout=(LinearLayout)findViewById(R.id.leftlayout);
        tvTimeLeft = (TextView) findViewById(R.id.timeLeft);
        dateleft = (TextView) findViewById(R.id.coutdown);
        deliveryDateTitle = (TextView) findViewById(R.id.title_delivery_date);
        deliveryToTitle = (TextView) findViewById(R.id.title_delivery_to);
        storeName = (TextView) findViewById(R.id.store_name);
        righttlayout=(LinearLayout)findViewById(R.id.righttlayout);
        timerlayout=(LinearLayout)findViewById(R.id.timerlayout);

        view4 = (View) findViewById(R.id.view4);
        header_txt1 = getIntent().getStringExtra("header");

        header_img = (ImageView) findViewById(R.id.header_img);

        if (getIntent().getStringExtra("parameter").equals("Close")) {
            Log.d("TAG", "header: "+getIntent().getStringExtra("parameter"));
            timerlayout.setVisibility(View.GONE);
            address_layout.setVisibility(View.GONE);
//            mobile_no_layout.setVisibility(View.GONE);
            header_img.setImageDrawable(getResources().getDrawable(R.drawable.delivered3x));
        } else if (getIntent().getStringExtra("parameter").equals("Rejected")) {
            timerlayout.setVisibility(View.GONE);
//            mobile_no_layout.setVisibility(View.GONE);
            header_img.setImageDrawable(getResources().getDrawable(R.drawable.cancel3x));
        } else if (getIntent().getStringExtra("header").equals("New Order")) {
            header_img.setImageDrawable(getResources().getDrawable(R.drawable.new_order3x));
        } else if (getIntent().getStringExtra("header").equals("Accepted Order")) {
            header_img.setImageDrawable(getResources().getDrawable(R.drawable.acceted3x));
        } else if (getIntent().getStringExtra("header").equals("Ready Order")) {
            header_img.setImageDrawable(getResources().getDrawable(R.drawable.ready3x));
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        accept.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this);
        order_details_list.setLayoutManager(mLayoutManager);

        address_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customDialog = new CustomDialog(OrderDetailsActivity.this, orderDetailsLists)
                        .setClickedView(address_txt);
                customDialog.show();
            }
        });

        mobile_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + orderDetailsLists.getMobile()));
                        if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + orderDetailsLists.getMobile()));
                    startActivity(intent);
                }
            }
        });

        google_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!orderDetailsLists.getLatitude().equals("") && !orderDetailsLists.getLongitude().equals("")) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + orderDetailsLists.getLatitude() + "," + orderDetailsLists.getLongitude());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });

        whats_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (orderDetailsLists.getOrderType().equals("Delivery")) {
                    if (!orderDetailsLists.getLatitude().equals("") && !orderDetailsLists.getLongitude().equals("")) {
                        PackageManager pm = getPackageManager();
                        try {

                            Intent waIntent = new Intent(Intent.ACTION_SEND);
                            waIntent.setType("text/plain");
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat current_date = new SimpleDateFormat("yyyy/MM/dd hh:mm a", Locale.US);
                            Date delivery_date = null;
                            String delivery_date1;

                            try {
                                delivery_date = dateFormat.parse(orderDetailsLists.getExpectedDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            delivery_date1 = current_date.format(delivery_date);
                            Log.d("TAG", "onClick: " + delivery_date);

                            double price = Double.parseDouble(orderDetailsLists.getTotal_Price());
                            Log.d("TAG", "onClick: " + Constants.decimalFormat.format(price));

//                        Log.d("TAG", "onClick: "+ "Invoice No : #" + orderDetailsLists.getInvoiceNo() + "\nExpected Time : " + delivery_date1 + "\nPayment Mode : " + orderDetailsLists.getPaymentType() + "\nMobile No : " + orderDetailsLists.getMobile() + "\nTotal Amount : " + Constants.decimalFormat.format(orderDetailsLists.getTotal_Price()) + " SAR" + "\nAddress Details : \n" + orderDetailsLists.getCAddress() + "\n" + "https://www.google.com/maps/search/?api=1&query=" + orderDetailsLists.getLatitude() + "," + orderDetailsLists.getLongitude() + " ");
                            String text = "Invoice No : #" + orderDetailsLists.getInvoiceNo() + "\nExpected Time : " + delivery_date1 + "\nPayment Mode : " + orderDetailsLists.getPaymentType() + "\nMobile No : " + orderDetailsLists.getMobile() + "\nTotal Amount : " + Constants.decimalFormat.format(price) + " SAR" + "\nAddress Details : \n" + orderDetailsLists.getCAddress() + "\n" + "https://www.google.com/maps/search/?api=1&query=" + orderDetailsLists.getLatitude() + "," + orderDetailsLists.getLongitude() + " ";

                            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                            //Check if package exists or not. If not then code
                            //in catch block will be called
                            waIntent.setPackage("com.whatsapp");

                            waIntent.putExtra(Intent.EXTRA_TEXT, text);
                            startActivity(Intent.createChooser(waIntent, "Share with"));

                        } catch (PackageManager.NameNotFoundException e) {
                            Toast.makeText(OrderDetailsActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                }
                else {
//                    if (orderDetailsLists.getLatitude().equals("") && orderDetailsLists.getLongitude().equals("")) {
                        PackageManager pm = getPackageManager();
                        try {
                            Intent waIntent = new Intent(Intent.ACTION_SEND);
                            waIntent.setType("text/plain");
                            waIntent.setType("text/plain");
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat current_date = new SimpleDateFormat("yyyy/MM/dd hh:mm a", Locale.US);
                            Date delivery_date = null;
                            String delivery_date1;

                            try {
                                delivery_date = dateFormat.parse(orderDetailsLists.getExpectedDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            delivery_date1 = current_date.format(delivery_date);
                            Log.d("TAG", "onClick: "+delivery_date);

                            double price = Double.parseDouble(orderDetailsLists.getTotal_Price());
                            Log.d("TAG", "onClick: "+Constants.decimalFormat.format(price));

//                        Log.d("TAG", "onClick: "+ "Invoice No : #" + orderDetailsLists.getInvoiceNo() + "\nExpected Time : " + delivery_date1 + "\nPayment Mode : " + orderDetailsLists.getPaymentType() + "\nMobile No : " + orderDetailsLists.getMobile() + "\nTotal Amount : " + Constants.decimalFormat.format(orderDetailsLists.getTotal_Price()) + " SAR" + "\nAddress Details : \n" + orderDetailsLists.getCAddress() + "\n" + "https://www.google.com/maps/search/?api=1&query=" + orderDetailsLists.getLatitude() + "," + orderDetailsLists.getLongitude() + " ");
                            String text = "Invoice No : #" + orderDetailsLists.getInvoiceNo() + "\nExpected Time : " + delivery_date1 + "\nPayment Mode : " + orderDetailsLists.getPaymentType() + "\nMobile No : " + orderDetailsLists.getMobile() + "\nTotal Amount : " + Constants.decimalFormat.format(price) + "\nOrder Type : " + orderDetailsLists.getOrderType()  + "\nAddress Details : \n" + orderDetailsLists.getStoreName();

                            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                            //Check if package exists or not. If not then code
                            //in catch block will be called
                            waIntent.setPackage("com.whatsapp");

                            waIntent.putExtra(Intent.EXTRA_TEXT, text);
                            startActivity(Intent.createChooser(waIntent, "Share with"));

                        } catch (PackageManager.NameNotFoundException e) {
                            Toast.makeText(OrderDetailsActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                                    .show();
                        }
//                    }
                }
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog customDialog = null;
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetailsActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = 0;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.comment_alert_dialog;
                } else if (language.equalsIgnoreCase("Ar")) {
                    layout = R.layout.comment_alert_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
                final EditText comment = (EditText) dialogView.findViewById(R.id.comment);
                TextView done = (TextView) dialogView.findViewById(R.id.done);

                customDialog = dialogBuilder.create();
                customDialog.show();


                final AlertDialog finalCustomDialog1 = customDialog;
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finalCustomDialog1.dismiss();

                    }
                });

                final AlertDialog finalCustomDialog = customDialog;
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (comment.getText().toString().equals("")) {

                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog("Please enter reason", getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OrderDetailsActivity.this);
                            } else {
                                showOneButtonAlertDialog("الرجاء كتابة السبب", getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);

                            }

                        } else {

                            order_id = String.valueOf(orderDetailsLists.getOrderid());
                            mcomment = comment.getText().toString();

                            new CancelOrderApi().execute();

                            finalCustomDialog.dismiss();

                        }

                    }
                });


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);

            }
        });



//        driver_details.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//                SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
//
//                String date1;
//                Date date = null;
//
//                try {
//                    date = dateFormat.parse(orderDetailsLists.get(0).getExpectedTime());
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//
//                date1 = dateFormat1.format(date);
//
//                Intent a = new Intent(OrderDetailsActivity.this, DriverListActivity.class);
//                a.putExtra("Brandid", orderDetailsLists.get(0).getBrandId());
//                a.putExtra("Branchid", orderDetailsLists.get(0).getBranchId());
//                a.putExtra("Orderid", orderDetailsLists.get(0).getOrderId());
//                a.putExtra("exp_time", date1);
//                a.putExtra("orderstatus", orderDetailsLists.get(0).getOrderStatus());
//                startActivity(a);
//
//            }
//        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (accept.getText().toString().equals("Locate")) {
//
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
//                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a", Locale.US);
//
//                    String date1;
//                    Date date = null;
//
//                    try {
//                        date = dateFormat.parse(orderDetailsLists.get(0).getExpectedTime());
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//
//                    date1 = dateFormat1.format(date);
//
//                    Intent a = new Intent(OrderDetailsActivity.this, LiveTrackingActivity.class);
//                    a.putExtra("order_id", orderDetailsLists.get(0).getOrderId());
//                    a.putExtra("exp_time", date1);
//                    a.putExtra("orderstatus", orderDetailsLists.get(0).getOrderStatus());
//                    startActivity(a);
//
//
//                } else {

                mcomment = "";
//                for (int i = 0; i < orderDetailsLists.size(); i++) {
                order_id = String.valueOf(orderDetailsLists.getOrderid());
                Log.d("TAG", "detailsordedrid: "+order_id);
//                }
                new UpdateOrderApi().execute();

//                }
            }
        });

        new OrderdetailsApi().execute();

    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + orderDetailsLists.getMobile()));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(OrderDetailsActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.i("TAG", "onBackPressed: ");
//        if (OrderListActivity.fa != null) {
//            OrderListActivity.fa.finish();
//            Intent a = new Intent(OrderDetailsActivity.this, OrderListActivity.class);
//            if (language.equalsIgnoreCase("En")) {
//                a.putExtra("header", getIntent().getStringExtra("header"));
//            } else {
//                a.putExtra("header", getIntent().getStringExtra("header"));
//            }
//            a.putExtra("parameter", getIntent().getStringExtra("parameter"));
//            startActivity(a);
//            finish();
//        } else {
        finish();
//        }

    }

    private String prepareJson() {
        JSONObject parentObj = new JSONObject();
        try {

            parentObj.put("OrderId", morder_id);
            parentObj.put("VendorId", userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private String cancelprepareJson() {
        JSONObject parentObj = new JSONObject();


        try {

            parentObj.put("OrderId", morder_id);
            parentObj.put("VendorId", userId);
            parentObj.put("Reason", mcomment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareJson: " + parentObj);

        return parentObj.toString();
    }

    private class UpdateOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
            Call<UpdateOrderList> call = null;
            if (orderDetailsLists.getOrderStatus().equals("New")) {
                call = apiService.getupdateorder(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            } else if (orderDetailsLists.getOrderStatus().equals("Accepted")) {
                call = apiService.getreadyorder(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            } else if (orderDetailsLists.getOrderStatus().equals("Ready")) {
                call = apiService.getserveorder(
                        RequestBody.create(MediaType.parse("application/json"), inputStr));
            }

            call.enqueue(new Callback<UpdateOrderList>() {
                @Override
                public void onResponse(Call<UpdateOrderList> call, Response<UpdateOrderList> response) {
                    if (response.isSuccessful()) {
                        UpdateOrderList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessageEn();
                            Log.i("TAG", "onResponse: " + message);

//                            if (mcomment.equalsIgnoreCase("")) {
//
//                                Constants.showOneButtonAlertDialog("Request Successful", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            } else {
//
//                                Constants.showOneButtonAlertDialog("Order Rejected Successfully", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            }
//                            if (OrderListActivity.fa != null) {
//                                OrderListActivity.fa.finish();
//                                Intent a = new Intent(OrderDetailsActivity.this, OrderListActivity.class);
//                                if (language.equalsIgnoreCase("En")) {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                } else {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                }
//                                a.putExtra("parameter", getIntent().getStringExtra("parameter"));
//                                startActivity(a);
//                                finish();
//                            } else {
                            finish();
//                            }


                        } else {
                            String failureResponse = updateorderList.getMessageEn();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OrderDetailsActivity.this);
                            } else {
                                showOneButtonAlertDialog(updateorderList.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateOrderList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class CancelOrderApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = cancelprepareJson();
            Constants.showLoadingDialog(OrderDetailsActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<UpdateOrderList> call = apiService.getcancelorder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateOrderList>() {
                @Override
                public void onResponse(Call<UpdateOrderList> call, Response<UpdateOrderList> response) {
                    if (response.isSuccessful()) {
                        UpdateOrderList updateorderList = response.body();
                        if (updateorderList.getStatus()) {
                            String message = updateorderList.getMessageEn();
                            Log.i("TAG", "onResponse: " + message);

//                            if (mcomment.equalsIgnoreCase("")) {
//
//                                Constants.showOneButtonAlertDialog("Request Successful", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            } else {
//
//                                Constants.showOneButtonAlertDialog("Order Rejected Successfully", getResources().getString(R.string.app_name), getResources().getString(R.string.ok), OrderDetailsActivity.this);
//
//                            }

//                            if (OrderListActivity.fa != null) {
//                                OrderListActivity.fa.finish();
//                                Intent a = new Intent(OrderDetailsActivity.this, OrderListActivity.class);
//                                if (language.equalsIgnoreCase("En")) {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                } else {
//                                    a.putExtra("header", getIntent().getStringExtra("header"));
//                                }
//                                a.putExtra("parameter", getIntent().getStringExtra("parameter"));
//                                startActivity(a);
                            finish();
//                            } else {
//                                finish();
//                            }

                        } else {
                            String failureResponse = updateorderList.getMessageEn();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), OrderDetailsActivity.this);
                            } else {
                                showOneButtonAlertDialog(updateorderList.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UpdateOrderList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareOrderDetailsJson() {
        JSONObject parentObj = new JSONObject();

        try {

            parentObj.put("OrderId", morder_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "orderid: " + morder_id);
        Log.i("TAG", "prepareorderdetailsJson: " + parentObj);

        return parentObj.toString();
    }

    private class OrderdetailsApi extends AsyncTask<String, Integer, String> {

        AlertDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetailsActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.progress_bar_alert;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            scrollView.setVisibility(View.GONE);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderDetailsList> call = apiService.getorderdetails(String.valueOf(morder_id));

            Log.i("TAG", "doInBackground: " + String.valueOf(morder_id));

            call.enqueue(new Callback<OrderDetailsList>() {
                @Override
                public void onResponse(Call<OrderDetailsList> call, Response<OrderDetailsList> response) {
                    Log.i("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        OrderDetailsList orderdetailsResponse = response.body();
                        if (orderdetailsResponse.getStatus()) {

                            orderDetailsLists = orderdetailsResponse.getData();
                            Constants.Current_time=orderdetailsResponse.getData().getCurrentTime();


                            SimpleDateFormat current_date5 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat current_date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                            SimpleDateFormat current_date2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                            SimpleDateFormat current_time = new SimpleDateFormat("hh:mm a", Locale.US);

                            SimpleDateFormat server_time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

                            Date expect_date = null;
                            String expected_date;

                            try {
                                expect_date = current_date5.parse(orderDetailsLists.getExpectedDate());
                                expected_date = dateFormat1.format(expect_date);
                                String date, time;

                                date = current_date2.format(expect_date);
                                time = current_time.format(expect_date);


                            } catch (Exception e) {
//                Crashlytics.logException(e);
                                e.printStackTrace();
                            }

                            Date orderdate = null;
                            String orderd_date;

                            try {
                                orderdate = current_date1.parse(orderDetailsLists.getOrderDate());
                                orderd_date = dateFormat1.format(orderdate);

                                String date, time;

                                date = current_date2.format(orderdate);
                                time = current_time.format(orderdate);


                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Date currentdate = null;
                            String current_timedate;
                            Date startDate = null;


                            try {
                                currentdate = server_time.parse(Constants.Current_time);
                                current_timedate = dateFormat1.format(currentdate);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            final long startMillis = orderdate.getTime();
                            final long[] currentMillis = {currentdate.getTime()};
                            final long end_millis = expect_date.getTime();

                            if (orderDetailsLists.getOrderType().equals("Dine In") || orderDetailsLists.getOrderType().equals("Pick Up")) {
                                if (language.equalsIgnoreCase("En")) {
                                    deliveryDateTitle.setText("Serving Date Time");
                                    deliveryToTitle.setText("Served to");
                                }
                                else {
                                    deliveryDateTitle.setText("موعد و تاريخ التسليم");
                                    deliveryToTitle.setText("تقدم الى");
                                }
                            }
                            if (language.equalsIgnoreCase("En")) {
                                storeName.setText(orderDetailsLists.getStoreName());
                            }else{
                                storeName.setText(orderDetailsLists.getStoreNameAr());
                            }


                            if (parameter.equals("New")){

                                leftTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @RequiresApi(api = Build.VERSION_CODES.M)
                                    @Override
                                    public void onTick(long l) {
                                        currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = currentMillis[0] - startMillis;
                                        long expriry30sce=startMillis+30000;

                                        if (currentMillis[0] > expriry30sce){
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutred));
                                        }
                                        else{
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                                        dateleft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        dateleft.setText("00:00:00");
                                    }
                                }.start();


                                rightTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @RequiresApi(api = Build.VERSION_CODES.M)
                                    @Override
                                    public void onTick(long l) {
//                    currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = end_millis - currentMillis[0];

                                        if (currentMillis[0] > end_millis){
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }
                                        else{
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutgreen));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                                        tvTimeLeft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        tvTimeLeft.setText("00:00:00");
                                    }
                                }.start();

                            }

                            else if (parameter.equals("Pending")){


                                leftTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @Override
                                    public void onTick(long l) {
                                        currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = currentMillis[0] - startMillis;
                                        long time90perstange= (long) ((end_millis-startMillis)*(0.9));
                                        long readyTime = startMillis + time90perstange;


                                        if (currentMillis[0] > readyTime){
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutred));
                                        }
                                        else{
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));


                                        dateleft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        dateleft.setText("00:00:00");
                                    }
                                }.start();

                                rightTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @Override
                                    public void onTick(long l) {
//             currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = end_millis - currentMillis[0];

                                        if (currentMillis[0] > end_millis){
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }
                                        else{
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutgreen));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                                        tvTimeLeft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        tvTimeLeft.setText("00:00:00");
                                    }
                                }.start();
                            }

                            else if (parameter.equals("Ready")){

                                leftTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @Override
                                    public void onTick(long l) {
                                        currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = currentMillis[0] - startMillis;

                                        if (currentMillis[0] > end_millis){
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutred));
                                        }
                                        else{
                                            colorlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                                        dateleft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        dateleft.setText("00:00:00");
                                    }
                                }.start();

                                rightTimer = new CountDownTimer(1000000000, 1000) {
                                    // 1000 means, onTick function will be called at every 1000 milliseconds

                                    @Override
                                    public void onTick(long l) {
//                currentMillis[0] = currentMillis[0] + 1000;
                                        long leftTimeInMilliseconds = end_millis - currentMillis[0];

                                        if (currentMillis[0] > end_millis){
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutblack));
                                        }
                                        else{
                                            righttlayout.setBackgroundColor(getResources().getColor(R.color.layoutgreen));
                                        }

                                        String hms = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(leftTimeInMilliseconds),
                                                TimeUnit.MILLISECONDS.toSeconds(leftTimeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

                                        tvTimeLeft.setText(hms);
                                    }
                                    @Override
                                    public void onFinish() {
                                        tvTimeLeft.setText("00:00:00");
                                    }
                                }.start(); }

//                            for (int i = 0; i < orderDetailsLists.size(); i++) {

                            invoice_no.setText("# " + orderDetailsLists.getInvoiceNo());
                            order_status.setText("" + orderDetailsLists.getOrderStatus());

                            orderDetailsAdapter = new OrderDetailsAdapter(OrderDetailsActivity.this, orderDetailsLists.getItems(), language);
                            order_details_list.setAdapter(orderDetailsAdapter);

                            if (language.equalsIgnoreCase("En")) {
                                order_details_total_price.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()));
                                service_type.setText("" + orderDetailsLists.getOrderType());
                            } else {
                                order_details_total_price.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()));
                                service_type.setText("" + orderDetailsLists.getOrderTypeAr());

                            }
                            item_qty.setText("" + orderDetailsLists.getCountofItems());

                            Log.i("TAG", "onResponse: " + orderDetailsLists.getOrderStatus());

                            if (orderDetailsLists.getOrderStatus().equals("New")) {
                                accept.setVisibility(View.VISIBLE);
                                reject.setVisibility(View.VISIBLE);
//                                driver_details.setVisibility(View.GONE);
                                if (language.equalsIgnoreCase("En")) {
                                    accept.setText("Accept");
                                } else {
                                    accept.setText("تم قبوله");
                                }
                            } else if (orderDetailsLists.getOrderStatus().equals("Accepted")) {
                                accept.setVisibility(View.VISIBLE);
                                reject.setVisibility(View.VISIBLE);
//                                driver_details.setVisibility(View.GONE);
                                if (language.equalsIgnoreCase("En")) {
                                    accept.setText("Ready");
                                } else {
                                    accept.setText("جاهز");
                                }
                            } else if (orderDetailsLists.getOrderStatus().equals("Ready")) {
                                accept.setVisibility(View.VISIBLE);
                                reject.setVisibility(View.GONE);
//                                driver_details.setVisibility(View.GONE);
                                if (orderDetailsLists.getOrderType().equals("Dine In")) {
                                    if (language.equalsIgnoreCase("En")) {
                                        accept.setText("Served");
                                    } else {
                                        accept.setText("يقدم");
                                    }
                                }
                                if (orderDetailsLists.getOrderType().equals("Pick Up")) {
                                    if (language.equalsIgnoreCase("En")) {
                                        accept.setText("Served");
                                    } else {
                                        accept.setText("يقدم");
                                    }
                                }
                                if (orderDetailsLists.getOrderType().equals("Delivery")) {

//                                        if (assigndriver == 0) {
//
//                                            accept.setVisibility(View.GONE);
//
//                                        } else {
//
//                                            accept.setVisibility(View.VISIBLE);
//
//                                        }
//                                        driver_details.setVisibility(View.VISIBLE);
                                    if (language.equalsIgnoreCase("En")) {
                                        accept.setText("Delivered");
                                    } else {
                                        accept.setText("Delivered");
                                    }

                                }
                            }
//                                else if (orderDetailsLists.getOrderStatus().equals("DriverAccept")) {
//
//                                    accept.setVisibility(View.VISIBLE);
//                                    reject.setVisibility(View.GONE);
//                                    driver_details.setVisibility(View.GONE);
//                                    if (orderDetailsLists.getOrderMode_En().equals("Dine In")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        } else {
//                                            accept.setText("تم تقديمه");
//                                        }
//                                    }
//                                    if (orderDetailsLists.getOrderMode_En().equals("Pick Up")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        } else {
//                                            accept.setText("تم تقديمه");
//                                        }
//                                    }
//                                    if (orderDetailsLists.getOrderMode_En().equals("Delivery")) {
////                                        if (assigndriver == 0) {
////
////                                            accept.setVisibility(View.GONE);
////
////                                        } else {
////
////                                            accept.setVisibility(View.VISIBLE);
////
////                                        }
////                                        driver_details.setVisibility(View.VISIBLE);
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Delivered");
//                                        } else {
//                                            accept.setText("Delivered");
//                                        }
//                                    }
//
//                                } else if (orderDetailsLists.getOrderStatus().equals("OnTheWay")) {
//
//                                    accept.setVisibility(View.VISIBLE);
//                                    reject.setVisibility(View.GONE);
//                                    driver_details.setVisibility(View.GONE);
//                                    if (orderDetailsLists.getOrderMode_En().equals("Dine In")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        } else {
//                                            accept.setText("تم تقديمه");
//                                        }
//                                    }
//                                    if (orderDetailsLists.getOrderMode_En().equals("Pick Up")) {
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Served");
//                                        } else {
//                                            accept.setText("تم تقديمه");
//                                        }
//                                    }
//                                    if (orderDetailsLists.getOrderMode_En().equals("Delivery")) {
////                                        if (assigndriver == 0) {
////
////                                            accept.setVisibility(View.GONE);
////
////                                        } else {
////
////                                            accept.setVisibility(View.VISIBLE);
////
////                                        }
////                                        driver_details.setVisibility(View.VISIBLE);
//                                        if (language.equalsIgnoreCase("En")) {
//                                            accept.setText("Delivered");
//                                        } else {
//                                            accept.setText("Delivered");
//                                        }
//                                    }
//
//                                }
                            else if (orderDetailsLists.getOrderStatus().equals("Close")) {
                                accept.setVisibility(View.GONE);
                                reject.setVisibility(View.GONE);
//                                view.setVisibility(View.GONE);
//                                driver_details.setVisibility(View.GONE);

                            } else if (orderDetailsLists.getOrderStatus().equals("Rejected")) {
                                accept.setVisibility(View.GONE);
                                reject.setVisibility(View.GONE);
//                                view.setVisibility(View.GONE);
//                                driver_details.setVisibility(View.GONE);
                            }


                            SimpleDateFormat current_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat dayFormat = new SimpleDateFormat("dd", Locale.US);
                            SimpleDateFormat mouthFormat = new SimpleDateFormat("MMMM", Locale.US);

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                            SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);


                            Date delivery_date = null, order_date = null;
                            String delivery_date1, order_date1;

                            try {
                                delivery_date = dateFormat.parse(orderDetailsLists.getExpectedDate());
                                order_date = dateFormat2.parse(orderDetailsLists.getOrderDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            delivery_date1 = current_date.format(delivery_date);
                            order_date1 = current_date.format(order_date);

                            String curentdate1, currenttime1;
                            String curentdate2, currenttime2;

                            curentdate1 = currentDate.format(order_date);
                            currenttime1 = currentTime.format(order_date);

                            curentdate2 = currentDate.format(delivery_date);
                            currenttime2 = currentTime.format(delivery_date);

                            order_date_time.setText("" + curentdate1 + "\n" + currenttime1);
                            delivery_date_time.setText("" + curentdate2 + "\n" + currenttime2);
//                            delivery_date_time.setText(delivery_date1);
//                            order_date_time.setText(order_date1);
                            if (language.equalsIgnoreCase("En")) {

                                payment_mode.setText("" + orderDetailsLists.getPaymentType());
                                total_amount.setText("SR  " + Constants.decimalFormat.format(orderDetailsLists.getSubTotal()));
                                total_amount1.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()));
                                subtotal.setText("SR  " + Constants.decimalFormat.format(orderDetailsLists.getSubTotal()));

//                                if (orderDetailsLists.getPromoValue() == 0 || orderDetailsLists.getPromoValue() == 0.0) {
//                                    discount_amount.setText("- " + "0.00" + " SAR");
//                                } else {
//                                    discount_amount.setText("- " + Constants.decimalFormat.format(orderDetailsLists.getPromoValue()) + " SAR");
//                                }
//                                    if (orderDetailsLists.getDeliveryCharges() == 0) {
                                delivery_charger.setText("+ " + "0.00" );
//                                    } else {
//                                        delivery_charger.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.getDeliveryCharges()) + " SAR");
//                                    }
                                vat.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.getVat()) );
                                promo.setText("-  " + Constants.decimalFormat.format(orderDetailsLists.getPromoValue()) );
                                net_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()) + " SAR");

                            } else {

                                payment_mode.setText("" + orderDetailsLists.getPaymentTypeAr());
                                total_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.getSubTotal()) + " SAR");
                                total_amount1.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()));
//                                if (orderDetailsLists.getPromoValue() == 0) {
//                                    discount_amount.setText("- " + "0.00" + " SAR");
//                                } else {
//                                    discount_amount.setText("- " + Constants.decimalFormat.format(orderDetailsLists.getPromoValue()) + " SAR");
//                                }
//                                    if (orderDetailsLists.getDeliveryCharges() == 0) {
                                delivery_charger.setText("+ " + "0.00");
//                                    } else {
//                                        delivery_charger.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.getDeliveryCharges()) + " SAR");
//                                    }
                                promo.setText("-  " + Constants.decimalFormat.format(orderDetailsLists.getPromoValue()));
                                vat.setText("+ " + Constants.decimalFormat.format(orderDetailsLists.getVat()) );
                                net_amount.setText("" + Constants.decimalFormat.format(orderDetailsLists.getTotalPrice()) + " SAR");
                                subtotal.setText("" + Constants.decimalFormat.format(orderDetailsLists.getSubTotal())+"  SR");

                            }
//                                note.setText("Note:");
                            //order date
                            Date day1 = null;
                            String day2, month2;

                            try {
                                day1 = dateFormat.parse(orderDetailsLists.getExpectedDate());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            day2 = current_date.format(day1);
                            month2 = mouthFormat.format(day1);

                            String currenttime, currentdatee;

                            currenttime = currentTime.format(day1);
                            currentdatee = currentDate.format(day1);

                            day.setText("" + currentdatee + "\n" + currenttime);

//                                month.setText(month2);
                            user_name.setText("" + orderDetailsLists.getFullname());
                            if (orderDetailsLists.getOrderType().equals("Delivery")) {
                                address_layout.setVisibility(View.VISIBLE);
                                google_img.setVisibility(View.VISIBLE);
                                mobile_no_layout.setVisibility(View.VISIBLE);
                                address_txt.setText("" + orderDetailsLists.getCAddress());
//                                mobile_no.setText("" + orderDetailsLists.getMobile());

                            } else if(orderDetailsLists.getOrderType().equals("Dine In")) {

                                address_layout.setVisibility(View.GONE);
                                google_img.setVisibility(View.GONE);
                                mobile_no_layout.setVisibility(View.VISIBLE);



                            }

//                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onResponse: ");
                    }
                    if (customDialog != null){
                        customDialog.dismiss();
                    }
                    scrollView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<OrderDetailsList> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    if (customDialog != null){
                        customDialog.dismiss();
                    }
                    scrollView.setVisibility(View.VISIBLE);
                }
            });
            return null;
        }
    }

}
