package com.cs.dr_cafe_vendor.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.cs.dr_cafe_vendor.Adapters.PendingListAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Fragments.OrderStatus;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderTypeActivity extends AppCompatActivity {

//    LinearLayout Dinein, pickup, delivery;
//    TextView scheduled_txt, order_txt, dinein_txt;
//    View scheduled_view, order_view, pending_view;
//    String language, userId = "";
//    SharedPreferences userPrefs;
//    SharedPreferences LanguagePrefs;
//    boolean scheduled_boolean = false, order_boolean = false, dinein_boolean = false;
//    ImageView back_btn;
//    boolean loder = true;
//    LinearLayout noorderlayout;
//
//    public static Activity fa;
//    ArrayList<OrderTypelist.Data> orderLists = new ArrayList<>();
//    ArrayList<OrderTypelist.Data> itemsLists = new ArrayList<>();
//
//    ArrayList<OrderTypelist.Data> dine_inorderLists = new ArrayList<>();
//    ArrayList<OrderTypelist.Data> pickuporderLists = new ArrayList<>();
//    ArrayList<OrderTypelist.Data> deliveryorderLists = new ArrayList<>();
//
//    RecyclerView mpending_list;
//    PendingListAdapter pendingListAdapter;
//    public static String header, header_title, parameter;
//    TextView header_txt;
//    public static int assigndriver;
//    private Timer timer = new Timer();
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
//        language = LanguagePrefs.getString("language", "En");
//        if (language.equalsIgnoreCase("En")) {
//            setContentView(R.layout.order_types);
//        } else {
//            setContentView(R.layout.order_types_arabic);
//        }
////        context = this;
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//
//        fa = this;
//
//        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
//        userId = userPrefs.getString("userId", "0");
//
//        header_txt = findViewById(R.id.header);
//        back_btn = findViewById(R.id.back_btn);
//
//        header_txt.setText("" + getIntent().getStringExtra("header"));
//
//        Dinein = findViewById(R.id.pending);
//        pickup = findViewById(R.id.schedule);
//        delivery = findViewById(R.id.order);
//
//        dinein_txt = findViewById(R.id.pending_txt);
//        scheduled_txt = findViewById(R.id.schedule_txt);
//        order_txt = findViewById(R.id.order_txt);
//
//        pending_view = findViewById(R.id.pending_view);
//        scheduled_view = findViewById(R.id.schedule_view);
//        order_view = findViewById(R.id.order_view);
//        noorderlayout = (LinearLayout) findViewById(R.id.noorderslayout);
//
//        mpending_list = findViewById(R.id.pendinglist);
//
//        back_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                finish();
//            }
//        });
//
//        Dinein.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////
////                dinein_boolean = true;
////                scheduled_boolean = false;
////                order_boolean = false;
//                dinein_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
//                order_txt.setTextColor(getResources().getColor(R.color.black));
//
//                pending_view.setVisibility(View.VISIBLE);
//                scheduled_view.setVisibility(View.GONE);
//                order_view.setVisibility(View.GONE);
//
//
//                if (dine_inorderLists.size() > 0) {
//
//                    noorderlayout.setVisibility(View.GONE);
//                    pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
//                    mpending_list.setAdapter(pendingListAdapter);
//
//                } else {
//                    noorderlayout.setVisibility(View.VISIBLE);
//
//                }
//                Log.i("TAG", "dinein_size: " + dine_inorderLists.size());
//
////                new Orderstatusapi().execute();
//
//            }
//        });
//
//        pickup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////
////                dinein_boolean = false;
////                scheduled_boolean = true;
////                order_boolean = false;
//
//                dinein_txt.setTextColor(getResources().getColor(R.color.black));
//                scheduled_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                order_txt.setTextColor(getResources().getColor(R.color.black));
//
//                pending_view.setVisibility(View.GONE);
//                scheduled_view.setVisibility(View.VISIBLE);
//                order_view.setVisibility(View.GONE);
//
//
//                if (pickuporderLists.size() > 0) {
//                    noorderlayout.setVisibility(View.GONE);
//                    pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
//                    mpending_list.setAdapter(pendingListAdapter);
//                    Log.i("TAG", "pickup_size: " + pickuporderLists.size());
//                } else {
//                    noorderlayout.setVisibility(View.VISIBLE);
//
//                }
//
////                new Orderstatusapi().execute();
//
//            }
//        });
//
//        delivery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////
////                dinein_boolean = false;
////                scheduled_boolean = false;
////                order_boolean = true;
//
//                dinein_txt.setTextColor(getResources().getColor(R.color.black));
//                scheduled_txt.setTextColor(getResources().getColor(R.color.black));
//                order_txt.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//
//                pending_view.setVisibility(View.GONE);
//                scheduled_view.setVisibility(View.GONE);
//                order_view.setVisibility(View.VISIBLE);
//
////                new Orderstatusapi().execute();
//
//                if (deliveryorderLists.size() > 0) {
//
//                    noorderlayout.setVisibility(View.GONE);
//                    pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
//                    mpending_list.setAdapter(pendingListAdapter);
//
//                    Log.i("TAG", "delvery_size: " + deliveryorderLists.size());
//                } else {
//                    noorderlayout.setVisibility(View.VISIBLE);
//
//                }
//
//            }
//        });
//
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OrderTypeActivity.this);
//        mpending_list.setLayoutManager(mLayoutManager);
//
//        new Orderstatusapi().execute();
//
//        LocalBroadcastManager.getInstance(OrderTypeActivity.this).registerReceiver(
//                mupdate_order, new IntentFilter("UpdateOrder"));
//    }
//
//    private BroadcastReceiver mupdate_order = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d("TAG", "onBroadcastReceive: UpdateOrder");
//
//            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
//            mpending_list.setAdapter(pendingListAdapter);
//
//            if (dinein_boolean) {
//
//                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
//                mpending_list.setAdapter(pendingListAdapter);
//
//            } else if (scheduled_boolean) {
//
//                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
//                mpending_list.setAdapter(pendingListAdapter);
//
//            } else if (order_boolean) {
//
//                pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, deliveryorderLists, userId, language);
//                mpending_list.setAdapter(pendingListAdapter);
//
//            }
//
//            new Orderstatusapi1().execute();
//
//        }
//    };
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        if (timer != null) {
//            timer.cancel();
//        }
//    }
//
//    private class MyTimerTask extends TimerTask {
//
//        @Override
//        public void run() {
//            if (OrderTypeActivity.this != null) {
//
//                OrderTypeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        String networkStatus = OrderStatus.NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
//                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                            new Orderstatusapi().execute();
//                        } else {
//                            if (language.equalsIgnoreCase("En")) {
//                                Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                            } else {
//                                Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//
//                    }
//                });
//            }
//        }
//    }
//
//
//    private class Orderstatusapi extends AsyncTask<String, Integer, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            if (loder) {
//                Constants.showLoadingDialog(OrderTypeActivity.this);
//            }
//            loder = false;
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<OrderTypelist> call = apiService.orderstatus(userId, header);
//
//
//            call.enqueue(new Callback<OrderTypelist>() {
//                @Override
//                public void onResponse(Call<OrderTypelist> call, Response<OrderTypelist> response) {
//
//                    Log.i("TAG", "onResponse: " + response.isSuccessful());
//                    if (response.isSuccessful()) {
//                        OrderTypelist Response = response.body();
//
//                        orderLists = Response.getData();
//                        Log.d("TAG", "onResponse: " + response);
//
//                        pickuporderLists.clear();
//                        deliveryorderLists.clear();
//                        dine_inorderLists.clear();
//
//                        Log.i("TAG", "count: " + orderLists.size());
//
//                        for (int i = 0; i < orderLists.size(); i++) {
//
//                            Log.i("TAG", "Order type: " + orderLists.get(i).getOrderStatus());
//
//                            if (orderLists.get(i).getOrderType().equals("Dine In")) {
//                                dine_inorderLists.add(orderLists.get(i));
//
//                            }
//                            if (orderLists.get(i).getOrderType().equals("PickUp")) {
//                                pickuporderLists.add(orderLists.get(i));
//                            }
//
//                            if (orderLists.get(i).getOrderType().equals("Delivery")) {
//                                deliveryorderLists.add(orderLists.get(i));
//                            }
//                        }
//
//
//                        if (language.equalsIgnoreCase("En")) {
//
//                            scheduled_txt.setText("Pickup (" + pickuporderLists.size() + ")");
//                            order_txt.setText("Delivery (" + deliveryorderLists.size() + ")");
//                            dinein_txt.setText("Dine In (" + dine_inorderLists.size() + ")");
//
//                        } else {
//
//                            scheduled_txt.setText("(" + pickuporderLists.size() + ") طريقة التوصيل");
//                            order_txt.setText("(" + deliveryorderLists.size() + ") استلام");
//
//                        }
//
//
//                        if (dine_inorderLists.size() <= 0) {
//                            noorderlayout.setVisibility(View.VISIBLE);
//                        } else {
//                            noorderlayout.setVisibility(View.GONE);
//                        }
//
//
//                        if (dine_inorderLists.size() > 0){
//
//                            Dinein.performClick();
//
//                        } else if (dine_inorderLists.size() <= 0 && pickuporderLists.size() > 0) {
//
//                            pickup.performClick();
//
//                        } else if (dine_inorderLists.size() <= 0 && pickuporderLists.size() <= 0 && deliveryorderLists.size() > 0) {
//
//                            delivery.performClick();
//
//                        }
//
//
//                        Log.i("TAG", "onResponse: " + Response.getMessage());
//
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    Constants.closeLoadingDialog();
//                }
//
//                @Override
//                public void onFailure(Call<OrderTypelist> call, Throwable t) {
//                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
//                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                        Log.i("TAG", "onFailure: " + t);
//                    }
//                    Constants.closeLoadingDialog();
//                }
//            });
//            return null;
//        }
//    }
//
//
//    private class Orderstatusapi1 extends AsyncTask<String, Integer, String> {
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<OrderTypelist> call = apiService.orderstatus(userId, header);
//            Log.d("TAG", "doInBackground1: " + userId + header);
//            call.enqueue(new Callback<OrderTypelist>() {
//                @Override
//                public void onResponse(Call<OrderTypelist> call, Response<OrderTypelist> response) {
//
//                    Log.i("TAG", "onResponse: " + response.isSuccessful());
//                    if (response.isSuccessful()) {
//                        OrderTypelist Response = response.body();
//
//                        orderLists = Response.getData();
//
//                        pickuporderLists.clear();
//                        deliveryorderLists.clear();
//                        Log.i("TAG", "count: " + orderLists.size());
//
//                        for (int i = 0; i < orderLists.size(); i++) {
//                            Log.i("TAG", "Order type: " + orderLists.get(i).getOrderStatus());
//
//                            if (orderLists.get(i).getOrderType().equals("DineIn/PickUp")) {
//                                pickuporderLists.add(orderLists.get(i));
//                            }
//                            if (orderLists.get(i).getOrderType().equals("Delivery")) {
//                                deliveryorderLists.add(orderLists.get(i));
//                            }
//                        }
//
//
//                        pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
//                        mpending_list.setAdapter(pendingListAdapter);
//                        Log.i("TAG", "pickup_size: " + pickuporderLists.size());
//
//
//                        if (scheduled_boolean) {
//
//                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, pickuporderLists, userId, language);
//                            mpending_list.setAdapter(pendingListAdapter);
//                            Log.i("TAG", "pickup_size: " + pickuporderLists.size());
//
//                        } else if (dinein_boolean) {
//
//                            pendingListAdapter = new PendingListAdapter(OrderTypeActivity.this, OrderTypeActivity.this, dine_inorderLists, userId, language);
//                            mpending_list.setAdapter(pendingListAdapter);
//                            Log.i("TAG", "delvery_size: " + deliveryorderLists.size());
//
//                        }
//
//                        Log.i("TAG", "onResponse: " + Response.getMessage());
//
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<OrderTypelist> call, Throwable t) {
//                    final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
//                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        if (language.equalsIgnoreCase("En")) {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OrderTypeActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
//                        }
//                        Log.i("TAG", "onFailure: " + t);
//                    }
//                }
//            });
//            return null;
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
////        if (timer == null) {
//        timer = new Timer();
//        timer.schedule(new OrderTypeActivity.MyTimerTask(), 60000, 60000);
////        }
//
//        String networkStatus = OrderStatus.NetworkUtil.getConnectivityStatusString(OrderTypeActivity.this);
//        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//
//            new Orderstatusapi().execute();
//
//        } else {
//            if (language.equalsIgnoreCase("En")) {
//                Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(OrderTypeActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }
//


}


