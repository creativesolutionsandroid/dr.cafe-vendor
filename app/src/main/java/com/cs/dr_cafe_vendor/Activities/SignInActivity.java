package com.cs.dr_cafe_vendor.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

//import com.crashlytics.android.Crashlytics;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.Sign_In;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    String language;
//    Toolbar toolbar;
    Context context;
    Button buttonSignIn;
//    TextView textRegister;
//    ImageButton imagePasswordEye;
    SharedPreferences userPrefs;
    String strMobile, strPassword;
    SharedPreferences LanguagePrefs;
    EditText inputPassword;
    EditText inputMobile;
    SharedPreferences.Editor userPrefsEditor;
    LinearLayout forgotPasswordLayout;
//    AlertDialog loaderDialog = null;
    TextView mlanguage;


    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){
            setContentView(R.layout.activity_signin);
        } else {
            setContentView(R.layout.activity_signin_arabic);
        }
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();


        buttonSignIn = (Button) findViewById(R.id.signin_button);
//        textRegister = (TextView) findViewById(R.id.signin_register);
        inputMobile = (EditText) findViewById(R.id.signin_input_mobile);
        inputPassword= (EditText) findViewById(R.id.signin_input_password);
//        registerLayout = (LinearLayout) findViewById(R.id.signin_register_layout);
        forgotPasswordLayout = (LinearLayout) findViewById(R.id.signin_forgot_password);
        mlanguage=(TextView)findViewById(R.id.arabic);

//        mlanguage = findViewById(R.id.language);

        Log.d("TAG", "onCreate: "+SplashScreenActivity.regId);
        if (SplashScreenActivity.regId.equals("-1")){
            FirebaseApp.initializeApp(SignInActivity.this);
            SplashScreenActivity.regId = FirebaseInstanceId.getInstance().getToken();
        }

        mlanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
//                    new ChangeLanguageApi().execute();

                    Intent intent = new Intent(SignInActivity.this, SignInActivity.class);
                    startActivity(intent);
//                    finish();
                }else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(SignInActivity.this, SignInActivity.class);
                    startActivity(intent);
//                    finish();
                }

            }
        });

//        inputMobile.setText(Constants.Country_Code);

        setTypeface();

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userSignIn(v);

            }
        });
//
//        inputMobile.addTextChangedListener(new TextWatcher(inputMobile));
//        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setTypeface(){
        inputMobile.setTypeface(Constants.getTypeFace(context));
        inputPassword.setTypeface(Constants.getTypeFace(context));
        buttonSignIn.setTypeface(Constants.getTypeFace(context));
//        textRegister.setTypeface(Constants.getTypeFace(context));
    }

    public void userSignIn(View view){
        if(validations()){
            String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                userLoginApi();
            }
            else{
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

//    public void launchSignUpActivity(View view){
//        Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
//        startActivityForResult(intent, SIGNUP_REQUEST);
//    }

    public void launchForgotPasswordActivity(View view){
        Intent intent1 = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        startActivityForResult(intent1, FORGOT_PASSWORD_REQUEST);
    }

    @SuppressLint("NewApi")
    public void tooglePasswordMode(View view){
        if(inputPassword.getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD){
            inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        else{
            inputPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

        }
        inputPassword.setSelection(inputPassword.length());
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
        else if(requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
//        strMobile = strMobile.replace("+966 ","");

        if (strMobile.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_email));
            } else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;

//        }else if (!Constants.isValidEmail(strMobile)){
//            if (language.equalsIgnoreCase("En")) {
//                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_email));
//            }else {
//                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
//            }
//            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
//            return false;
        }
//        else if (strMobile.length() != 9){
//            inputLayoutMobile.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
//            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
//            return false;
//        }
        else if (strPassword.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            if (language.equalsIgnoreCase("En")) {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }

//    private class TextWatcher implements android.text.TextWatcher {
//        private View view;
//
//        private TextWatcher(View view) {
//            this.view = view;
//        }
//
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void afterTextChanged(Editable editable) {
//            switch (view.getId()) {
//                case R.id.signin_input_mobile:
////                    String enteredMobile = editable.toString();
////                    if(!enteredMobile.contains(Constants.Country_Code)){
////                        if(enteredMobile.length() > Constants.Country_Code.length()){
////                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
////                            inputMobile.setText(Constants.Country_Code + enteredMobile);
////                        }
////                        else {
////                            inputMobile.setText(Constants.Country_Code);
////                        }
////                        inputMobile.setSelection(inputMobile.length());
////                    }
//                    clearErrors();
//                    break;
//                case R.id.signin_input_password:
//                    clearErrors();
//                    if(editable.length() > 20){
//                        if (language.equalsIgnoreCase("En")) {
//                            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
//                        } else {
//                            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
//                        }
//                    }
//                    break;
//            }
//        }
//    }

//    private void clearErrors(){
//        inputPassword.setErrorEnabled(false);
//        inputPassword.setErrorEnabled(false);
//    }

//    private class userLoginApi extends AsyncTask<String, Integer, String>{
//
//        ACProgressFlower dialog;
//        String inputStr;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            inputStr = prepareLoginJson();
//            dialog = new ACProgressFlower.Builder(SignInActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
//            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
//
//            Call<UserRegistrationResponse> call = apiService.userLogin(
//                    RequestBody.create(MediaType.parse("application/json"), inputStr));
//            call.enqueue(new Callback<UserRegistrationResponse>() {
//                @Override
//                public void onResponse(Call<UserRegistrationResponse> call, Response<UserRegistrationResponse> response) {
//                    if(response.isSuccessful()){
//                        UserRegistrationResponse registrationResponse = response.body();
//                        try {
//                            if(registrationResponse.getStatus()){
//    //                          status true case
//                                String userId = registrationResponse.getData().getUserid();
//                                userPrefsEditor.putString("userId", userId);
//                                userPrefsEditor.putString("name", registrationResponse.getData().getName());
//                                userPrefsEditor.putString("email", registrationResponse.getData().getEmail());
//                                userPrefsEditor.putString("mobile", registrationResponse.getData().getMobile());
//                                userPrefsEditor.commit();
//                                Toast.makeText(SignInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
//                                setResult(RESULT_OK);
//                                finish();
//                            }
//                            else {
//    //                          status false case
//                                String failureResponse = registrationResponse.getMessage();
//                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
//                                        getResources().getString(R.string.ok), SignInActivity.this);
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    else{
//                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<UserRegistrationResponse> call, Throwable t) {
//                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                    }
//                    else {
//                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            if(dialog != null){
//                dialog.dismiss();
//            }
//        }
//    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignInActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    //main user login service

    private void userLoginApi(){
        String inputStr = prepareLoginJson();
        Constants.showLoadingDialog(SignInActivity.this);

        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<Sign_In> call = apiService.getSignIn(
                RequestBody.create(MediaType.parse("application/json"), inputStr));

        call.enqueue(new Callback<Sign_In>() {
            @Override
            public void onResponse(Call<Sign_In> call, Response<Sign_In> response) {
                if(response.isSuccessful()){
                    Sign_In registrationResponse = response.body();
                    Log.d("TAG", "onResponse: "+response);
                    try {
                        if(registrationResponse.getStatus()){
                            //status true case
                            String userId = String.valueOf(registrationResponse.getData().getId());
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("Name", registrationResponse.getData().getFullName());
                            userPrefsEditor.putString("email", registrationResponse.getData().getEmailId());
                            userPrefsEditor.putString("mobile", registrationResponse.getData().getPhone());
                            userPrefsEditor.commit();
                            Log.i("TAG", "onResponse: " + userPrefs.getString("name","-"));
                            Log.i("TAG", "onResponse: " + registrationResponse.getData().getFullName());
                            Log.i("TAG", "userid: " + registrationResponse.getData().getId());
                            Toast.makeText(SignInActivity.this, "Login successful", Toast.LENGTH_SHORT).show();
                            Intent a = new Intent(SignInActivity.this, MainActivity.class);
                            startActivity(a);
                            setResult(RESULT_OK);
                            finish();

                            String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";

//                            Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
//                            Crashlytics.setString(AppLanguage, language/* string value */);
//                            Crashlytics.setString("Device Token", SplashScreenActivity.regId);
//
//                            if (!userId.equals("0")) {
//                                Crashlytics.setString(UserId, userId/* string value */);
//                                Crashlytics.setString("Name", registrationResponse.getData().getFullName()/* string value */);
//                                Crashlytics.setString("Mobile", registrationResponse.getData().getPhone()/* string value */);
//                            }
//
//                            Crashlytics.getInstance();

                        }
                        else {
                            //status false case
                            if (language.equalsIgnoreCase("En")) {
                                String failureResponse = registrationResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SignInActivity.this);
                            } else {
                                String failureResponse = registrationResponse.getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), SignInActivity.this);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else{
                    Log.d("TAG", "onResponse: ");
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }
                }

               Constants.closeLoadingDialog();
            }

            @Override
            public void onFailure(Call<Sign_In> call, Throwable t) {
                Log.d("TAG", "onFailure: "+t);
                if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }
                }
                Constants.closeLoadingDialog();
            }
        });
    }


    private String prepareLoginJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("username",strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("DeviceToken", SplashScreenActivity.regId);
//            parentObj.put("DeviceToken", "testtoken_testtoken_testtoken_testtoken_testtoken_testtoken_");
            parentObj.put("Lang", language);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", " prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private String prepareChangeLangJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Flag", 2);
            parentObj.put("UserId", 0);
            parentObj.put("Language", language);
            parentObj.put("deviceToken", SplashScreenActivity.regId);
            parentObj.put("DeviceType", "andriod");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }


}
