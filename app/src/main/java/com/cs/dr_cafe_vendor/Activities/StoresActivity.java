package com.cs.dr_cafe_vendor.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.dr_cafe_vendor.Adapters.PendingListAdapter;
import com.cs.dr_cafe_vendor.Adapters.StoreListAdapter;
import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.OrderTypelist;
import com.cs.dr_cafe_vendor.Models.Sign_In;
import com.cs.dr_cafe_vendor.Models.StorelistResponce;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoresActivity extends AppCompatActivity {
    ImageView menu_image;
    RecyclerView list;

    SharedPreferences LanguagePrefs;

    String language, userId = "";
    SharedPreferences userPrefs;
    StoreListAdapter mAdapter;
    TextView noorders;
    ArrayList<StorelistResponce.Data> storeslist = new ArrayList<>() ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){

            setContentView(R.layout.activity_storelist);
        }else {

            setContentView(R.layout.activity_storelist_arabic);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "0");


        menu_image=(ImageView)findViewById(R.id.menu_image);
        list=(RecyclerView)findViewById(R.id.list_item);
        noorders=(TextView)findViewById(R.id.noorderstext);

        menu_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        new Orderstatusapi().execute();


    }

    private class Orderstatusapi extends AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("TAG", "onCreate: ");

            Constants.showLoadingDialog(StoresActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<StorelistResponce> call = apiService.getstores(userId);
            call.enqueue(new Callback<StorelistResponce>() {
                @Override
                public void onResponse(Call<StorelistResponce> call, Response<StorelistResponce> response) {

                    Log.d("TAG", "onResponse1: "+response.body());
                    if (response.isSuccessful()) {
                        StorelistResponce Response = response.body();

                        storeslist = Response.getData();
                        if (storeslist.size()==0){


                            noorders.setVisibility(View.VISIBLE);
                            list.setVisibility(View.GONE);
                        }
                        else {
                            noorders.setVisibility(View.GONE);
                            list.setVisibility(View.VISIBLE);

                        }
                        Log.i("TAG", "onResponse: " + storeslist.size());

                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(StoresActivity.this);
                        list.setLayoutManager(linearLayoutManager);
                        mAdapter = new StoreListAdapter(StoresActivity.this, StoresActivity.this, storeslist, userId, language);
                        list.setAdapter(mAdapter);

                    } else {
                        Log.d("TAG", "onResponse3: ");
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(StoresActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(StoresActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<StorelistResponce> call, Throwable t) {
                    Log.d("TAG", "onResponse1: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(StoresActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(StoresActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(StoresActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(StoresActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(StoresActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        Log.i("TAG", "onFailure: " + t);
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

}
