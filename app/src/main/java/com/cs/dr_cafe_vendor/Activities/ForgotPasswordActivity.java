package com.cs.dr_cafe_vendor.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Dialogs.ResetPasswordDialog;
import com.cs.dr_cafe_vendor.Dialogs.VerifyOtpDialog;
import com.cs.dr_cafe_vendor.Models.ForgetPassword;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    public static boolean isOTPSuccessful = false;
    public static boolean isResetSuccessful = false;
//    private TextInputLayout inputLayoutMobile;
    private EditText inputMobile;
    private String strMobile;
    private Button buttonSubmit;
    private String serverOtp,userid;
    Toolbar toolbar;
    Context context;
    AlertDialog loaderDialog = null;
    ImageView mback_btn;

    String language;
    SharedPreferences LanguagePrefs;

    public static final String TAG = "TAG";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_forgot_password);
        } else {
            setContentView(R.layout.activity_forgot_password_arabic);
        }
        context = this;


//        inputLayoutMobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        inputMobile = (EditText) findViewById(R.id.forgot_input_mobile);
        buttonSubmit = (Button) findViewById(R.id.forgot_submit_button);
        mback_btn = (ImageView) findViewById(R.id.back_btn);

        mback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        setTypeFace();

        buttonSubmit.setOnClickListener(this);
//        inputMobile.addTextChangedListener(new MyTextWatcher(inputMobile));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forgot_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ForgotPasswordApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    private void setTypeFace(){
        inputMobile.setTypeface(Constants.getTypeFace(context));
        buttonSubmit.setTypeface(Constants.getTypeFace(context));
        ((TextView) findViewById(R.id.forgot_body)).setTypeface(Constants.getTypeFace(context));
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();

        if (strMobile.length() == 0){
            if (language.equalsIgnoreCase("En")) {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_email));
            } else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }
            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
//        else if (!Constants.isValidEmail(strMobile)){
//            if (language.equalsIgnoreCase("En")) {
//                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_email));
//            }else {
//                inputMobile.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
//            }
//            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
//            return false;
//        }
        return true;
    }

//    private void clearErrors(){
//        inputLayoutMobile.setErrorEnabled(false);
//    }

//    private class MyTextWatcher implements TextWatcher {
//        private View view;
//
//        private MyTextWatcher(View view) {
//            this.view = view;
//        }
//
//        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//        }
//
//        public void afterTextChanged(Editable editable) {
//            switch (view.getId()) {
//                case R.id.forgot_input_mobile:
//                    clearErrors();
//                    break;
//            }
//        }
//    }

    private void displayVerifyOTPDialog(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", serverOtp);
        args.putString("userid",userid );

        isOTPSuccessful = false;

        final VerifyOtpDialog newFragment = VerifyOtpDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed

                if(isOTPSuccessful){
                    displayResetPasswordDiaolg();
                }

                Log.i(TAG, "onDismiss: " + isOTPSuccessful);

                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }

    private void displayResetPasswordDiaolg(){
        Bundle args = new Bundle();
        args.putString("mobile", strMobile);
        args.putString("otp", serverOtp);
        args.putString("userid",userid );

        Log.i(TAG, "displayResetPasswordDiaolg: " + strMobile);

        isResetSuccessful = false;

        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
        newFragment.setCancelable(false);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "forgot");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed


                if(isResetSuccessful){
                    setResult(RESULT_OK);
                    Intent a = new Intent(ForgotPasswordActivity.this, SignInActivity.class);
                    startActivity(a);
                    finish();
                }

                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }

    private String prepareForgotPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("mobileNo",""+strMobile);
        } catch (JSONException e) {
            e.printStackTrace();


        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

//    public void showloaderAlertDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.loder_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(true);
//
//        loaderDialog = dialogBuilder.create();1
//        loaderDialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = loaderDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }

    private class ForgotPasswordApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareForgotPasswordJson();
            Constants.showLoadingDialog(ForgotPasswordActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ForgetPassword> call = apiService.forgotPassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ForgetPassword>() {
                @Override
                public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                    if(response.isSuccessful()){
                        ForgetPassword forgotPasswordResponse = response.body();
                        try {
                            if(forgotPasswordResponse.getStatus()){
                                serverOtp = String.valueOf(forgotPasswordResponse.getData().getOTPCode());
                                userid = String.valueOf(forgotPasswordResponse.getData().getVendorId());

                                Log.i(TAG, "onResponse: "+serverOtp);
                                Log.i(TAG, "onResponse1: "+userid);
                                displayVerifyOTPDialog();
                            }
                            else {
                                //                          status false case
                                if (language.equalsIgnoreCase("En")) {
                                    String failureResponse = forgotPasswordResponse.getMessageEn();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                            getResources().getString(R.string.ok), ForgotPasswordActivity.this);
                                } else {
                                    String failureResponse = forgotPasswordResponse.getMessageAr();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name_ar),
                                            getResources().getString(R.string.ok_ar), ForgotPasswordActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ForgetPassword> call, Throwable t) {
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Log.i(TAG, "onFailure: " + t);

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Constants.closeLoadingDialog();
        }
    }
}
