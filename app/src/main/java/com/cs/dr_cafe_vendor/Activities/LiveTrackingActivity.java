package com.cs.dr_cafe_vendor.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.DirectionJSONParser;
import com.cs.dr_cafe_vendor.Models.LiverTrackingList;
import com.cs.dr_cafe_vendor.Models.TrafficTimeList;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.cs.dr_cafe_vendor.Rest.GoogleMapApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);

    ImageView back_btn;
    TextView driverNameTxt, callNow, expTime, deliveryTime;
    String storePhone, driverPhone;
    Double storeLat, storeLong, userLat, userLong, driverLat, driverLong;
    String driverName;
    String driverNumber;
    String driverId;
    int orderId;
    String expectedTime, orderstatus;

    private String timeResponse = null;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private Timer timer = new Timer();

    SharedPreferences languagePrefs;
    String language, ordertype;

    private Polyline mPolyline, mPolyline1;

    Marker marker;
    boolean isLoadingFirstTime = true;
    float rotate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_live_tracking);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_live_tracking_arabic);
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
//        setSupportActionBar(back_btn);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

//        driverName = getIntent().getStringExtra("driver_name");
//        driverNumber = getIntent().getStringExtra("driver_number");
//        driverId = getIntent().getStringExtra("driver_id");
        orderId = getIntent().getIntExtra("order_id", 0);
        expectedTime = getIntent().getStringExtra("exp_time");
        orderstatus = getIntent().getStringExtra("orderstatus");
        Log.i("TAG", "onCreate: " + orderId);

//        new GetCurrentTime().execute();
        callNow = (TextView) findViewById(R.id.call_now);
        deliveryTime = (TextView) findViewById(R.id.delivery_time);
        expTime = (TextView) findViewById(R.id.expected_time);
        driverNameTxt = (TextView) findViewById(R.id.driver_name);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(LiveTrackingActivity.this);

        if (language.equalsIgnoreCase("En")) {
            expTime.setText("Expected Time : " + expectedTime);
        } else if (language.equalsIgnoreCase("Ar")) {
            expTime.setText("الوقت المتوقع  : " + expectedTime);
        }

        new getTrackingDetails().execute();

        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + driverNumber));
                        if (ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,

                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + driverNumber));
                    startActivity(intent);
                }
            }
        });

        timer.schedule(new MyTimerTask(), 10000, 30000);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new getTrackingDetails().execute();
                }
            });
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driverNumber));
                    if (ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(LiveTrackingActivity.this, "Call phone permission denied, unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        new getTrackingDetails().execute();
//        LatLng driver = new LatLng(driverLat, driverLong);
//        LatLng store = new LatLng(storeLat, storeLong);
//        LatLng user = new LatLng(userLat, userLong);
//
//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
//        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
//        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }

    private class getTrackingDetails extends AsyncTask<String, String, String> {

        AlertDialog dialog = null;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareLiverTrackingJson();

//            dialog = new ACProgressFlower.Builder(OfferActivity.this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .fadeColor(Color.DKGRAY).build();
//            dialog.show();

//            Constants.showLoadingDialog(LiveTrackingActivity.this);
//
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<LiverTrackingList> call = apiService.getLiveTracking(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<LiverTrackingList>() {
                @Override
                public void onResponse(Call<LiverTrackingList> call, Response<LiverTrackingList> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        LiverTrackingList livetracking = response.body();

                        try {
                            if (livetracking.getStatus()) {
                                String message = livetracking.getMessage();

                                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                        .findFragmentById(R.id.map);
                                mapFragment.getMapAsync(LiveTrackingActivity.this);

                                storeLat = Double.valueOf(livetracking.getData().getStoreLatitude());
                                storeLong = Double.valueOf(livetracking.getData().getStoreLongitude());
                                userLat = Double.valueOf(livetracking.getData().getUserLatitude());
                                userLong = Double.valueOf(livetracking.getData().getUserLongitude());
                                driverLat = Double.valueOf(livetracking.getData().getLatitude());
                                driverLong = Double.valueOf(livetracking.getData().getLongitude());
//                                storePhone = Double.valueOf(livetracking.getData());
                                driverPhone = livetracking.getData().getMobileNo();
                                driverNameTxt.setText(livetracking.getData().getFullName());
                                timeResponse = livetracking.getData().getCurrentDateTime();
                                rotate = Float.parseFloat(livetracking.getData().getRotation());
                                orderstatus = livetracking.getData().getOrderStatus();

                                Log.i("TAG", "driver lat, log: " + driverLat + " , " + driverLong);
                                Log.i("TAG", "user lat, log: " + userLat + " , " + userLong);
                                Log.i("TAG", "store lat, log: " + storeLat + " , " + storeLong);

                                if (mMap != null) {
//                                    mMap.clear();
                                    final LatLng driver = new LatLng(driverLat, driverLong);
                                    LatLng store = new LatLng(storeLat, storeLong);
                                    LatLng user = new LatLng(userLat, userLong);

                                    if (isLoadingFirstTime) {
                                        marker = mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)).rotation(0).flat(true));
                                        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
                                        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
                                        isLoadingFirstTime = false;

                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
                                        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

                                        final Handler handler = new Handler();
                                        final long start = SystemClock.uptimeMillis();
                                        final long duration = 2500;

                                        final Interpolator interpolator = new BounceInterpolator();
                                        marker.setVisible(true);

                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {

                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        long elapsed = SystemClock.uptimeMillis() - start;
                                                        float t = Math.max(
                                                                1 - interpolator.getInterpolation((float) elapsed
                                                                        / duration), 0);

                                                        marker.setAnchor(0.5f, 1.0f + 6 * t);
                                                        marker.setPosition(driver);

                                                        if (mMap != null)
                                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driver.latitude, driver.longitude), 17.0f));


                                                        if (t > 0.0) {
                                                            // Post again 16ms later.
                                                            handler.postDelayed(this, 16);
                                                        }

                                                    }
                                                });
                                                handler.postDelayed(this, 3000);
                                            }
                                        });
                                    } else {
//                                        marker = mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//                                        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
                                    }
//                                    changePositionSmoothly(marker, user);
                                    final LatLng startPosition = marker.getPosition();
                                    final LatLng finalPosition = new LatLng(driverLat, driverLong);
                                    final Handler handler = new Handler();
                                    final long start = SystemClock.uptimeMillis();
                                    final Interpolator interpolator = new AccelerateDecelerateInterpolator();
                                    final float durationInMs = 3000;
                                    final boolean hideMarker = false;
                                    handler.post(new Runnable() {
                                        long elapsed;
                                        float t;
                                        float v;

                                        @Override
                                        public void run() {
                                            // Calculate progress using interpolator
                                            elapsed = SystemClock.uptimeMillis() - start;
                                            t = elapsed / durationInMs;
                                            v = interpolator.getInterpolation(t);
                                            LatLng currentPosition = new LatLng(
                                                    startPosition.latitude * (1 - t) + finalPosition.latitude * t,
                                                    startPosition.longitude * (1 - t) + finalPosition.longitude * t);
                                            marker.setPosition(currentPosition);
                                            marker.setRotation(rotate);
                                            // Repeat till progress is complete.
                                            if (t < 1) {
                                                // Post again 16ms later.
                                                handler.postDelayed(this, 16);
                                            } else {
                                                if (hideMarker) {
                                                    marker.setVisible(false);
                                                } else {
                                                    marker.setVisible(true);
                                                }
                                            }
                                        }
                                    });

                                }

                                if (orderstatus.equals("OnTheWay")) {

//                                    drawRoute1();

                                } else {

//                                    drawRoute();
//                                    drawRoute1();
                                }


                                getTrafficTimeapi();
                            } else {
                                //                          status false case
                                String failureResponse = livetracking.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), LiveTrackingActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(LiveTrackingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LiveTrackingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

//                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<LiverTrackingList> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(LiveTrackingActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LiveTrackingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
//                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareLiverTrackingJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("OrderId", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareVerifyMobileJson: " + parentObj);
        return parentObj.toString();
    }

    private void drawRoute() {

        LatLng mOrigin, mDestination;

        mOrigin = new LatLng(driverLat, driverLong);
        mDestination = new LatLng(storeLat, storeLong);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(mOrigin, mDestination);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }
    private void drawRoute1() {

        LatLng mOrigin, mDestination;

        if (orderstatus.equals("OnTheWay")) {
            mOrigin = new LatLng(driverLat, driverLong);
        } else {
            mOrigin = new LatLng(storeLat, storeLong);
        }
        mDestination = new LatLng(userLat, userLong);

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl1(mOrigin, mDestination);

        DownloadTask1 downloadTask = new DownloadTask1();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Key
        String key = "key=" + "AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String getDirectionsUrl1(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Key
        String key = "key=" + "AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * Zooms a Route (given a List of LalLng) at the greatest possible zoom level.
     *
     * @param googleMap:      instance of GoogleMap
     * @param lstLatLngRoute: list of LatLng forming Route
     */
    public void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute) {

        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute)
            boundsBuilder.include(latLngPoint);

        int width = 3000; //map fragment(view) width;
        int height = 3000;//map fragment(view) height;
        int padding = (int) (width * 0.20); // offset from edges of the map 12% of screen
        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, padding));


//        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }


    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }
    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception on download", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("DownloadTask", "DownloadTask : " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {

                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            try {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(Color.BLACK);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (mPolyline != null) {
                    mPolyline.remove();
                }
                mPolyline = mMap.addPolyline(lineOptions);

            } else
                Toast.makeText(getApplicationContext(), "No route is found", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl1(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception on download", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to download data from Google Directions URL
     */
    private class DownloadTask1 extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl1(url[0]);
                Log.d("DownloadTask", "DownloadTask : " + data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask1 parserTask = new ParserTask1();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Directions in JSON format
     */
    private class ParserTask1 extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;


            try {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(10);
                    lineOptions.color(getResources().getColor(R.color.blue));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                if (mPolyline1 != null) {
                    mPolyline1.remove();
                }
                mPolyline1 = mMap.addPolyline(lineOptions);

            } else
                Toast.makeText(getApplicationContext(), "No route is found", Toast.LENGTH_LONG).show();
        }
    }

//        public class getTrackingDetails extends AsyncTask<String, Integer, String> {
//            ProgressDialog dialog;
//            String networkStatus;
//            String response;
//
//            @Override
//            protected void onPreExecute() {
//                networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
//                dialog = ProgressDialog.show(LiveTrackingActivity.this, "",
//                        "Please wait...");
//            }
//
//            @Override
//            protected String doInBackground(String... params) {
//                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    JSONParser jParser = new JSONParser();
//                    response = jParser.getJSONFromUrl(params[0]);
//                    Log.i("TAG", "user response: " + response);
//                    return response;
//                } else {
//                    return "no internet";
//                }
//
//            }
//
//
//            @Override
//            protected void onPostExecute(String result) {
//
//                if (result != null) {
//                    if (result.equalsIgnoreCase("no internet")) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LiveTrackingActivity.this);
//
//                        // set title
//                        alertDialogBuilder.setTitle("Bakery & Co.");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Communication error! please check the internet connection?")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    } else {
//                        try {
//                            JSONObject jo = new JSONObject(result);
//
//                            try {
//                                JSONArray ja = jo.getJSONArray("Success");
//                                for (int i = 0; i < ja.length(); i++) {
//                                    JSONObject jo1 = ja.getJSONObject(i);
//
//
//
//                                }
//
//                            } catch (JSONException je) {
//                                je.printStackTrace();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }
//
//                if (dialog != null) {
//                    dialog.dismiss();
//                }
//
//                new GetCurrentTime().execute();
//                super.onPostExecute(result);
//
//            }
//
//        }


    private void getTrafficTimeapi() {

//        AlertDialog dialog = null;

//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LiveTrackingActivity.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.progress_bar_alert;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(false);
//
//        dialog = dialogBuilder.create();
//        dialog.show();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = dialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the progressDialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth * 0.45;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);

        final String networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);

        APIInterface apiService = GoogleMapApiClient.getClient().create(APIInterface.class);
        Call<TrafficTimeList> call;

        if (orderstatus.equals("OnTheWay")) {
            call = apiService.gettraffictimes(driverLat + "," + driverLong, userLat + "," + userLong, "now", "true", "driving", "en-EN", "driving", "AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ");
        } else {
            call = apiService.gettraffictimes(driverLat + "," + driverLong, storeLat + "," + storeLong, "now", "true", "driving", "en-EN", "driving", "AIzaSyA3k068n19sujgNgTe7z6VSL6YXXqVGMuQ");
        }
//        final AlertDialog finalDialog = dialog;
        call.enqueue(new Callback<TrafficTimeList>() {

            public void onResponse(Call<TrafficTimeList> call, Response<TrafficTimeList> response) {
                if (response.isSuccessful()) {
                    TrafficTimeList registrationResponse = response.body();
                    try {
                        //status true case
                        Log.i("TAG", "onResponse: " + registrationResponse.getStatus());

                        String secs = registrationResponse.getRows().get(0).getElements().get(0).getDuration_in_traffic().getText();
                        String value = String.valueOf(registrationResponse.getRows().get(0).getElements().get(0).getDuration_in_traffic().getValue());
                        if (orderstatus.equals("OnTheWay")) {

                            if (language.equalsIgnoreCase("En")) {
                                deliveryTime.setText("Order will reach in : " + secs);
                            } else if (language.equalsIgnoreCase("Ar")) {
                                deliveryTime.setText("Order will reach in : " + secs);
                            }

                        } else {

                            if (language.equalsIgnoreCase("En")) {
                                deliveryTime.setText("Drive will reach in : " + secs);
                            } else if (language.equalsIgnoreCase("Ar")) {
                                deliveryTime.setText("Drive will reach in : " + secs);
                            }

                        }

                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat2.parse(expectedTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime = timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat1.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat1.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        int mins = (Integer.parseInt(value) / 60) + 1;
                        Calendar now = Calendar.getInstance();
                        now.setTime(currentServerDate);
                        now.add(Calendar.MINUTE, mins);
                        currentServerDate = now.getTime();
                        String CTimeString = timeFormat2.format(currentServerDate);

                        if (language.equalsIgnoreCase("En")) {
                            expTime.setText("Expected Time : " + CTimeString);
                        } else if (language.equalsIgnoreCase("Ar")) {
                            expTime.setText("الوقت المتوقع  : " + CTimeString);
                        }


                    } catch (Exception e) {

                    }

//                    if (finalDialog != null)
//                        finalDialog.dismiss();
                }
            }

            public void onFailure(Call<TrafficTimeList> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(LiveTrackingActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LiveTrackingActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }

//                if (finalDialog != null)
//                    finalDialog.dismiss();
            }
        });
    }


//        public class getTrafficTime extends AsyncTask<String, Integer, String> {
//            ProgressDialog pDialog;
//            String networkStatus;
//            String distanceResponse;
//
//            @Override
//            protected void onPreExecute() {
//                networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
//
//            }
//
//            @Override
//            protected String doInBackground(String... params) {
//                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                    JSONParser jParser = new JSONParser();
//
//                    distanceResponse = jParser
//                            .getJSONFromUrl(URL_DISTANCE + userLat + "," + userLong + "&destinations=" + driverLat + "," + driverLong + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDbUS1BGaNS_-UlStOypbm_FhnCPKFHK7Q");
//                    Log.i("TAG", "user response: " + URL_DISTANCE + userLat + "," + userLong + "&destinations=" + driverLat + "," + driverLong + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDbUS1BGaNS_-UlStOypbm_FhnCPKFHK7Q");
//                    return distanceResponse;
//                } else {
//                    return "no internet";
//                }
//
//            }
//
//
//            @Override
//            protected void onPostExecute(String result) {
//
//                if (result != null) {
//                    if (result.equalsIgnoreCase("no internet")) {
//                        Toast.makeText(LiveTrackingActivity.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                    } else {
//                        try {
//                            JSONObject jo = new JSONObject(result);
//                            JSONArray ja = jo.getJSONArray("rows");
//                            JSONObject jo1 = ja.getJSONObject(0);
//                            JSONArray ja1 = jo1.getJSONArray("elements");
//                            JSONObject jo2 = ja1.getJSONObject(0);
//                            JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
//                            String secs = jo3.getString("text");
//                            String value = jo3.getString("value");
////                        if(language.equalsIgnoreCase("En")) {
//                            if (language.equalsIgnoreCase("En")) {
//                                deliveryTime.setText("Enjoy your food in : " + secs);
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                deliveryTime.setText("استمتع بوجبتك في : " + secs);
//                            }
//
//                            Date current24Date = null, currentServerDate = null;
//                            Date expectedTimeDate = null, expectedTime24 = null;
//                            try {
//                                current24Date = timeFormat.parse(timeResponse);
//                                expectedTime24 = timeFormat2.parse(expectedTime);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                            String currentTime = timeFormat1.format(current24Date);
//                            String expTimeStr = timeFormat1.format(expectedTime24);
//                            try {
//                                currentServerDate = timeFormat1.parse(currentTime);
//                                expectedTimeDate = timeFormat1.parse(expTimeStr);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                            long diff = expectedTimeDate.getTime() - currentServerDate.getTime();
//
//                            long diffSeconds = diff / 1000 % 60;
//                            long diffMinutes = diff / (60 * 1000) % 60;
//                            long diffHours = diff / (60 * 60 * 1000) % 24;
//                            int expMins = (int) diffMinutes * 60 * 1000;
//                            Log.i("TAG", "mins response: " + expMins);
//                            int mins = (Integer.parseInt(value) / 60) + 1;
//                            Calendar now = Calendar.getInstance();
//                            now.setTime(currentServerDate);
//                            now.add(Calendar.MINUTE, mins);
//                            currentServerDate = now.getTime();
//                            String CTimeString = timeFormat2.format(currentServerDate);
//                            if (language.equalsIgnoreCase("En")) {
//                                expTime.setText("Expected Time : " + CTimeString);
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                expTime.setText("الوقت المتوقع  : " + CTimeString);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                }
//                super.onPostExecute(result);
//
//            }
//
//        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
//        new getTrackingDetails().cancel(true);
    }
}
