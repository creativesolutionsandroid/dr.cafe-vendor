package com.cs.dr_cafe_vendor.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.dr_cafe_vendor.Constants;
import com.cs.dr_cafe_vendor.Models.ChangePasswordlist;
import com.cs.dr_cafe_vendor.NetworkUtil;
import com.cs.dr_cafe_vendor.R;
import com.cs.dr_cafe_vendor.Rest.APIInterface;
import com.cs.dr_cafe_vendor.Rest.ApiClient;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.dr_cafe_vendor.Constants.showOneButtonAlertDialog;


public class ChangePasswordActivity extends AppCompatActivity {

    EditText inputOldPassword, inputNewPassword, inputConfirmPassword;
    String strOldPassword, strNewPassword, strConfirmPassword;
    Button buttonSubmit;
    Context context;
    String language, userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    Toolbar toolbar;
    ImageView back_btn;
    AlertDialog loaderDialog = null;

    //    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_change_password);
        } else {
            setContentView(R.layout.activity_change_password_arabic);
        }
        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        userId = userPrefs.getString("userId", "0");


        buttonSubmit = (Button) findViewById(R.id.button_submit);
        inputOldPassword = (EditText) findViewById(R.id.change_password_input_old_password);
        inputNewPassword = (EditText) findViewById(R.id.change_password_input_new_password);
        inputConfirmPassword = (EditText) findViewById(R.id.change_password_input_confirm_password);


        back_btn = (ImageView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new changePasswordApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

//        setTypeface();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    private void setTypeface() {
//        inputOldPassword.setTypeface(Constants.getTypeFace(context));
//        inputNewPassword.setTypeface(Constants.getTypeFace(context));
//        inputConfirmPassword.setTypeface(Constants.getTypeFace(context));
//        buttonSubmit.setTypeface(Constants.getTypeFace(context));
//    }


    private boolean validations() {
        strOldPassword = inputOldPassword.getText().toString();
        strNewPassword = inputNewPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strOldPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        } else if (strOldPassword.length() < 4 || strOldPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        }
        if (strNewPassword.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputNewPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            } else {
                inputNewPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }
            Constants.requestEditTextFocus(inputNewPassword, ChangePasswordActivity.this);
            return false;
        } else if (strNewPassword.length() < 4 || strNewPassword.length() > 20) {
            if (language.equalsIgnoreCase("En")) {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            } else {
                inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }
            Constants.requestEditTextFocus(inputOldPassword, ChangePasswordActivity.this);
            return false;
        } else if (!strNewPassword.equals(strConfirmPassword)) {
            if (language.equalsIgnoreCase("En")) {
                inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            } else {
                inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match_ar));
            }
            Constants.requestEditTextFocus(inputConfirmPassword, ChangePasswordActivity.this);
            return false;
        }

        return true;
    }

//    private void clearErrors() {
//        inputLayoutOldPassword.setErrorEnabled(false);
//        inputLayoutNewPassword.setErrorEnabled(false);
//        inputLayoutConfirmPassword.setErrorEnabled(false);
//    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.change_password_input_old_password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputOldPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.change_password_input_new_password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {
                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.change_password_input_confirm_password:
//                    clearErrors();
                    if (editable.length() > 20) {
                        if (language.equalsIgnoreCase("En")) {

                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        } else {
                            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
            }
        }
    }

    private String prepareChangePasswordJson() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("VendorId", userId);
            parentObj.put("OldPassword", strOldPassword);
            parentObj.put("NewPassword", strNewPassword);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "prepareChangePasswordJson: " + parentObj);

        return parentObj.toString();
    }

    private class changePasswordApi extends AsyncTask<String, Integer, String> {

//        ACProgressFlower dialog;
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareChangePasswordJson();

            Constants.showLoadingDialog(ChangePasswordActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordlist> call = apiService.getchangepass(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordlist>() {
                @Override
                public void onResponse(Call<ChangePasswordlist> call, Response<ChangePasswordlist> response) {
                    if (response.isSuccessful()) {
                        ChangePasswordlist changePasswordResponse = response.body();
                        if (changePasswordResponse.getStatus()) {
                            String message = changePasswordResponse.getMessageEn();
                            Log.i("TAG", "onResponse: " + message);
                            AlertDialog customDialog = null;
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChangePasswordActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout;
//                            if (language.equalsIgnoreCase("En")) {
                                 layout = R.layout.alert_dialog;
//                            }else {
//                                 layout = R.layout.alert_dialog_arabic;
//                            }
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView title = (TextView) dialogView.findViewById(R.id.title);
                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);
                            if (language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.app_name));
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText("Password Changed Successful");
                            } else {
                                title.setText(getResources().getString(R.string.app_name_ar));
                                yes.setText(getResources().getString(R.string.ok_ar));
                                desc.setText("تم تغيير كلمة المرور بنجاح");
                            }

                            customDialog = dialogBuilder.create();
                            customDialog.show();

                            final AlertDialog finalCustomDialog = customDialog;
                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    finalCustomDialog.dismiss();
                                    finish();
                                }
                            });

                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth * 0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        } else {
                            String failureResponse = changePasswordResponse.getMessageEn();
                            if (language.equalsIgnoreCase("En")) {
                                showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ChangePasswordActivity.this);
                            } else {
                                showOneButtonAlertDialog(changePasswordResponse.getMessageAr(), getResources().getString(R.string.app_name_ar),
                                        getResources().getString(R.string.ok_ar), ChangePasswordActivity.this);
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                 Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ChangePasswordlist> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangePasswordActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ChangePasswordActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ChangePasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                   Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

//    private void showOneButtonAlertDialog(String descriptionStr, String titleStr, String buttonStr, Activity context){
//        AlertDialog customDialog = null;
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = context.getLayoutInflater();
//        int layout = R.layout.alert_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(false);
//
//        TextView title = (TextView) dialogView.findViewById(R.id.title);
//        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//        View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//        no.setVisibility(View.GONE);
//        vert.setVisibility(View.GONE);
//
//        title.setText(titleStr);
//        yes.setText(buttonStr);
//        desc.setText(descriptionStr);
//
//        customDialog = dialogBuilder.create();
//        customDialog.show();
//
//        final AlertDialog finalCustomDialog = customDialog;
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finalCustomDialog.dismiss();
//                finish();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = customDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = context.getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
//    }
}
